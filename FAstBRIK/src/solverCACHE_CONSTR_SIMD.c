#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <stdint.h>

#include "include/solverCACHE_CONSTR_SIMD.h"
#include "include/dataformat.h"
#include "include/algorithm.h"

void checkPoint_CACHE_CONSTR_SIMD(const Point* p, const char* label, int line)
{
	checkArray((float*)&p->x, 3, label, line);
}

void checkRotation_CACHE_CONSTR_SIMD(const Rotation* r, const char* label, int line)
{
	checkPoint_CACHE_CONSTR_SIMD(&r->x, label, line);
	checkPoint_CACHE_CONSTR_SIMD(&r->y, label, line);
	checkPoint_CACHE_CONSTR_SIMD(&r->z, label, line);
}

void checkTransform_CACHE_CONSTR_SIMD(const Transform* t, const char* label, int line)
{
	checkPoint_CACHE_CONSTR_SIMD(&t->translation, label, line);
	checkRotation_CACHE_CONSTR_SIMD(&t->rotation, label, line);
}

FLOAT length_squared_CACHE_CONSTR_SIMD(Point *p1, Point *p2)
{
	FLOAT dx, dy, dz;
	dx = p1->x - p2->x;
	dy = p1->y - p2->y;
	dz = p1->z - p2->z;
	return dx * dx + dy * dy + dz * dz;
}

FLOAT computeError_CACHE_CONSTR_SIMD(int jointCount, SolverJoint_CACHE_CONSTR_SIMD* joints)
{
	FLOAT current_error = 0;
	for (int i = 0; i < jointCount; i++)
	{
		// TODO: Can be achieved without using "has objective"
		/*
		if (joints[i].has_objective)
		{
			// TODO: Probably better to just add them up (?)
			const FLOAT ls = length_squared_CACHE_MEM_CONSTR(&joints[i].current_target, &joints[i].t.translation);
			current_error = max(ls, current_error);
		}
		*/
	}

	return current_error;
}

Rotation_CACHE_CONSTR_SIMD transpose(const Rotation_CACHE_CONSTR_SIMD* rot)
{
	__m128 _Tmp2, _Tmp1, _Tmp0;
	_Tmp0 = _mm_shuffle_ps(rot->x, rot->y, 0b00000000);
	_Tmp1 = _mm_shuffle_ps(rot->x, rot->y, 0b00010001);
	_Tmp2 = _mm_shuffle_ps(rot->x, rot->y, 0b00100010);

	Rotation_CACHE_CONSTR_SIMD trn;
	trn.x = _mm_shuffle_ps(_Tmp0, rot->z, 0b00001000);
	trn.y = _mm_shuffle_ps(_Tmp1, rot->z, 0b00011000);
	trn.z = _mm_shuffle_ps(_Tmp2, rot->z, 0b00101000);
	return trn;
}

INLINE __m128 constrain_CACHE_CONSTR_SIMD(const Rotation_CACHE_CONSTR_SIMD* rot, const __m128 nrm, const __m128 cst)
{

	/*
	Point normal;
	_mm_store_ps(&normal.x, nrm);

	Rotation r;
	_mm_store_ps(&r.x.x, rot->x);
	_mm_store_ps(&r.y.x, rot->y);
	_mm_store_ps(&r.z.x, rot->z);
	
	Point c;
	_mm_store_ps(&c.x, cst);
	
	// Project onto axis (or, transform to local space)
	const FLOAT xDot = normal.x * r.x.x + normal.y * r.x.y + normal.z * r.x.z;
	const FLOAT yDot = normal.x * r.y.x + normal.y * r.y.y + normal.z * r.y.z;
	const FLOAT zDot = normal.x * r.z.x + normal.y * r.z.y + normal.z * r.z.z;



	// Transform to local space
	const __m128 dpx = _mm_dp_ps(nrm, rot->x, 0b01110001);
	const __m128 dpy = _mm_dp_ps(nrm, rot->y, 0b01110101);
	const __m128 dpz = _mm_dp_ps(nrm, rot->z, 0b01111010);
	const __m128 pol = _mm_or_ps(dpy, dpz);

	// Can compute mult for all constraints at once, permutation is done later with comparison masking
	const __m128 clp = _mm_mul_ps(pol, cst);

	const __m128 neg = _mm_xor_ps(clp, _mm_set_ps(-0.0f, -0.0f, 0.0f, 0.0f)); // Negates third and fourth entry for masking
	const __m128 mask = _mm_cmpgt_ps(neg, _mm_set1_ps(0.0)); // Puts 0xFFFFFFFF for the two positive entries
	const __m128 pnt = _mm_and_ps(clp, mask); // 1st or 3rd entry is py, 2nd or 4th entry is pz

	const __m128 prj = _mm_sqrt_ps(_mm_dp_ps(pnt, pnt, 0b11111111));// _mm_max_ps(, _mm_set1_ps(EPS_THRES));

	FLOAT project;
	_mm_store_ps(&project, prj);
	//if (project < EPS_THRES) return rot->x;

	const __m128 ones = _mm_set1_ps(1.0);
	const __m128 dt = _mm_sub_ps(ones, prj);

	FLOAT dotCheck, frwCheck;
	_mm_store_ps(&dotCheck, dpx);
	_mm_store_ps(&frwCheck, dt);


	// Find ellipse axis'
	FLOAT a, b;
	a = (yDot < FLT(0.0)) ? c.z : c.x;
	b = (zDot < FLT(0.0)) ? c.w : c.y;

	// "Project" onto ellipse.
	// This skews the result but works well enough.
	const FLOAT py = yDot * a;
	const FLOAT pz = zDot * b;

	// Compute final angle to check whether we need to constrain at all
	const FLOAT proj = SQRT(py * py + pz * pz);
	if (proj < EPS_THRES)
	{
		// If all constraints are 0 we can just use forward vector
		return rot->x;
	}
	else
	{
		const FLOAT dot = FLT(1.0) - proj;
		if (dot > xDot)
		{
			// Convert to euclidean space and normalise
			const FLOAT s = SQRT(FLT(1.0) - dot * dot) / proj;
			const FLOAT yd = py * s;
			const FLOAT zd = pz * s;







			// Extract scaled py and pz using dot-products
			const __m128 sqt = _mm_sub_ps(ones, _mm_mul_ps(dt, dt));
			const __m128 scl = _mm_div_ps(_mm_sqrt_ps(sqt), prj);
			const __m128 py = _mm_dp_ps(pnt, scl, 0b01010010);
			const __m128 pz = _mm_dp_ps(pnt, scl, 0b10100100);

			// Extract projected local coordinate
			const __m128 frw = _mm_load_ss(&dot);
			const __m128 vec = _mm_or_ps(frw, _mm_or_ps(py, pz));

			Point tst;
			_mm_store_ps(&tst.x, vec);
			printf("tst: %.3f, %.3f, %.3f, %.3f \n", tst.x, tst.y, tst.z, tst.w);
			printf("xDot: %.3f, %.3f, %.3f, %.3f \n", dot, yd, zd, 0.0f);



			// Project back into worldspace
			Point out;
			out.x = dot * r.x.x + yd * r.y.x + zd * r.z.x;
			out.y = dot * r.x.y + yd * r.y.y + zd * r.z.y;
			out.z = dot * r.x.z + yd * r.y.z + zd * r.z.z;

			return _mm_load_ps(&out.x);
		}
	}
	return nrm;
	*/


	// Transform to local space
	const __m128 dpx = _mm_dp_ps(nrm, rot->x, 0b01110001);
	const __m128 dpy = _mm_dp_ps(nrm, rot->y, 0b01110101);
	const __m128 dpz = _mm_dp_ps(nrm, rot->z, 0b01111010);
	const __m128 pol = _mm_or_ps(dpy, dpz);

	// Can compute mult for all constraints at once, permutation is done later with comparison masking
	const __m128 clp = _mm_mul_ps(pol, cst);

	const __m128 neg = _mm_xor_ps(clp, _mm_set_ps(-0.0f, -0.0f, 0.0f, 0.0f)); // Negates third and fourth entry for masking
	const __m128 mask = _mm_cmpgt_ps(neg, _mm_set1_ps(0.0)); // Puts 0xFFFFFFFF for the two positive entries
	const __m128 pnt = _mm_and_ps(clp, mask); // 1st or 3rd entry is py, 2nd or 4th entry is pz

	// Since at most two entries here can be non-zero, dot product over the whole vector will return the proper norm
	const __m128 prjsq = _mm_max_ps(_mm_dp_ps(pnt, pnt, 0b11111111), _mm_set1_ps(EPS_THRES * EPS_THRES));
	
	// Masking is slower than omitting branch
	FLOAT dotsq, dotX;
	_mm_store_ss(&dotX, dpx);
	_mm_store_ss(&dotsq, prjsq);
	const FLOAT chk = FLT(1.0) - dotX;
	if (chk * chk > dotsq)
	{
		const __m128 ones = _mm_set1_ps(1.0);
		const __m128 prj = _mm_sqrt_ps(prjsq);
		const __m128 dt = _mm_sub_ps(ones, prj);

		// Extract scaled py and pz using dot-products
		const __m128 sqt = _mm_sub_ps(ones, _mm_mul_ps(dt, dt));
		const __m128 scl = _mm_div_ps(_mm_sqrt_ps(sqt), prj);
		const __m128 py = _mm_dp_ps(pnt, scl, 0b01010010);
		const __m128 pz = _mm_dp_ps(pnt, scl, 0b10100100);

		// Extract projected local coordinate
		const __m128 frw = _mm_mul_ps(dt, _mm_set_ps(FLT(0.0), FLT(0.0), FLT(0.0), FLT(1.0))); //_mm_load_ss(&dt);
		const __m128 vec = _mm_or_ps(frw, _mm_or_ps(py, pz));

		// Transform to worldspace
		Rotation_CACHE_CONSTR_SIMD trn = transpose(rot);
		const __m128 dpx = _mm_dp_ps(vec, trn.x, 0b01110001);
		const __m128 dpy = _mm_dp_ps(vec, trn.y, 0b01110010);
		const __m128 dpz = _mm_dp_ps(vec, trn.z, 0b01110100);
		return _mm_or_ps(dpx, _mm_or_ps(dpy, dpz));
	}
	return nrm;
}

INLINE Rotation_CACHE_CONSTR_SIMD rotateTowards_CACHE_CONSTR_SIMD(const Rotation_CACHE_CONSTR_SIMD* r, const __m128 frw)
{
	Rotation_CACHE_CONSTR_SIMD out;
	out.x = frw;

	// Project axis
	const __m128 yDot = _mm_dp_ps(frw, r->y, 0b01111111);
	const __m128 yPrj = _mm_mul_ps(frw, yDot);
	const __m128 proj = _mm_sub_ps(r->y, yPrj);

	// normalise
	const __m128 sq = _mm_dp_ps(proj, proj, 0b01111111);
	const __m128 s = _mm_sqrt_ps(sq);
	out.y = _mm_div_ps(proj, s);

	// Cross product for vector base
	const __m128 nrmt = _mm_shuffle_ps(frw, frw, _MM_SHUFFLE(3, 0, 2, 1));
	const __m128 frwt = _mm_shuffle_ps(out.y, out.y, _MM_SHUFFLE(3, 0, 2, 1));
	const __m128 zxy = _mm_sub_ps(_mm_mul_ps(frw, frwt), _mm_mul_ps(nrmt, out.y));
	out.z = _mm_shuffle_ps(zxy, zxy, _MM_SHUFFLE(3, 0, 2, 1));

	// By permuting the cross product we can save a shuffle
	//out.z.z = out.x.x * out.y.y - out.x.y * out.y.x;
	//out.z.x = out.x.y * out.y.z - out.x.z * out.y.y;
	//out.z.y = out.x.z * out.y.x - out.x.x * out.y.z;

	return out;
}


void doSolve_CACHE_CONSTR_SIMD(Armature_CACHE_CONSTR_SIMD* armature, int* mapping, Pose *output, unsigned int iterations)
{
	//FLOAT old_error = FLT_MAX;
	//FLOAT current_error = 0;
	//current_error = computeError_CACHE_CONSTR_SIMD(armature->jointCount, armature->joints);
	// TODO: Always same amount of iterations for performance testing
	// while (current_error > FABRIK_EPS && fabs(old_error - current_error) > FABRIK_EPS)
	for(unsigned int i = 0; i < iterations; i++)
	{
		// forward
		for (int j = 0; j < armature->jointCount-1; j++)
		{
			const int index = mapping[j];

			SolverJoint_CACHE_CONSTR_SIMD* ae = &armature->joints[index];
			Transform_CACHE_CONSTR_SIMD* at = &armature->transforms[index];
			const Transform_CACHE_CONSTR_SIMD* pt = &armature->transforms[ae->predecessor];
			SolverJoint_CACHE_CONSTR_SIMD* pe = &armature->joints[ae->predecessor];

			// Calculate proper average
			at->translation = _mm_mul_ps(ae->point_avg, ae->weight);
			ae->point_avg = ae->current_target;

			// Compute desired direction
			const __m128 mmDelta = _mm_sub_ps(at->translation, pt->translation);
			const __m128 mmSq = _mm_dp_ps(mmDelta, mmDelta, 0b01111111);
			const __m128 mmS = _mm_sqrt_ps(mmSq);
			const __m128 mmDiv = _mm_max_ps(mmS, _mm_set1_ps(FABRIK_EPS_CACHE_CONSTR_SIMD));
			const __m128 mmNrm = _mm_div_ps(mmDelta, mmDiv);

			//const Point delta = { point->x - prev->x, point->y - prev->y, point->z - prev->z };
			//const FLOAT norm = max(SQRT(delta.x * delta.x + delta.y * delta.y + delta.z * delta.z), FABRIK_EPS_CACHE_CONSTR_SIMD);
			//Point nrm = { delta.x / norm, delta.y / norm, delta.z / norm };

			// Move bone
			const __m128 mmFma = MM_FMSUB_PS(ae->bone_length, mmNrm, at->translation);
			pe->point_avg = _mm_sub_ps(pe->point_avg, mmFma);

			//prept->x += point->x - ae->bone_length * nrm.x;
			//prept->y += point->y - ae->bone_length * nrm.y;
			//prept->z += point->z - ae->bone_length * nrm.z;
		}

		// backward
		// we did not move the root, therefore we can skip it
		for (int j = armature->jointCount - 2; j >= 0; j--)
		{
			const int index = mapping[j];

			SolverJoint_CACHE_CONSTR_SIMD* ae = &armature->joints[index];
			Transform_CACHE_CONSTR_SIMD* at = &armature->transforms[index];
			const Transform_CACHE_CONSTR_SIMD* pt = &armature->transforms[ae->predecessor];
			SolverJoint_CACHE_CONSTR_SIMD* pe = &armature->joints[ae->predecessor];

			// Compute desired direction
			const __m128 mmDelta = _mm_sub_ps(at->translation, pt->translation);
			const __m128 mmSq = _mm_dp_ps(mmDelta, mmDelta, 0b01111111);
			const __m128 mmS = _mm_sqrt_ps(mmSq);
			const __m128 mmDiv = _mm_max_ps(mmS, _mm_set1_ps(FABRIK_EPS_CACHE_CONSTR_SIMD));
			__m128 nrm = _mm_div_ps(mmDelta, mmDiv);

			//const Point delta = { point->x - prev->x, point->y - prev->y, point->z - prev->z };
			//const FLOAT norm = max(SQRT(delta.x*delta.x + delta.y*delta.y + delta.z*delta.z), FABRIK_EPS_CACHE_CONSTR_SIMD);
			//Point nrm = { delta.x / norm, delta.y / norm, delta.z / norm };

			// Constrain to axis and rotate
#ifdef FABRIK_CONSTRAINT
			nrm = constrain_CACHE_CONSTR_SIMD(&pt->rotation, nrm, pe->c);
#endif
#ifdef FABRIK_ROTATION
			at->rotation = rotateTowards_CACHE_CONSTR_SIMD(&pt->rotation, nrm);
#endif

			// Move bone
			at->translation = MM_FMADD_PS(ae->bone_length, nrm, pt->translation);
		}
		//old_error = current_error;
		//current_error = computeError_CACHE_CONSTR_SIMD(armature->jointCount, armature->joints);
	}
	for (int i = 0; i < armature->jointCount; i++)
	{
		Transform_CACHE_CONSTR_SIMD* t = &armature->transforms[i];
		_mm_store_ps(&output[i].translation.x, t->translation);
		_mm_store_ps(&output[i].rotation.x.x, t->rotation.x);
		_mm_store_ps(&output[i].rotation.y.x, t->rotation.y);
		_mm_store_ps(&output[i].rotation.z.x, t->rotation.z);

		VALIDATE(Transform_CACHE_CONSTR_SIMD, &output[i]);
	}
}

void solve_CACHE_CONSTR_SIMD(Scene* scene)
{
	Armature *armature = scene->armature;

	Armature_CACHE_CONSTR_SIMD arm;
	arm.jointCount = armature->jointCount;

	void* jointsMem = malloc(sizeof(SolverJoint_CACHE_CONSTR_SIMD) * arm.jointCount + 63);
	arm.joints = (SolverJoint_CACHE_CONSTR_SIMD*)((((uintptr_t)jointsMem + 63) & ~(uintptr_t)63));
	SolverJoint_CACHE_CONSTR_SIMD* joints = malloc(sizeof(SolverJoint_CACHE_CONSTR_SIMD) * arm.jointCount);

	void* transformsMem = malloc(sizeof(Transform_CACHE_CONSTR_SIMD) * arm.jointCount + 63);
	arm.transforms = (Transform_CACHE_CONSTR_SIMD*)((((uintptr_t)transformsMem + 63) & ~(uintptr_t)63));
	Transform_CACHE_CONSTR_SIMD* transforms = malloc(sizeof(Transform_CACHE_CONSTR_SIMD) * arm.jointCount);

	int** children = (int**)malloc(sizeof(int*) * arm.jointCount);
	int* childCount = (int*)malloc(sizeof(int) * arm.jointCount);

	// compute predecessors and intialize the SolverJoints
	for (int i = 0; i < arm.jointCount; i++)
	{
		childCount[i] = 0;

		joints[i].current_target = _mm_set1_ps(0.0f);
		joints[i].point_avg = _mm_set1_ps(0.0f);

		Point c;
		c.x = FLT(1.0) - COS(armature->joints[i].c.x);
		c.y = FLT(1.0) - COS(armature->joints[i].c.y);
		c.z = FLT(1.0) - COS(armature->joints[i].c.z);
		c.w = FLT(1.0) - COS(armature->joints[i].c.w);
		joints[i].c = _mm_load_ps(&c.x);

		transforms[i].translation = _mm_load_ps(&scene->initialPose[i].translation.x);
		transforms[i].rotation.x = _mm_load_ps(&scene->initialPose[i].rotation.x.x);
		transforms[i].rotation.y = _mm_load_ps(&scene->initialPose[i].rotation.y.x);
		transforms[i].rotation.z = _mm_load_ps(&scene->initialPose[i].rotation.z.x);
		if (i != 0)
		{
			joints[i].predecessor = armature->joints[i].parent;
			const FLOAT boneLength = SQRT(length_squared_CACHE_CONSTR_SIMD(&scene->initialPose[i].translation, &scene->initialPose[joints[i].predecessor].translation));
			joints[i].bone_length = _mm_set1_ps(boneLength);
		}
		else
		{
			joints[i].predecessor = 0;
		}
	}
	for (int i = 0; i < arm.jointCount; i++)
	{
		if (joints[i].predecessor != i)
		{
			childCount[joints[i].predecessor]++;
		}
	}
	for (int i = 0; i < arm.jointCount; i++)
	{
		children[i] = (int*)malloc(sizeof(int) * childCount[i]);
		childCount[i] = 0;
	}
	for (int i = 0; i < arm.jointCount; i++)
	{
		const int pred = joints[i].predecessor;
		if (pred != i)
		{
			children[pred][childCount[pred]] = i;
			childCount[pred]++;
		}
	}

	// Compute topological ordering of joints
	int *jointOrder = (int*)malloc(sizeof(int) * arm.jointCount);
	int jointOrderPos = 0;
	int *toposort = (int*)malloc(sizeof(int) * arm.jointCount);
	for (int i = 0; i < arm.jointCount; i++)
	{
		toposort[i] = childCount[i];
		if (toposort[i] == 0)
		{
			jointOrder[jointOrderPos++] = i;
		}
	}
	for (int i = 0; i < arm.jointCount-1; i++)
	{
		int parentIndex = joints[jointOrder[i]].predecessor;
		toposort[parentIndex]--;
		if (toposort[parentIndex] == 0)
		{
			jointOrder[jointOrderPos++] = parentIndex;
		}
	}
	free(toposort);
	assert(jointOrderPos == arm.jointCount);
	assert(jointOrder[jointOrderPos-1] == 0);

	for (int i = 0; i < arm.jointCount; i++)
	{
		const int newpos_count = childCount[i];
		if (newpos_count > 0)
		{
			joints[i].weight = _mm_set1_ps(FLT(1.0) / newpos_count);
		}
		else
		{
			joints[i].weight = _mm_set1_ps(FLT(0.0));
		}
	}

	// set objective joints
	for (int i = 0; i < armature->objectiveCount; i++)
	{
		const int index = armature->objectives[i].joint;
		const int newpos_count = childCount[index];
		joints[index].weight = _mm_set1_ps(FLT(1.0) / (newpos_count + 1));
	}

	// Remap joints for sequential access
	int* mapping = (int*)malloc(sizeof(int) * arm.jointCount);
	for (int i = 0; i < arm.jointCount; i++)
	{
		const int index = i;// jointOrder[i];
		arm.joints[i] = joints[index];
		arm.transforms[i] = transforms[index];
		mapping[index] = i;
	}
	for (int i = 0; i < arm.jointCount; i++)
	{
		arm.joints[i].predecessor = mapping[arm.joints[i].predecessor];
	}

	///////////////////////////////////////////////////////////////////////////////

	// run FABRIK for each frame
	for (int f = 0; f < scene->frameCount; f++)
	{
		///////////////////////////////////////////////////////////////////////////////
		Target* targets = scene->targets[f];
		for (int i = 0; i < armature->objectiveCount; i++)
		{
			const int index = mapping[armature->objectives[i].joint];
			SolverJoint_CACHE_CONSTR_SIMD* ae = &arm.joints[index];
			ae->current_target = _mm_load_ps(&targets[i].x);
			ae->point_avg = ae->current_target;
		}
		Pose* output = scene->frames[f];
		doSolve_CACHE_CONSTR_SIMD(&arm, jointOrder, output, scene->iterations);
		///////////////////////////////////////////////////////////////////////////////
	}

	// clean up
	free(jointOrder);
	for (int i = 0; i < arm.jointCount; i++)
	{
		free(children[i]);
	}
	free(children);
	free(childCount);
	free(mapping);

	free(joints);
	free(transforms);

	free(jointsMem);
	free(transformsMem);
}
