#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "include/solverSIMD.h"
#include "include/dataformat.h"
#include "include/algorithm.h"
#include "include/armature.h"


void checkPoint_SIMD(const __m128 p, const char* label, int line)
{
	FLOAT f[4];
	_mm_store_ps(f, p);
	checkArray((float*)f, 3, label, line);
}

void checkRotation_SIMD(const Rotation_SIMD* r, const char* label, int line)
{
	checkPoint_SIMD(r->x, label, line);
	checkPoint_SIMD(r->y, label, line);
	checkPoint_SIMD(r->z, label, line);
}

void checkTransform_SIMD(const Transform_SIMD* t, const char* label, int line)
{
	checkPoint_SIMD(t->translation, label, line);
	checkRotation_SIMD(&t->rotation, label, line);
}


INLINE FLOAT dot_prod_SIMD(const __m128 p1, const __m128 p2)
{
	// TODO: Make absolutely sure memory is aligned
	const __m128 dp = _mm_dp_ps(p1, p2, 0b01110001);

	FLOAT f = 0.0f;
	_mm_store_ss(&f, dp);
	return f;
}

INLINE __m128 length_squared_SIMD(const __m128 p1, const __m128 p2)
{
	const __m128 d = _mm_sub_ps(p1, p2);
	return _mm_dp_ps(d, d, 0b01111111);
}

INLINE FLOAT computeError_SIMD(int jointCount, SolverJoint_SIMD* joints)
{
	FLOAT current_error = 0;
	for (int i = 0; i < jointCount; i++)
	{
		if (joints[i].has_objective)
		{
			// TODO: Probably better to just add them up (?)
			const __m128 d = _mm_sub_ps(joints[i].current_target, joints[i].t.translation);
			const __m128 dp = _mm_dp_ps(d, d, 0b01110001);

			FLOAT ls = 0.0f;
			_mm_store_ss(&ls, dp);

			current_error = max(ls, current_error);
		}
	}

	return current_error;
}

INLINE __m128 constrain_SIMD(const Rotation_SIMD* r, const __m128 nrm, FLOAT c[4])
{
	// TODO: Constraints make about half the runtime! There is much to optimise still!

	// Project onto axis (or, transform to local space)
	//const __m128 nrm = _mm_load_ps(normal);

	const FLOAT xDot = dot_prod_SIMD(nrm, r->x);
	const FLOAT yDot = dot_prod_SIMD(nrm, r->y);
	const FLOAT zDot = dot_prod_SIMD(nrm, r->z);

	// Convert to polar coordinates
	const FLOAT angle = ACOS(xDot);
	const FLOAT norm = SQRT(yDot * yDot + zDot * zDot);

	OP_ADD(1);
	if (norm > EPS_THRES)
	{
		// TODO: Can do some SIMD masking magic here
		// Find ellipse axis'
		FLOAT a, b;
		if (yDot >= FLT(0.0) && zDot >= FLT(0.0)) { a = c[0]; b = c[1]; }
		else if (yDot >= FLT(0.0) && zDot < FLT(0.0)) { a = c[0]; b = c[3]; }
		else if (yDot < FLT(0.0) && zDot >= FLT(0.0)) { a = c[2]; b = c[1]; }
		else // (yDot < FLT(0.0) && zDot < FLT(0.0))
		{
			a = c[2]; b = c[3];
		}

		// "Project" onto ellipse.
		// This skews the result but works well enough.
		const FLOAT py = yDot / norm * a;
		const FLOAT pz = zDot / norm * b;

		// Compute final angle to check whether we need to constrain at all
		const FLOAT proj = SQRT(py * py + pz * pz);
		if (proj < EPS_THRES)
		{
			// If all constraints are 0 we can just use forward vector
			return r->x;
		}
		else
		{
			if (angle > proj)
			{
				// Convert to euclidean space and normalise
				const FLOAT dot = COS(proj);
				const FLOAT s = SQRT(FLT(1.0) - dot * dot) / proj;
				const FLOAT yd = py * s;
				const FLOAT zd = pz * s;

				// Project back into worldspace

				const __m128 dt = _mm_load_ps1(&dot);
				const __m128 y0 = _mm_mul_ps(dt, r->x);

				const __m128 ys = _mm_load_ps1(&yd);
				const __m128 y1 = MM_FMADD_PS(ys, r->y, y0);

				const __m128 zs = _mm_load_ps1(&zd);
				const __m128 y2 = MM_FMADD_PS(zs, r->z, y1);

				//Point out;
				//_mm_store_ps(&out.x, y2);
				//out.x = dot * r->x.x + yd * r->y.x + zd * r->z.x; OP_ADD(2); OP_MUL(3);
				//out.y = dot * r->x.y + yd * r->y.y + zd * r->z.y; OP_ADD(2); OP_MUL(3);
				//out.z = dot * r->x.z + yd * r->y.z + zd * r->z.z; OP_ADD(2); OP_MUL(3);

				return y2;
			}
		}
	}
	return nrm;
}

INLINE Rotation_SIMD rotateTowards_SIMD(const Rotation_SIMD* r, const __m128 frw)
{
	Rotation_SIMD out;
	out.x = frw;

	// Project axis
	const __m128 yDot = _mm_dp_ps(frw, r->y, 0b01111111);
	const __m128 yPrj = _mm_mul_ps(frw, yDot);
	const __m128 proj = _mm_sub_ps(r->y, yPrj);

	// normalise
	const __m128 sq = _mm_dp_ps(proj, proj, 0b01111111);
	const __m128 s = _mm_sqrt_ps(sq);
	out.y = _mm_div_ps(proj, s);

	// Cross product for vector base
#ifdef FABRIK_CROSS_OPT
	const __m128 nrmt = _mm_shuffle_ps(frw, frw, _MM_SHUFFLE(3, 0, 2, 1));
	const __m128 frwt = _mm_shuffle_ps(out.y, out.y, _MM_SHUFFLE(3, 0, 2, 1));
	const __m128 zxy = _mm_sub_ps(_mm_mul_ps(frw, frwt), _mm_mul_ps(nrmt, out.y));
	out.z = _mm_shuffle_ps(zxy, zxy, _MM_SHUFFLE(3, 0, 2, 1));
#else
	const __m128 frw_yzx = _mm_shuffle_ps(frw, frw, _MM_SHUFFLE(3, 0, 2, 1));
	const __m128 frw_zxy = _mm_shuffle_ps(frw, frw, _MM_SHUFFLE(3, 1, 0, 2));
	const __m128 nrm_zxy = _mm_shuffle_ps(nrm, nrm, _MM_SHUFFLE(3, 1, 0, 2));
	const __m128 nrm_yzx = _mm_shuffle_ps(nrm, nrm, _MM_SHUFFLE(3, 0, 2, 1));
	out.z = _mm_sub_ps(_mm_mul_ps(frw_yzx, nrm_zxy), _mm_mul_ps(frw_zxy, nrm_yzx));
#endif

	// By permuting the cross product we can save a shuffle
	//out.z.z = out.x.x * out.y.y - out.x.y * out.y.x;
	//out.z.x = out.x.y * out.y.z - out.x.z * out.y.y;
	//out.z.y = out.x.z * out.y.x - out.x.x * out.y.z;

	return out;
}

void doSolve_SIMD(int jointCount, SolverJoint_SIMD *joints, int *jointOrder, Pose *output, unsigned int iterations)
{
	//FLOAT old_error = FLT_MAX;
	//FLOAT current_error = 0;
	//current_error = computeError_SIMD(jointCount, joints);

	// TODO: Always same amount of iterations for performance testing
	//while (current_error > FABRIK_EPS_SIMD && fabs(old_error - current_error) > FABRIK_EPS_SIMD)
	for (unsigned int i = 0; i < iterations; i++)
	{
		// forward
		for (int j = 0; j < jointCount - 1; j++)
		{
			SolverJoint_SIMD *ae = &joints[jointOrder[j]];

			// Add target to average count
			int newpos_count = ae->successor_count;
			if (ae->has_objective)
			{
				ae->point_avg = _mm_add_ps(ae->point_avg, ae->current_target);
				newpos_count += 1;
			}

			// Calculate proper average
			if (newpos_count > 0)
			{
				const float weight = (float)newpos_count;
				const __m128 cnt = _mm_load_ps1(&weight);
				ae->t.translation = _mm_div_ps(ae->point_avg, cnt);

				ae->point_avg = _mm_set1_ps(0);
			}

			// Compute desired direction
			const __m128 dlt = _mm_sub_ps(ae->t.translation, ae->predecessor->t.translation);

			const __m128 sq = _mm_dp_ps(dlt, dlt, 0b01111111);
			const __m128 s = _mm_sqrt_ps(sq);
			const __m128 div = _mm_max_ps(s, _mm_set1_ps(FABRIK_EPS_SIMD));
			const __m128 nrm = _mm_div_ps(dlt, div);

			// Move bone
			const __m128 fma = MM_FMSUB_PS(ae->bone_length, nrm, ae->t.translation);

			ae->predecessor->point_avg = _mm_sub_ps(ae->predecessor->point_avg, fma);
		}

		// backward
		// we did not move the root, therefore we can skip it
		for (int j = jointCount - 2; j >= 0; j--)
		{
			SolverJoint_SIMD *ae = &joints[jointOrder[j]];

			// Compute desired direction
			const __m128 dlt = _mm_sub_ps(ae->t.translation, ae->predecessor->t.translation);

			//Point nrm = normalize_SIMD(dlt);
			const __m128 sq = _mm_dp_ps(dlt, dlt, 0b01111111);
			const __m128 s = _mm_sqrt_ps(sq);
			const __m128 div = _mm_max_ps(s, _mm_set1_ps(FABRIK_EPS_SIMD));
			__m128 nrm = _mm_div_ps(dlt, div);

			// Constrain to axis and rotate
			nrm = constrain_SIMD(&ae->predecessor->t.rotation, nrm, ae->predecessor->c);
			ae->t.rotation = rotateTowards_SIMD(&ae->predecessor->t.rotation, nrm);

			// Move bone
			ae->t.translation = MM_FMADD_PS(ae->bone_length, nrm, ae->predecessor->t.translation);
			VALIDATE(Point_SIMD, ae->t.translation);
		}

		//old_error = current_error;
		//current_error = computeError_SIMD(jointCount, joints);
	}

	for (int i = 0; i < jointCount; i++)
	{
		_mm_store_ps(&output[i].translation.x, joints[i].t.translation);
		_mm_store_ps(&output[i].rotation.x.x, joints[i].t.rotation.x);
		_mm_store_ps(&output[i].rotation.y.x, joints[i].t.rotation.y);
		_mm_store_ps(&output[i].rotation.z.x, joints[i].t.rotation.z);
		VALIDATE(Transform_SIMD, &joints[i].t);
	}
}

void solve_SIMD(Scene* scene)
{
	OP_RESET();

	Armature *armature = scene->armature;
	const int jointCount = armature->jointCount;
	SolverJoint_SIMD *joints = (SolverJoint_SIMD*)malloc(sizeof(SolverJoint_SIMD) * jointCount);

	// compute predecessors and intialize the SolverJoints
	for (int i = 0; i < jointCount; i++)
	{
		joints[i].index = i;
		joints[i].successor_count = 0;
		joints[i].successor_filled = 0;
		joints[i].has_objective = 0;
		joints[i].point_avg = _mm_set1_ps(0.0);

		joints[i].c[0] = armature->joints[i].c.x;
		joints[i].c[1] = armature->joints[i].c.y;
		joints[i].c[2] = armature->joints[i].c.z;
		joints[i].c[3] = armature->joints[i].c.w;

		joints[i].t.translation = _mm_load_ps(&scene->initialPose[i].translation.x);
		joints[i].t.rotation.x = _mm_load_ps(&scene->initialPose[i].rotation.x.x);
		joints[i].t.rotation.y = _mm_load_ps(&scene->initialPose[i].rotation.y.x);
		joints[i].t.rotation.z = _mm_load_ps(&scene->initialPose[i].rotation.z.x);
		if (i != 0)
		{
			joints[i].predecessor = &joints[armature->joints[i].parent];
			joints[i].bone_length_squared = length_squared_SIMD(joints[i].t.translation, joints[i].predecessor->t.translation);
			joints[i].bone_length = _mm_sqrt_ps(joints[i].bone_length_squared);
		}
		else
		{
			joints[i].predecessor = NULL;
		}
	}
	for (int i = 0; i < jointCount; i++)
	{
		if (joints[i].predecessor != NULL)
		{
			joints[i].predecessor->successor_count++;
		}
	}
	for (int i = 0; i < jointCount; i++)
	{
		joints[i].successors = (SolverJoint_SIMD**)malloc(sizeof(SolverJoint_SIMD*) * joints[i].successor_count);
	}
	for (int i = 0; i < jointCount; i++)
	{
		if (joints[i].predecessor != NULL)
		{
			joints[i].predecessor_successor_index = joints[i].predecessor->successor_filled;
			joints[i].predecessor->successors[joints[i].predecessor->successor_filled] = &joints[i];
			joints[i].predecessor->successor_filled++;
		}
		else
		{
			joints[i].predecessor_successor_index = -1;
		}
	}

	// Compute topological ordering of joints
	int *jointOrder = (int*)malloc(sizeof(int) * jointCount);
	int jointOrderPos = 0;
	int *toposort = (int*)malloc(sizeof(int) * jointCount);
	for (int i = 0; i < jointCount; i++)
	{
		toposort[i] = joints[i].successor_count;
		if (toposort[i] == 0)
		{
			jointOrder[jointOrderPos++] = i;
		}
	}
	for (int i = 0; i < jointCount - 1; i++)
	{
		int parentIndex = joints[jointOrder[i]].predecessor->index;
		toposort[parentIndex]--;
		if (toposort[parentIndex] == 0)
		{
			jointOrder[jointOrderPos++] = parentIndex;
		}
	}
	free(toposort);
	assert(jointOrderPos == jointCount);
	assert(jointOrder[jointOrderPos - 1] == 0);

	// set objective joints
	for (int i = 0; i < armature->objectiveCount; i++)
	{
		joints[armature->objectives[i].joint].has_objective = 1;
	}

	///////////////////////////////////////////////////////////////////////////////

	// run FABRIK for each frame
	for (int f = 0; f < scene->frameCount; f++)
	{
		///////////////////////////////////////////////////////////////////////////////
		Target* targets = scene->targets[f];
		for (int i = 0; i < armature->objectiveCount; i++)
		{
			joints[armature->objectives[i].joint].current_target = _mm_load_ps(&targets[i].x);
		}
		Pose* output = scene->frames[f];
		doSolve_SIMD(jointCount, joints, jointOrder, output, scene->iterations);
		///////////////////////////////////////////////////////////////////////////////
	}

	// clean up
	free(jointOrder);
	for (int i = 0; i < jointCount; i++)
	{
		free(joints[i].successors);
	}
	free(joints);
}
