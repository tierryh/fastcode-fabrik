#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "include/solver.h"
#include "include/dataformat.h"
#include "include/algorithm.h"

void checkPoint(const Point* p, const char* label, int line)
{
	checkArray((float*)&p->x, 3, label, line);
}

void checkRotation(const Rotation* r, const char* label, int line)
{
	checkPoint(&r->x, label, line);
	checkPoint(&r->y, label, line);
	checkPoint(&r->z, label, line);
}

void checkTransform(const Transform* t, const char* label, int line)
{
	checkPoint(&t->translation, label, line);
	checkRotation(&t->rotation, label, line);
}

FLOAT length_squared(Point *p1, Point *p2)
{
	FLOAT dx, dy, dz;
	dx = p1->x - p2->x; OP_ADD(1); OP_RED(2*sizeof(float));
	dy = p1->y - p2->y; OP_ADD(1); OP_RED(2*sizeof(float));
	dz = p1->z - p2->z; OP_ADD(1); OP_RED(2*sizeof(float));
	return dx * dx + dy * dy + dz * dz; OP_ADD(2); OP_MUL(3);
}

FLOAT computeError(int jointCount, SolverJoint *joints)
{
	FLOAT current_error = 0;
	for (int i = 0; i < jointCount; i++)
	{
		OP_RED(sizeof(int));
		if (joints[i].has_objective)
		{
			// TODO: Probably better to just add them up (?)
			const FLOAT ls = length_squared(&joints[i].current_target, &joints[i].t.translation);
			current_error = max(ls, current_error);
		}
	}

	return current_error;
}

Point constrain(const Rotation* r, const Point* normal, FLOAT c[4])
{
	// Project onto axis (or, transform to local space)
	const FLOAT xDot = normal->x * r->x.x + normal->y * r->x.y + normal->z * r->x.z; OP_ADD(2); OP_MUL(3); OP_RED(6*sizeof(float));
	const FLOAT yDot = normal->x * r->y.x + normal->y * r->y.y + normal->z * r->y.z; OP_ADD(2); OP_MUL(3); OP_RED(6*sizeof(float));
	const FLOAT zDot = normal->x * r->z.x + normal->y * r->z.y + normal->z * r->z.z; OP_ADD(2); OP_MUL(3); OP_RED(6*sizeof(float));

	// Convert to polar coordinates
	const FLOAT angle = ACOS(xDot); OP_TRG(1);
	const FLOAT norm = SQRT(yDot * yDot + zDot * zDot); OP_ADD(1); OP_MUL(2); OP_SQR(1);

	OP_ADD(1);
	if (norm > EPS_THRES)
	{
		// Find ellipse axis'
		FLOAT a, b;
		if (yDot >= FLT(0.0) && zDot >= FLT(0.0)) { a = c[0]; b = c[1]; }
		else if (yDot >= FLT(0.0) && zDot < FLT(0.0)) { a = c[0]; b = c[3]; }
		else if (yDot < FLT(0.0) && zDot >= FLT(0.0)) { a = c[2]; b = c[1]; }
		else /* (yDot < FLT(0.0) && zDot < FLT(0.0)) */ { a = c[2]; b = c[3]; }
		OP_RED(2*sizeof(float));

		// "Project" onto ellipse.
		// This skews the result but works well enough.
		const FLOAT py = yDot / norm * a; OP_DIV(1); OP_MUL(1);
		const FLOAT pz = zDot / norm * b; OP_DIV(1); OP_MUL(1);

		/* Projecting properly will break hinge constraints
		const FLOAT el = atan2(a*zDot, b*yDot);
		const FLOAT py = a * COS(el);
		const FLOAT pz = b * SIN(el);
		*/

		// Compute final angle to check whether we need to constrain at all
		const FLOAT proj = SQRT(py * py + pz * pz); OP_ADD(1); OP_MUL(2); OP_SQR(1);
		OP_ADD(1);
		if (proj < EPS_THRES)
		{
			// If all constraints are 0 we can just use forward vector
			return r->x;
		}
		else
		{
			OP_ADD(1);
			if (angle > proj)
			{
				// Convert to euclidean space and normalise
				const FLOAT dot = COS(proj); OP_TRG(1);
				const FLOAT s = SQRT(FLT(1.0) - dot * dot) / proj; OP_ADD(1); OP_MUL(1); OP_DIV(1); OP_SQR(1);
				const FLOAT yd = py * s; OP_MUL(1);
				const FLOAT zd = pz * s; OP_MUL(1);

				// Project back into worldspace
				Point out;
				out.x = dot * r->x.x + yd * r->y.x + zd * r->z.x; OP_ADD(2); OP_MUL(3); OP_RED(3*sizeof(float));
				out.y = dot * r->x.y + yd * r->y.y + zd * r->z.y; OP_ADD(2); OP_MUL(3); OP_RED(3*sizeof(float));
				out.z = dot * r->x.z + yd * r->y.z + zd * r->z.z; OP_ADD(2); OP_MUL(3); OP_RED(3*sizeof(float));

				return out;
			}
		}
	}
	return *normal;
}

Rotation rotateTowards(const Rotation* r, const Point* normal)
{
	Rotation out;
	out.x = *normal;

	const FLOAT ydot = r->y.x * normal->x + r->y.y * normal->y + r->y.z * normal->z; OP_ADD(2); OP_MUL(3); OP_RED(6*sizeof(float));
	if (ydot < (FLT(1.0) - EPS_THRES))
	{
		// Project and normalize y axis
		const Point y = { r->y.x - normal->x * ydot, r->y.y - normal->y * ydot, r->y.z - normal->z * ydot }; OP_ADD(3); OP_MUL(3); OP_RED(6*sizeof(float));
		const FLOAT invNorm = FLT(1.0) / SQRT(y.x*y.x + y.y*y.y + y.z*y.z); OP_ADD(2); OP_MUL(3); OP_DIV(1); OP_SQR(1);

		out.y.x = y.x * invNorm; OP_MUL(1);
		out.y.y = y.y * invNorm; OP_MUL(1);
		out.y.z = y.z * invNorm; OP_MUL(1);
	}
	else
	{
		// Can just reflect forward axis for the y axis whem orthogonal
		const Point refl = { -r->x.x, -r->x.y, -r->x.z }; OP_RED(3*sizeof(float));
		out.y = refl;
	}

	// Cross product for vector base
	out.z.x = out.x.y * out.y.z - out.x.z * out.y.y; OP_ADD(1); OP_MUL(2);
	out.z.y = out.x.z * out.y.x - out.x.x * out.y.z; OP_ADD(1); OP_MUL(2);
	out.z.z = out.x.x * out.y.y - out.x.y * out.y.x; OP_ADD(1); OP_MUL(2);

	return out;
}


void doSolve(int jointCount, SolverJoint *joints, int *jointOrder, Pose *output, unsigned int iterations)
{
	//FLOAT old_error = FLT_MAX;
	//FLOAT current_error = 0;
	//current_error = computeError(jointCount, joints);
	// TODO: Always same amount of iterations for performance testing
	// while (current_error > FABRIK_EPS && fabs(old_error - current_error) > FABRIK_EPS)
	for(unsigned int i = 0; i < iterations; i++)
	{
		// forward
		for (int j = 0; j < jointCount-1; j++)
		{
			SolverJoint *ae = &joints[jointOrder[j]];

			// Add target to average count
			int newpos_count = ae->successor_count; OP_RED(sizeof(int));
			if (ae->has_objective)
			{
				ae->point_avg.x += ae->current_target.x; OP_ADD(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));
				ae->point_avg.y += ae->current_target.y; OP_ADD(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));
				ae->point_avg.z += ae->current_target.z; OP_ADD(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));
				newpos_count += 1;
			}

			// Calculate proper average
			//OP_CMP(1); integer comparison?
			Point* point = &ae->t.translation;
			if (newpos_count > 0)
			{
				point->x = ae->point_avg.x / newpos_count; OP_DIV(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));
				point->y = ae->point_avg.y / newpos_count; OP_DIV(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));
				point->z = ae->point_avg.z / newpos_count; OP_DIV(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));

				ae->point_avg.x = FLT(0.0); OP_WRT(sizeof(float));
				ae->point_avg.y = FLT(0.0); OP_WRT(sizeof(float));
				ae->point_avg.z = FLT(0.0); OP_WRT(sizeof(float));
			}

			// Compute desired direction
			const Point* prev = &ae->predecessor->t.translation;
			const Point delta = { point->x - prev->x, point->y - prev->y, point->z - prev->z }; OP_ADD(3); OP_RED(6*sizeof(float));
			const FLOAT norm = max(SQRT(delta.x * delta.x + delta.y * delta.y + delta.z * delta.z), FABRIK_EPS); OP_MAX(1); OP_ADD(2); OP_MUL(3); OP_SQR(1);
			Point nrm = { delta.x / norm, delta.y / norm, delta.z / norm }; OP_DIV(3);

			// TODO: If we want to rotate on forward pass we need to average rotation. let's just... not do that?
			// No rotations being computed of course also kills constraints since joints down the line need it.
			// Only doing it on backward pass decreases performance but it's probably alright for this project.
			//nrm = constrain(&ae->t.rotation, &nrm, c); // note that *if* we want to do it the constraints need to be flipped here!
			//ae->predecessor->t.rotation = rotateTowards(&ae->t.rotation, &nrm);

			// Move bone
			Point *prept = &ae->predecessor->point_avg;
			prept->x += point->x - ae->bone_length * nrm.x; OP_ADD(1); OP_MUL(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));
			prept->y += point->y - ae->bone_length * nrm.y; OP_ADD(1); OP_MUL(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));
			prept->z += point->z - ae->bone_length * nrm.z; OP_ADD(1); OP_MUL(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));
		}

		// backward
		// we did not move the root, therefore we can skip it
		for (int j = jointCount - 2; j >= 0; j--)
		{
			SolverJoint *ae = &joints[jointOrder[j]];

			// Compute desired direction
			const Point* point = &ae->t.translation;
			const Point* prev = &ae->predecessor->t.translation;
			const Point delta = { point->x - prev->x, point->y - prev->y, point->z - prev->z }; OP_ADD(3); OP_RED(6*sizeof(float));
			const FLOAT norm = max(SQRT(delta.x*delta.x + delta.y*delta.y + delta.z*delta.z), FABRIK_EPS); OP_MAX(1); OP_ADD(2); OP_MUL(3); OP_SQR(1);
			Point nrm = { delta.x / norm, delta.y / norm, delta.z / norm }; OP_DIV(3);

			// Constrain to axis and rotate
#ifdef FABRIK_CONSTRAINT
			nrm = constrain(&ae->predecessor->t.rotation, &nrm, ae->predecessor->c);
#endif
#ifdef FABRIK_ROTATION
			ae->t.rotation = rotateTowards(&ae->predecessor->t.rotation, &nrm); OP_WRT(sizeof(Rotation));
#endif

			// Move bone
			Point* res = &ae->t.translation;
			res->x = prev->x + ae->bone_length * nrm.x; OP_ADD(1); OP_MUL(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));
			res->y = prev->y + ae->bone_length * nrm.y; OP_ADD(1); OP_MUL(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));
			res->z = prev->z + ae->bone_length * nrm.z; OP_ADD(1); OP_MUL(1); OP_RED(sizeof(float)); OP_WRT(sizeof(float));
		}
		//old_error = current_error;
		//current_error = computeError(jointCount, joints);
	}
	for (int i = 0; i < jointCount; i++)
	{
		output[i] = joints[i].t; OP_RED(sizeof(Transform)); OP_WRT(sizeof(Transform));
		VALIDATE(Transform, &output[i]);
	}
}

void solve(Scene* scene)
{
	OP_RESET();

	Armature *armature = scene->armature;
	const int jointCount = armature->jointCount; OP_RED(sizeof(float));
	SolverJoint *joints = (SolverJoint*)malloc(sizeof(SolverJoint) * jointCount);

	// Scene
	OP_CPL(sizeof(Scene));
	OP_CPL(sizeof(Pose)*jointCount); // initialPose
	OP_CPL(sizeof(Target*)*scene->frameCount); // targets*
	OP_CPL(sizeof(Target)*scene->frameCount*armature->objectiveCount); // targets
	OP_CPL(sizeof(Pose*)*scene->frameCount); // frames*
	OP_CPL(sizeof(Pose)*scene->frameCount*jointCount); // frames

	// Armature
	OP_CPL(sizeof(Armature));
	OP_CPL(sizeof(Objective) * armature->objectiveCount); // Objectives
	OP_CPL(sizeof(Joint) * jointCount); // Joints

	// working set
	OP_CPL(sizeof(SolverJoint) * jointCount); // Working set

	// compute predecessors and intialize the SolverJoints
	for (int i = 0; i < jointCount; i++)
	{
		joints[i].index = i; OP_WRT(sizeof(int));
		joints[i].successor_count = 0; OP_WRT(sizeof(int));
		joints[i].successor_filled = 0; OP_WRT(sizeof(int));
		joints[i].has_objective = 0; OP_WRT(sizeof(int));
		joints[i].point_avg = PointIdentity; OP_WRT(sizeof(PointIdentity));
		joints[i].c[0] = armature->joints[i].c.x; OP_RED(sizeof(float)); OP_WRT(sizeof(float));
		joints[i].c[1] = armature->joints[i].c.y; OP_RED(sizeof(float)); OP_WRT(sizeof(float));
		joints[i].c[2] = armature->joints[i].c.z; OP_RED(sizeof(float)); OP_WRT(sizeof(float));
		joints[i].c[3] = armature->joints[i].c.w; OP_RED(sizeof(float)); OP_WRT(sizeof(float));

		joints[i].t = scene->initialPose[i]; OP_WRT(sizeof(Transform)); OP_RED(sizeof(Transform));
		if (i != 0)
		{
			joints[i].predecessor = &joints[armature->joints[i].parent]; OP_WRT(sizeof(SolverJoint*)); OP_RED(sizeof(SolverJoint*));
			joints[i].bone_length_squared = length_squared(&joints[i].t.translation, &joints[i].predecessor->t.translation); OP_WRT(sizeof(float)); OP_RED(2*sizeof(Point*));
			joints[i].bone_length = SQRT(joints[i].bone_length_squared); OP_WRT(sizeof(float)); OP_RED(sizeof(float));
		}
		else
		{
			joints[i].predecessor = NULL; OP_WRT(sizeof(SolverJoint*));
		}
	}
	for (int i = 0; i < jointCount; i++)
	{
		OP_RED(sizeof(SolverJoint*));
		if (joints[i].predecessor != NULL)
		{
			joints[i].predecessor->successor_count++; OP_WRT(sizeof(int));
		}
	}
	for (int i = 0; i < jointCount; i++)
	{
		joints[i].successors = (SolverJoint**)malloc(sizeof(SolverJoint*) * joints[i].successor_count); OP_WRT(sizeof(SolverJoint**));
	}
	for (int i = 0; i < jointCount; i++)
	{
		if (joints[i].predecessor != NULL)
		{
			joints[i].predecessor_successor_index = joints[i].predecessor->successor_filled; OP_RED(sizeof(int)); OP_WRT(sizeof(int));
			joints[i].predecessor->successors[joints[i].predecessor->successor_filled] = &joints[i]; OP_RED(sizeof(SolverJoint*)); OP_WRT(sizeof(SolverJoint*));
			joints[i].predecessor->successor_filled++; OP_WRT(sizeof(int));
		}
		else
		{
			joints[i].predecessor_successor_index = -1; OP_WRT(sizeof(int));
		}
	}

	// Compute topological ordering of joints
	int *jointOrder = (int*)malloc(sizeof(int) * jointCount);
	int jointOrderPos = 0;
	int *toposort = (int*)malloc(sizeof(int) * jointCount);
	for (int i = 0; i < jointCount; i++)
	{
		toposort[i] = joints[i].successor_count; OP_WRT(sizeof(int)); OP_RED(sizeof(int));
		if (toposort[i] == 0)
		{
			jointOrder[jointOrderPos++] = i; OP_WRT(sizeof(int));
		}
	}
	for (int i = 0; i < jointCount-1; i++)
	{
		int parentIndex = joints[jointOrder[i]].predecessor->index; OP_RED(sizeof(int));
		toposort[parentIndex]--; OP_WRT(sizeof(int));
		if (toposort[parentIndex] == 0)
		{
			jointOrder[jointOrderPos++] = parentIndex; OP_WRT(sizeof(int));
		}
	}
	free(toposort);
	assert(jointOrderPos == jointCount);
	assert(jointOrder[jointOrderPos-1] == 0);

	// set objective joints
	for (int i = 0; i < armature->objectiveCount; i++)
	{
		joints[armature->objectives[i].joint].has_objective = 1; OP_RED(sizeof(int)); OP_WRT(sizeof(float));
	}

	///////////////////////////////////////////////////////////////////////////////

	// run FABRIK for each frame
	for (int f = 0; f < scene->frameCount; f++)
	{
		///////////////////////////////////////////////////////////////////////////////
		Target* targets = scene->targets[f]; OP_RED(sizeof(Target*));
		for (int i = 0; i < armature->objectiveCount; i++)
		{
			joints[armature->objectives[i].joint].current_target = targets[i]; OP_RED(sizeof(Target)); OP_WRT(sizeof(Target));
		}
		Pose* output = scene->frames[f]; OP_RED(sizeof(Pose*));
		doSolve(jointCount, joints, jointOrder, output, scene->iterations);
		///////////////////////////////////////////////////////////////////////////////
	}

	// clean up
	free(jointOrder);
	for (int i = 0; i < jointCount; i++)
	{
		free(joints[i].successors);
	}
	free(joints);
}
