#include <iostream>
#include <igl/opengl/glfw/Viewer.h>
#include <igl/opengl/glfw/imgui/ImGuiMenu.h>
#include <igl/opengl/glfw/imgui/ImGuiHelpers.h>
#include <imgui/imgui.h>

extern "C" {
#include "include/armature.h"
#include "include/reader.h"
#include "include/solver.h"
#include "include/solverSIMD.h"
#include "include/solverMEM.h"
#include "include/solverFAST.h"
#include "include/solverCACHE.h"
#include "include/solverCONSTR.h"
#include "include/solverCACHE_MEM_CONSTR.h"
#include "include/solverCACHE_CONSTR_SIMD.h"
#include "include/solverCACHE_MEM_CONSTR_SIMD.h"
#include "include/solverFAST_CONSTR.h"
#include "include/solverFAST_CONSTR_PARALLEL.h"
#include "include/solverOther.h"
#include "include/solverOther_baseline.h"
}

#define EPS 1e-8
#define VERTS_PER_BONE 6
#define FACES_PER_BONE 8

using namespace std;
using Viewer = igl::opengl::glfw::Viewer;

// Current solver state
Args glArgs = {3, 3, 50, 8};
Scene* scene = nullptr;
float timer = 0.0;

// Current mesh state
Eigen::MatrixXd C;
Eigen::MatrixXd V;
Eigen::MatrixXi F;
std::vector<int32_t> parents;
std::vector<std::vector<std::tuple<Eigen::Vector3d, Eigen::Matrix3d>>> frames;

// Settings
float boneRadius = 0.1f;
float boneAnchor = 0.2f;
float maxBoneLength = 1.0f;
bool useColour = true;
bool isPlaying = false;
int32_t frame = 0;
float interval = 0.0f;

// In case we want hotkeys, here is the place!
bool callback_key_down(Viewer& viewer, unsigned char key, int modifiers)
{
    return true;
}

// Make unit orthogonal to a given vector.
Eigen::Vector3d makeOrthogonal(const Eigen::Vector3d& vector)
{
	// Cross product with 0,0,1
	const Eigen::Vector3d orth = Eigen::Vector3d(vector.y(), -vector.x(), 0.0);
	const FLOAT norm = orth.norm();
	if (norm < EPS)
	{
		// Could be any unit vector orthogonal to 0,0,1
		return Eigen::Vector3d(1.0, 0.0, 0.0);
	}
	return orth / norm;
}

// Safely make orthonormal from any vector diff and an orthonormal vector orth.
Eigen::Vector3d makeBase(const Eigen::Vector3d& diff, const Eigen::Vector3d& orth)
{
	const Eigen::Vector3d side = diff.cross(orth);
	const FLOAT norm = side.norm();
	if (norm < EPS)
	{
		return makeOrthogonal(orth);
	}
	return side / norm;
}


// Compute spherical vertex positions around a given bone
void generateEffector(int32_t index)
{
	// Grab root position
	const Eigen::Vector3d& position = std::get<0>(frames[frame][index]);

	// Compute vertices
	const int32_t vIndex = index * VERTS_PER_BONE;

	V.row(vIndex + 0) = position + Eigen::Vector3d(0.0, 0.0, +boneRadius);
	V.row(vIndex + 1) = position + Eigen::Vector3d(+boneRadius, 0.0, 0.0);
	V.row(vIndex + 2) = position + Eigen::Vector3d(0.0, +boneRadius, 0.0);
	V.row(vIndex + 3) = position + Eigen::Vector3d(-boneRadius, 0.0, 0.0);
	V.row(vIndex + 4) = position + Eigen::Vector3d(0.0, -boneRadius, 0.0);
	V.row(vIndex + 5) = position + Eigen::Vector3d(0.0, 0.0, -boneRadius);
}

// Compute given bone index vertex positions
void generateBone(int32_t index, int32_t parent)
{
	// Render point if referencing self
	if (index == parent)
	{
		generateEffector(index);
		return;
	}

	// Grab bone positions
	const Eigen::Vector3d& tail = std::get<0>(frames[frame][index]);
	const Eigen::Matrix3d& quat = std::get<1>(frames[frame][index]);
	const Eigen::Vector3d& head = std::get<0>(frames[frame][parent]);

	// Create orthonormal vectorbase
	const Eigen::Vector3d diff = tail - head;
	const Eigen::Vector3d forw = quat.row(0);
	const Eigen::Vector3d orth = quat.row(1);
	const Eigen::Vector3d side = quat.row(2);
	maxBoneLength = std::max(maxBoneLength, (float)diff.norm());

	// Compute anchor position between head and tail
	const Eigen::Vector3d anchor = head + diff * boneAnchor;

	// Compute vertices
	const int32_t vIndex = index * VERTS_PER_BONE;

	V.row(vIndex + 0) = head;
	V.row(vIndex + 1) = anchor + orth * boneRadius;
	V.row(vIndex + 2) = anchor + side * boneRadius;
	V.row(vIndex + 3) = anchor - orth * boneRadius;
	V.row(vIndex + 4) = anchor - side * boneRadius;
	V.row(vIndex + 5) = tail;
}

// Render current pose on the viewer
bool generateBones(Viewer& viewer)
{
	maxBoneLength = 0.0f;

	// Iterate through all bones
	const size_t n = parents.size();
	for (int32_t i = 0; i < n; i++)
	{
		generateBone(i, parents[i]);
	}

	// Update viewer
	viewer.data().clear();
	viewer.data().set_mesh(V, F);
	if (useColour)
	{
		viewer.data().set_colors(C);
	}
	return true;
}

// Generate faces in diamond shape and add colour
void generateFacesAndColour(int32_t index)
{
	// Compute vertex colour
	const int32_t vIndex = index * VERTS_PER_BONE;

	if (index == 0) // Root
	{
		C.row(vIndex + 0) = Eigen::Vector3d(1.0, 0.0, 0.0);
		C.row(vIndex + 1) = Eigen::Vector3d(0.0, 1.0, 0.0);
		C.row(vIndex + 2) = Eigen::Vector3d(0.0, 0.0, 1.0);
		C.row(vIndex + 3) = Eigen::Vector3d(1.0, 1.0, 0.0);
		C.row(vIndex + 4) = Eigen::Vector3d(0.0, 1.0, 1.0);
		C.row(vIndex + 5) = Eigen::Vector3d(1.0, 0.0, 1.0);
	}
	else if (parents[index] == index) // Objective
	{
		const Eigen::Vector3d single = Eigen::Vector3d(1.0, 0.0, 0.0);
		C.row(vIndex + 0) = single;
		C.row(vIndex + 1) = single;
		C.row(vIndex + 2) = single;
		C.row(vIndex + 3) = single;
		C.row(vIndex + 4) = single;
		C.row(vIndex + 5) = single;
	}
	else // Bone
	{
		const Eigen::Vector3d primary = Eigen::Vector3d(1.0, 1.0, 0.0);
		const Eigen::Vector3d secondary = Eigen::Vector3d(0.0, 1.0, 0.0);
		const Eigen::Vector3d direction = Eigen::Vector3d(1.0, 0.0, 0.0);
		C.row(vIndex + 0) = primary;
		C.row(vIndex + 1) = direction;
		C.row(vIndex + 2) = primary;
		C.row(vIndex + 3) = primary;
		C.row(vIndex + 4) = primary;
		C.row(vIndex + 5) = secondary;
	}

	// Compute faces
	const int32_t fIndex = index * FACES_PER_BONE;

	const Eigen::Vector3i base = Eigen::Vector3i::Constant(vIndex);
	F.row(fIndex + 0) = base + Eigen::Vector3i(0, 1, 2);
	F.row(fIndex + 1) = base + Eigen::Vector3i(0, 2, 3);
	F.row(fIndex + 2) = base + Eigen::Vector3i(0, 3, 4);
	F.row(fIndex + 3) = base + Eigen::Vector3i(0, 4, 1);
	F.row(fIndex + 4) = base + Eigen::Vector3i(5, 2, 1);
	F.row(fIndex + 5) = base + Eigen::Vector3i(5, 3, 2);
	F.row(fIndex + 6) = base + Eigen::Vector3i(5, 4, 3);
	F.row(fIndex + 7) = base + Eigen::Vector3i(5, 1, 4);
}

// Allocates armature data and initialises pose-independent properties based on parent list
void generateArmature()
{
	// Initialise vertices and faces
	const size_t n = parents.size();
	C = Eigen::MatrixXd::Zero(VERTS_PER_BONE * n, 3);
	V = Eigen::MatrixXd::Zero(VERTS_PER_BONE * n, 3);
	F = Eigen::MatrixXi::Zero(FACES_PER_BONE * n, 3);
	for (int32_t i = 0; i < n; i++)
	{
		generateFacesAndColour(i);
	}
}

void solveArmature(Viewer& viewer)
{
	if (!scene) return;

	// Solve scene
	//solve_CONSTR(scene);
	//solve_CACHE(scene);
	//solve_FAST(scene);
	//solve_CACHE_MEM_CONSTR(scene);
	solve_CACHE_CONSTR_SIMD(scene);
	//solve_CACHE_MEM_CONSTR_SIMD(scene);
	//solve(scene);
	//solve_FAST_CONSTR_PARALLEL(scene);
	
	// solve_SIMD(scene);
	//solve_Other(scene);
	//solveOtherBaseline(scene);

	// Load parents
	const Armature* armature = scene->armature;
	parents.resize(armature->jointCount + armature->objectiveCount);
	for (int i = 0; i < armature->jointCount; i++)
	{
		parents[i] = armature->joints[i].parent;
	}
	// Target point are rendered like self-referencing joints
	for (int i = 0; i < armature->objectiveCount; i++)
	{
		parents[armature->jointCount + i] = armature->jointCount + i;
	}

	// Load frames
	frames.clear();
	for (int i = 0; i < scene->frameCount; i++)
	{
		std::vector<std::tuple<Eigen::Vector3d, Eigen::Matrix3d>> frame;
		frame.resize(armature->jointCount + armature->objectiveCount);
		for (int j = 0; j < armature->jointCount; j++)
		{
			Pose& pose = scene->frames[i][j];
			std::get<0>(frame[j]) = Eigen::Vector3d((FLOAT)pose.translation.x, (FLOAT)pose.translation.y, (FLOAT)pose.translation.z);
			Eigen::Matrix3d rotation;
			rotation <<
				(FLOAT)pose.rotation.x.x, (FLOAT)pose.rotation.x.y, (FLOAT)pose.rotation.x.z,
				(FLOAT)pose.rotation.y.x, (FLOAT)pose.rotation.y.y, (FLOAT)pose.rotation.y.z,
				(FLOAT)pose.rotation.z.x, (FLOAT)pose.rotation.z.y, (FLOAT)pose.rotation.z.z;
			std::get<1>(frame[j]) = rotation;
		}
		for (int j = 0; j < armature->objectiveCount; j++)
		{
			Target& target = scene->targets[i][j];
			const int index = armature->jointCount + j;

			std::get<0>(frame[index]) = Eigen::Vector3d((FLOAT)target.x, (FLOAT)target.y, (FLOAT)target.z);
			std::get<1>(frame[index]) = Eigen::Quaterniond::Identity();
		}
		frames.push_back(frame);
	}

	// Generate skeleton
	generateArmature();
	generateBones(viewer);
}

void openFile(Viewer& viewer, const std::string& filename)
{
	// If another scene already exist, deallocate it first
	if(scene) deleteScene(scene);

	// Read and run solver
	scene = readScene(filename.c_str(), &glArgs);
	//scene = makeScene(&glArgs);
	solveArmature(viewer);
}

bool tick(Viewer& viewer)
{
	if (scene && isPlaying)
	{
		if (timer <= 0.0f)
		{
			timer = interval;
			frame = (frame + 1) % scene->frameCount;
			generateBones(viewer);
		}
		else
		{
			timer -= 1.0 / viewer.core.animation_max_fps;
		}
	}

	return false;
}

// TODO allow joint subdivision also for visualization
int main(int argc, char *argv[])
{
	// Load armature
	std::string filename;
	if (argc == 2)
	{
		filename = std::string(argv[1]);
	}
	else
	{
		filename = std::string("default");
	}

	/*
	// Do not start viewer on specific option
	if (filename == "-")
	{
		Scene* scene = readScene(filename.c_str(), &glArgs);
		solve_FAST(scene);
		return 0;
	}
	*/

	// Initialise viewer
	Viewer viewer;
	viewer.callback_key_down = callback_key_down;
	viewer.callback_pre_draw = &tick;

	openFile(viewer, filename);

	// Set up viewer
	viewer.core.align_camera_center(V, F);
	viewer.core.lighting_factor = 0.3f;

	// Add menu
	igl::opengl::glfw::imgui::ImGuiMenu menu;
	viewer.plugins.push_back(&menu);

	// Add content to the default menu window
	menu.callback_draw_viewer_menu = [&]()
	{
		if (ImGui::CollapsingHeader("File", ImGuiTreeNodeFlags_DefaultOpen))
		{
			if (ImGui::Button("Open Armature"))
			{
				std::string fname = igl::file_dialog_open();
				if (fname.length() == 0) return;
				openFile(viewer, fname);
			}

			if (ImGui::Button("Default Armature"))
			{
				std::string fname = "default";
				openFile(viewer, fname);
			}
		}

		if (ImGui::CollapsingHeader("Settings", ImGuiTreeNodeFlags_DefaultOpen))
		{
			if (ImGui::SliderFloat("Radius", &boneRadius, 0.0f, maxBoneLength / 4, "%.4f", 1.0f)) { generateBones(viewer); }
			if (ImGui::SliderFloat("Anchor", &boneAnchor, 0.0f, 1.0f, "%.4f", 1.0f)) { generateBones(viewer);  }
			if (ImGui::Checkbox("Use Colours", &useColour)) { generateBones(viewer); }
			if (ImGui::InputInt("Iterations", &glArgs.maxIterations, 1, 100)) { openFile(viewer, filename); };
			if (ImGui::InputInt("Frame Subdivisions", &glArgs.frameSubdivisions, 0, 50)) { openFile(viewer, filename); };
			if (ImGui::InputInt("Joint Subdivisions", &glArgs.jointSubdivisions, 0, 16)) { openFile(viewer, filename); };
			if (ImGui::InputInt("Tree depth", &glArgs.treeDepth, 1, 25)) { openFile(viewer, filename); };
		}

		if (ImGui::CollapsingHeader("Frames", ImGuiTreeNodeFlags_DefaultOpen))
		{
			if (ImGui::SliderInt("Frame", &frame, 0.0f, frames.size()-1, "%d")) { generateBones(viewer); }
			if (ImGui::SliderFloat("frame duration", &interval, 0.0f, 0.2f, "%.4f", 1.0f)) { }
			if (ImGui::Checkbox("Animate", &isPlaying)) { viewer.core.is_animating = isPlaying; }
		}
	};

	viewer.launch();

	// Clean up if scene was loaded
	if (scene) deleteScene(scene);
}
