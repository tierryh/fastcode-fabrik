#include "include/validation.h"
#include <math.h> 
#include <stdio.h>

void initHistory()
{
	hasError = 1;
	history = (Record*)malloc(sizeof(Record) * VALIDATION_BUFFER);
}

void deleteHistory()
{
	free(history);
}

void startRecording()
{
	historySize = 0;
	isRecording = 1;
	hasError = 0;
}

void startValidation()
{
	historyPointer = 0;
	isRecording = 0;
	hasError = 0;
}

void check(FLOAT f, const char* label, int line)
{
	if (hasError)
	{
		return;
	}

	int value = (int)floorf(f / VALIDATION_EPS);
	if (isRecording)
	{
		// Expand history if too small
		if (historySize < VALIDATION_BUFFER)
		{
			// Store value
			Record record;
			record.value = value;
			history[historySize] = record;
			historySize++;
		}
	}
	else
	{
		// Make sure we aren't getting too many values
		if (historyPointer >= VALIDATION_BUFFER)
		{
			return;
		}
		else if (historyPointer >= historySize)
		{
			printf("[%d] %s <%d>: History buffer overflow. \n", historyPointer, label, line);
			hasError = 1;
		}
		else
		{
			// Check match
			const Record record = history[historyPointer];
			if (record.value != value)
			{
				const FLOAT truth = VALIDATION_EPS * record.value;
				const FLOAT actual = VALIDATION_EPS * value;
				printf("[%d] %s <%d>: Values do not match! Truth: %.3f Actual: %.3f \n", historyPointer, label, line, truth, actual);
				hasError = 1;
			}

			historyPointer++;
		}
	}
}


void checkArray(FLOAT* f, int n, const char* label, int line)
{
	for (int i = 0; i < n; i++)
	{
		check(f[i], label, line);
	}
}
