#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "include/reader.h"

Scene* readScene(const char* filename, const Args* args)
{
	if (strcmp(filename, "default") == 0)
	{
		return makeScene(args);
	}

	Scene* scene = (Scene*)malloc(sizeof(Scene));

	scene->iterations = args->maxIterations;

	FILE *fp;
	fp = fopen(filename, "r");

	if (fp == NULL)
	{
		printf("Could not read file %s\n", filename);
		return NULL;
	}

	int joints = 0;
	int targets = 0;
	int frames = 0;

	// Read object count
	fscanf(fp, "%d %d %d", &joints, &targets, &frames);
	assert(joints > 0);
	assert(targets > 0);
	assert(frames > 0);

	int offset = 0;
	const int totalJoints = joints * (args->jointSubdivisions + 1) + 1;
	const int totalFrames = (frames - 1) * args->frameSubdivisions + frames;

	// Allocate armature memory
	Armature *armature = createArmature(totalJoints, targets);
	scene->frameCount = totalFrames;
	scene->armature = armature;
	scene->initialPose = createPose(armature);
	scene->targets = createTargets(armature, totalFrames);
	scene->frames = createPoses(armature, totalFrames);

	// Initialise root
	Joint* root = armature->joints;
	root->parent = 0;

	Point c;
	fscanf(fp, "%f %f %f %f", &c.x, &c.y, &c.z, &c.w);

	// Convert from degrees to radians
	root->c.x = (FLOAT)(c.x / 180.0 * PI);
	root->c.y = (FLOAT)(c.y / 180.0 * PI);
	root->c.z = (FLOAT)(c.z / 180.0 * PI);
	root->c.w = (FLOAT)(c.w / 180.0 * PI);

	// Read joints
	for (int i = 1; i < joints+1; i++)
	{
		const int index = i * (args->jointSubdivisions + 1);

		int parent;
		fscanf(fp, "%d %f %f %f %f", &parent, &c.x, &c.y, &c.z, &c.w);
		assert(parent >= 0 && parent < joints+1);
		armature->joints[index].parent = parent * (args->jointSubdivisions + 1);
		for (int j = 0; j < 4; j++)
		{
			// Convert from degrees to radians
			armature->joints[index].c.x = (FLOAT)(c.x / 180.0 * PI);
			armature->joints[index].c.y = (FLOAT)(c.y / 180.0 * PI);
			armature->joints[index].c.z = (FLOAT)(c.z / 180.0 * PI);
			armature->joints[index].c.w = (FLOAT)(c.w / 180.0 * PI);
		}
	}

	// Read targets
	for (int i = 0; i < targets; i++)
	{
		int joint;
		fscanf(fp, "%d", &joint);
		assert(joint > 0 && joint < joints+1);
		armature->objectives[i].joint = joint * (args->jointSubdivisions + 1);
	}

	// Initialise root
	scene->initialPose[0] = TransformIdentity;

	// Read joint positions
	for (int i = 1; i < joints+1; i++)
	{
		const int index = i * (args->jointSubdivisions + 1);

		double x, y, z;
		fscanf(fp, "%lf %lf %lf", &x, &y, &z);
		Point* point = &scene->initialPose[index].translation;
		point->x = (FLOAT)x;
		point->y = (FLOAT)y;
		point->z = (FLOAT)z;

		// First solver iteration will fix the rotation
		scene->initialPose[index].rotation = RotationIdentity;
	}

	// Interpolate inbetweens
	for (int i = 1; i < joints + 1; i++)
	{
		const int next = i * (args->jointSubdivisions + 1);
		const Point* b = &scene->initialPose[next].translation;
		const int prev = armature->joints[next].parent;
		const Point* a = &scene->initialPose[prev].translation;

		int parent = prev;
		for (int j = 1; j <= args->jointSubdivisions; j++)
		{
			// Subdivisions are stored below the owning joint
			const int index = next - (args->jointSubdivisions + 1) + j;
			armature->joints[index].parent = parent;

			// Interpolate properties
			const FLOAT alpha = ((FLOAT)j) / (args->jointSubdivisions + 1);
			Point* point = &scene->initialPose[index].translation;
			point->x = a->x + alpha * (b->x - a->x);
			point->y = a->y + alpha * (b->y - a->y);
			point->z = a->z + alpha * (b->z - a->z);
			scene->initialPose[index].rotation = RotationIdentity;

			armature->joints[index].c.x = armature->joints[prev].c.x + alpha * (armature->joints[next].c.x - armature->joints[prev].c.x);
			armature->joints[index].c.y = armature->joints[prev].c.y + alpha * (armature->joints[next].c.y - armature->joints[prev].c.y);
			armature->joints[index].c.z = armature->joints[prev].c.z + alpha * (armature->joints[next].c.z - armature->joints[prev].c.z);
			armature->joints[index].c.w = armature->joints[prev].c.w + alpha * (armature->joints[next].c.w - armature->joints[prev].c.w);

			// Go up tree to assign parents correctly
			parent = index;
		}

		// Reparent next joint to subdivision chain
		armature->joints[next].parent = parent;
	}

	// Read frames
	for (int i = 0; i < frames; i++)
	{
		const int base = i * (args->frameSubdivisions + 1);

		// Read target positions
		for (int j = 0; j < targets; j++)
		{
			double x, y, z;
			fscanf(fp, "%lf %lf %lf", &x, &y, &z);
			scene->targets[base][j].x = (FLOAT)x;
			scene->targets[base][j].y = (FLOAT)y;
			scene->targets[base][j].z = (FLOAT)z;
		}

		// Interpolate linearly between frames
		if (i > 0)
		{
			for (int j = 0; j < targets; j++)
			{
				const int prevBase = base - (args->frameSubdivisions + 1);
				const Point prev = scene->targets[prevBase][j];
				const Point next = scene->targets[base][j];

				// Create interpolated frames
				for (int k = 1; k <= args->frameSubdivisions; k++)
				{
					const int frame = prevBase + k;
					const FLOAT alpha = ((FLOAT)k / (args->frameSubdivisions + 1));
					scene->targets[frame][j].x = prev.x - (prev.x - next.x) * alpha;
					scene->targets[frame][j].y = prev.y - (prev.y - next.y) * alpha;
					scene->targets[frame][j].z = prev.z - (prev.z - next.z) * alpha;
				}
			}
		}
	}

	fclose(fp);

	return scene;
}

void deleteScene(Scene* scene)
{
	deletePoses(scene->frames, scene->frameCount);
	deleteTargets(scene->targets, scene->frameCount);
	deletePose(scene->initialPose);
	deleteArmature(scene->armature);
	free(scene);
}


Scene* makeScene(const Args* args)
{
	const int depth = args->treeDepth;
	Scene* scene = (Scene*)malloc(sizeof(Scene));

	scene->iterations = args->maxIterations;
	
	int joints = (2 << depth) - 2; // Don't count root
	int targets = (1 << depth);
	int frames = 4;

	int offset = 0;
	const int totalJoints = joints * (args->jointSubdivisions + 1) + 1;
	const int totalFrames = (frames - 1) * args->frameSubdivisions + frames;

	// Allocate armature memory
	Armature * armature = createArmature(totalJoints, targets);
	scene->frameCount = totalFrames;
	scene->armature = armature;
	scene->initialPose = createPose(armature);
	scene->targets = createTargets(armature, totalFrames);
	scene->frames = createPoses(armature, totalFrames);

	// Initialise root
	Joint * root = armature->joints;
	root->parent = 0;

	const FLOAT rootAngle = FLT(0.0);
	const Point c = { rootAngle, rootAngle, rootAngle, rootAngle };

	// Convert from degrees to radians
	root->c.x = (FLOAT)(c.x / 180.0 * PI);
	root->c.y = (FLOAT)(c.y / 180.0 * PI);
	root->c.z = (FLOAT)(c.z / 180.0 * PI);
	root->c.w = (FLOAT)(c.w / 180.0 * PI);

	const FLOAT maxAngle = FLT(90.0);

	// Read joints
	for (int i = 1; i < joints + 1; i++)
	{
		const int index = i * (args->jointSubdivisions + 1);

		const int parent = (i - 1) / 2;
		const FLOAT angle = (maxAngle * i) / (joints + 1);
		const Point d = { angle, angle, angle, angle };

		assert(parent >= 0 && parent < joints + 1);
		armature->joints[index].parent = parent * (args->jointSubdivisions + 1);
		for (int j = 0; j < 4; j++)
		{
			// Convert from degrees to radians
			armature->joints[index].c.x = (FLOAT)(d.x / 180.0 * PI);
			armature->joints[index].c.y = (FLOAT)(d.y / 180.0 * PI);
			armature->joints[index].c.z = (FLOAT)(d.z / 180.0 * PI);
			armature->joints[index].c.w = (FLOAT)(d.w / 180.0 * PI);
		}
	}

	// Read targets
	for (int i = 0; i < targets; i++)
	{
		const int joint = joints - i; // joints is already -1
		assert(joint > 0 && joint < joints + 1);
		armature->objectives[i].joint = joint * (args->jointSubdivisions + 1);
	}

	// Initialise root
	scene->initialPose[0] = TransformIdentity;

	// Read joint positions
	for (int i = 1; i < joints + 1; i++)
	{
		const int index = i * (args->jointSubdivisions + 1);

		// Extract depth
		const int d = (int)log2((double)(i + 1));
		double x = 0.0, y = 1.0 * d, z = 0.0;

		Point * point = &scene->initialPose[index].translation;
		point->x = (FLOAT)x;
		point->y = (FLOAT)y;
		point->z = (FLOAT)z;

		// First solver iteration will fix the rotation
		scene->initialPose[index].rotation = RotationIdentity;
	}

	// Interpolate inbetweens
	for (int i = 1; i < joints + 1; i++)
	{
		const int next = i * (args->jointSubdivisions + 1);
		const Point * b = &scene->initialPose[next].translation;
		const int prev = armature->joints[next].parent;
		const Point * a = &scene->initialPose[prev].translation;

		int parent = prev;
		for (int j = 1; j <= args->jointSubdivisions; j++)
		{
			// Subdivisions are stored below the owning joint
			const int index = next - (args->jointSubdivisions + 1) + j;
			armature->joints[index].parent = parent;

			// Interpolate properties
			const FLOAT alpha = ((FLOAT)j) / (args->jointSubdivisions + 1);
			Point * point = &scene->initialPose[index].translation;
			point->x = a->x + alpha * (b->x - a->x);
			point->y = a->y + alpha * (b->y - a->y);
			point->z = a->z + alpha * (b->z - a->z);
			scene->initialPose[index].rotation = RotationIdentity;

			armature->joints[index].c.x = armature->joints[prev].c.x + alpha * (armature->joints[next].c.x - armature->joints[prev].c.x);
			armature->joints[index].c.y = armature->joints[prev].c.y + alpha * (armature->joints[next].c.y - armature->joints[prev].c.y);
			armature->joints[index].c.z = armature->joints[prev].c.z + alpha * (armature->joints[next].c.z - armature->joints[prev].c.z);
			armature->joints[index].c.w = armature->joints[prev].c.w + alpha * (armature->joints[next].c.w - armature->joints[prev].c.w);

			// Go up tree to assign parents correctly
			parent = index;
		}

		// Reparent next joint to subdivision chain
		armature->joints[next].parent = parent;
	}

	// Read frames
	for (int i = 0; i < frames; i++)
	{
		const int base = i * (args->frameSubdivisions + 1);

		// Read target positions
		for (int j = 0; j < targets; j++)
		{
			double x = 1.0 * (j - targets / 2) * (1.0 - 0.9 * (double)i / frames);
			double y = depth + 1;
			double z = 0.2 * (i - frames / 2);
			scene->targets[base][j].x = (FLOAT)x;
			scene->targets[base][j].y = (FLOAT)y;
			scene->targets[base][j].z = (FLOAT)z;
		}

		// Interpolate linearly between frames
		if (i > 0)
		{
			for (int j = 0; j < targets; j++)
			{
				const int prevBase = base - (args->frameSubdivisions + 1);
				const Point prev = scene->targets[prevBase][j];
				const Point next = scene->targets[base][j];

				// Create interpolated frames
				for (int k = 1; k <= args->frameSubdivisions; k++)
				{
					const int frame = prevBase + k;
					const FLOAT alpha = ((FLOAT)k / (args->frameSubdivisions + 1));
					scene->targets[frame][j].x = prev.x - (prev.x - next.x) * alpha;
					scene->targets[frame][j].y = prev.y - (prev.y - next.y) * alpha;
					scene->targets[frame][j].z = prev.z - (prev.z - next.z) * alpha;
				}
			}
		}
	}

	return scene;
}