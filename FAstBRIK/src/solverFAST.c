#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "include/solverFAST.h"
#include "include/dataformat.h"
#include "include/algorithm.h"

void checkPoint_FAST(const Point* p, const char* label, int line)
{
	checkArray((float*)&p->x, 3, label, line);
}

void checkRotation_FAST(const Rotation* r, const char* label, int line)
{
	checkPoint_FAST(&r->x, label, line);
	checkPoint_FAST(&r->y, label, line);
	checkPoint_FAST(&r->z, label, line);
}

void checkTransform_FAST(const Transform* t, const char* label, int line)
{
	checkPoint_FAST(&t->translation, label, line);
	checkRotation_FAST(&t->rotation, label, line);
}

INLINE FLOAT length_squared_FAST(Point *p1, Point *p2)
{
	FLOAT dx, dy, dz;
	dx = p1->x - p2->x;
	dy = p1->y - p2->y;
	dz = p1->z - p2->z;
	return dx * dx + dy * dy + dz * dz;
}

INLINE FLOAT computeError_FAST(Armature_FAST* armature)
{
	FLOAT current_error = 0;
	for (int i = 0; i < armature->objectiveCount; i++)
	{
		// Add target to approrpiate chain
		const int chain = armature->targetChains[i];
		Point* tr = &armature->targets[chain];

		const int joint = armature->chains[chain].tail;
		Point* ps = &armature->transforms[joint].translation;

		const FLOAT ls = length_squared_FAST(tr, ps);
		current_error = max(ls, current_error);
	}
	return current_error;
}

INLINE Point constrain_FAST(const Rotation* r, const Point* normal, const Point* c)
{
	// Project onto axis (or, transform to local space)
	const FLOAT xDot = normal->x * r->x.x + normal->y * r->x.y + normal->z * r->x.z;
	const FLOAT yDot = normal->x * r->y.x + normal->y * r->y.y + normal->z * r->y.z;
	const FLOAT zDot = normal->x * r->z.x + normal->y * r->z.y + normal->z * r->z.z;

	// Convert to polar coordinates
	const FLOAT angle = ACOS(xDot);
	const FLOAT norm = SQRT(yDot * yDot + zDot * zDot);

	if (norm > EPS_THRES)
	{
		// Find ellipse axis'
		FLOAT a, b;
		if (yDot >= FLT(0.0) && zDot >= FLT(0.0)) { a = c->x; b = c->y; }
		else if (yDot >= FLT(0.0) && zDot < FLT(0.0)) { a = c->x; b = c->w; }
		else if (yDot < FLT(0.0) && zDot >= FLT(0.0)) { a = c->z; b = c->y; }
		else /* (yDot < FLT(0.0) && zDot < FLT(0.0)) */ { a = c->z; b = c->w; }

		// "Project" onto ellipse.
		// This skews the result but works well enough.
		const FLOAT py = yDot / norm * a;
		const FLOAT pz = zDot / norm * b;

		// Compute final angle to check whether we need to constrain at all
		const FLOAT proj = SQRT(py * py + pz * pz);
		if (proj < EPS_THRES)
		{
			// If all constraints are 0 we can just use forward vector
			return r->x;
		}
		else
		{
			if (angle > proj)
			{
				// Convert to euclidean space and normalise
				const FLOAT dot = COS(proj);
				const FLOAT s = SQRT(FLT(1.0) - dot * dot) / proj;
				const FLOAT yd = py * s;
				const FLOAT zd = pz * s;

				// Project back into worldspace
				Point out;
				out.x = dot * r->x.x + yd * r->y.x + zd * r->z.x;
				out.y = dot * r->x.y + yd * r->y.y + zd * r->z.y;
				out.z = dot * r->x.z + yd * r->y.z + zd * r->z.z;

				return out;
			}
		}
	}
	return *normal;
}

INLINE Rotation rotateTowards_FAST(const Rotation* r, const Point* normal)
{
	Rotation out;
	out.x = *normal;

	const FLOAT ydot = r->y.x * normal->x + r->y.y * normal->y + r->y.z * normal->z;
	if (ydot < (FLT(1.0) - EPS_THRES))
	{
		// Project and normalize y axis
		const Point y = { r->y.x - normal->x * ydot, r->y.y - normal->y * ydot, r->y.z - normal->z * ydot };
		const FLOAT invNorm = FLT(1.0) / SQRT(y.x*y.x + y.y*y.y + y.z*y.z);

		out.y.x = y.x * invNorm;
		out.y.y = y.y * invNorm;
		out.y.z = y.z * invNorm;
	}
	else
	{
		// Can just reflect forward axis for the y axis whem orthogonal
		const Point refl = { -r->x.x, -r->x.y, -r->x.z };
		out.y = refl;
	}

	// Cross product for vector base
	out.z.x = out.x.y * out.y.z - out.x.z * out.y.y;
	out.z.y = out.x.z * out.y.x - out.x.x * out.y.z;
	out.z.z = out.x.x * out.y.y - out.x.y * out.y.x;

	return out;
}

Point computeNormal_FAST(const Point* a, const Point* b)
{
	const Point delta = { b->x - a->x, b->y - a->y, b->z - a->z };
	const FLOAT invNorm = FLT(1.0) / max(SQRT(delta.x * delta.x + delta.y * delta.y + delta.z * delta.z), FABRIK_EPS_FAST);
	const Point out = { delta.x * invNorm, delta.y * invNorm, delta.z * invNorm };
	return out;
}

Point computeBone_FAST(const Point* point, const Point* nrm, FLOAT length)
{
	Point out;
	out.x = point->x - length * nrm->x;
	out.y = point->y - length * nrm->y;
	out.z = point->z - length * nrm->z;
	return out;
}

Point forward_FAST(Armature_FAST* armature, int chain, const SolverJoint_FAST* joint, const Point* prev)
{
	const SolverChain_FAST* ch = &armature->chains[chain];
	Point* point = &armature->transforms[ch->tail].translation;
	const SolverJoint_FAST* pr = &armature->joints[ch->tail];

	// Branch out
	Point avg = armature->targets[chain];
	for (int i = 0; i < ch->numb; i++)
	{
		const Point acc = forward_FAST(armature, ch->succ + i, pr, point);
		avg.x += acc.x;
		avg.y += acc.y;
		avg.z += acc.z;
	}

	// Calculate proper average
	point->x = avg.x * pr->weight;
	point->y = avg.y * pr->weight;
	point->z = avg.z * pr->weight;

	// Compute all along chain
	for (int k = ch->tail - 1; k >= ch->head; k--)
	{
		const SolverJoint_FAST* ae = &armature->joints[k];

		// Compute normal from previous bone and move
		Point* pred = &armature->transforms[k].translation;
		const Point nrm = computeNormal_FAST(pred, point);
		*pred = computeBone_FAST(point, &nrm, ae->length);
		point = pred;
	}

	// Compute normal from root bone and return predecessor
	const Point nrm = computeNormal_FAST(prev, point);
	return computeBone_FAST(point, &nrm, joint->length);
}

void backward_FAST(Armature_FAST* armature, int chain, const SolverJoint_FAST* joint, const Transform* transform)
{
	const SolverChain_FAST* ch = &armature->chains[chain];

	// Grab previous chain
	for (int k = ch->head; k <= ch->tail; k++)
	{
		SolverJoint_FAST* ae = &armature->joints[k];
		Transform* at = &armature->transforms[k];

		// Compute desired direction
		const Point* point = &at->translation;
		const Point* prev = &transform->translation;
		Point nrm = computeNormal_FAST(prev, point);

		// Constrain to axis and rotate
#ifdef FABRIK_CONSTRAINT
		nrm = constrain_FAST(&transform->rotation, &nrm, &joint->c);
#endif
#ifdef FABRIK_ROTATION
		at->rotation = rotateTowards_FAST(&transform->rotation, &nrm);
#endif

		// Move bone
		at->translation = computeBone_FAST(prev, &nrm, -ae->length);

		// Drag joint with
		joint = ae;
		transform = at;
	}

	// Branch out
	for (int i = 0; i < ch->numb; i++)
	{
		backward_FAST(armature, ch->succ + i, joint, transform);
	}
}


void doSolve_FAST(Armature_FAST* armature, Pose *output, unsigned int iterations)
{
	//FLOAT old_error = FLT_MAX;
	//FLOAT current_error = 0;
	
	//current_error = computeError_FAST(armature);
	// TODO: Always same amount of iterations for performance testing
	// while (current_error > FABRIK_EPS && fabs(old_error - current_error) > FABRIK_EPS)
	for(unsigned int i = 0; i < iterations; i++)
	{
		const Transform* root = &armature->transforms[0]; 
		const SolverJoint_FAST* joint = &armature->joints[0];

		// Do forward and backward pass recursively
		forward_FAST(armature, 0, joint, &root->translation);
		backward_FAST(armature, 0, joint, root);

		//old_error = current_error;
		//current_error = computeError_FAST(armature);
	}

	for (int i = 0; i < armature->jointCount; i++)
	{
		output[i] = armature->transforms[i];
		VALIDATE(Transform_FAST, &output[i]);
	}
}



void solve_FAST(Scene* scene)
{
	Armature *armature = scene->armature;
	int jointCount = armature->jointCount;
	int objectiveCount = armature->objectiveCount;

	// Store number of children and targets for each joint
	int* childCount = (int*)malloc(sizeof(int) * jointCount);
	int* targetCount = (int*)malloc(sizeof(int) * jointCount);
	for (int i = 0; i < jointCount; i++)
	{
		childCount[i] = 0;
		targetCount[i] = 0;
	}

	for (int i = 0; i < objectiveCount; i++)
	{
		targetCount[armature->objectives[i].joint]++;
	}

	for (int i = 0; i < jointCount; i++)
	{
		const int parent = armature->joints[i].parent;
		if (parent >= 0)
		{
			childCount[parent]++;
		}
	}

	// Allocate children and reset count to rebuild
	int** children = (int**)malloc(sizeof(int*) * jointCount);
	int** targets = (int**)malloc(sizeof(int*) * jointCount);
	for (int i = 0; i < jointCount; i++)
	{
		children[i] = NULL;
		const int cCount = childCount[i];
		childCount[i] = 0;
		if (cCount > 0)
		{
			children[i] = (int*)malloc(sizeof(int) * cCount);
		}

		targets[i] = NULL;
		const int tCount = targetCount[i];
		targetCount[i] = 0;
		if (tCount > 0)
		{
			targets[i] = (int*)malloc(sizeof(int) * tCount);
		}
	}

	// Assign children
	for (int i = 0; i < jointCount; i++)
	{
		const int parent = armature->joints[i].parent;
		if (parent >= 0)
		{
			const int index = childCount[parent];

			children[parent][index] = i;
			if (parent != i) // ignore root
			{
				childCount[parent]++;
			}
		}
	}

	// Count chains (always count root)
	int chainCount = 1;
	for (int i = 0; i < jointCount; i++)
	{
		const int cCount = childCount[i];
		if (cCount > 1 || targetCount[i] > 0)
		{
			chainCount += cCount;
		}
	}

	// Assign targets
	for (int i = 0; i < objectiveCount; i++)
	{
		const int index = armature->objectives[i].joint;
		targets[index][targetCount[index]] = i;
		targetCount[index]++;
	}

	// Allocate chain armature
	Armature_FAST arm;
	arm.joints = (SolverJoint_FAST*)malloc(sizeof(SolverJoint_FAST) * jointCount);
	arm.transforms = (Transform*)malloc(sizeof(Transform) * jointCount);
	arm.jointCount = jointCount;
	jointCount = 0;

	arm.chains = (SolverChain_FAST*)malloc(sizeof(SolverChain_FAST) * chainCount);
	arm.targets = (Point*)malloc(sizeof(Point) * chainCount);
	int* queue = (int*)malloc(sizeof(int) * chainCount);
	arm.chainCount = chainCount;

	// Create target mappings
	arm.targetChains = (int*)malloc(sizeof(int) * objectiveCount);
	arm.objectiveCount = objectiveCount;

	// Create root chain
	arm.chains[0].head = 0;
	arm.targets[0] = PointIdentity;
	chainCount = 1;

	// FIFO
	queue[0] = 0;
	int top = 1, bottom = 0;
	while (bottom < top)
	{
		const int chain = queue[bottom++];
		SolverChain_FAST* ch = &arm.chains[chain];

		// Assign proper chain-joint index
		int joint = ch->head;
		ch->head = jointCount;

		// Build chain
		int cCount, tCount;
		for(;;)
		{
			cCount = childCount[joint];
			tCount = targetCount[joint];

			// Move along chain
			ch->tail = jointCount++;

			// Get next bone
			Joint* ae = &armature->joints[joint];

			// Set joint settings
			arm.joints[ch->tail].c = ae->c;
			arm.transforms[ch->tail] = scene->initialPose[joint];

			const int pred = max(armature->joints[joint].parent, 0); // Parent root bone with itself
			arm.joints[ch->tail].length = SQRT(length_squared_FAST(&scene->initialPose[joint].translation, &scene->initialPose[pred].translation));
			arm.joints[ch->tail].weight = FLT(1.0) / (cCount + tCount);

			// Go to next joint if chain continues
			if (cCount != 1 || tCount > 0) break;
			joint = children[joint][0];
		}

		// Add children to queue
		ch->numb = cCount;
		ch->succ = chainCount;
		for (int i = 0; i < cCount; i++)
		{
			SolverChain_FAST* child = &arm.chains[chainCount];
			child->head = children[joint][i];
			queue[top++] = chainCount;
			chainCount++;
		}

		// Assign and initialise targets
		arm.targets[chain] = PointIdentity;
		for (int i = 0; i < tCount; i++)
		{
			arm.targetChains[targets[joint][i]] = chain;
		}
	}
	free(queue);

	// Make sure root isn't touched
	arm.chains[0].head = 1;
	arm.joints[0].weight = FLT(0.0);

	// Initialisation cleanup
	for (int i = 0; i < jointCount; i++)
	{
		if(children[i])	free(children[i]);
		if(targets[i]) free(targets[i]);
	}
	free(children);
	free(targets);
	free(childCount);
	free(targetCount);

	// run FABRIK for each frame
	for (int f = 0; f < scene->frameCount; f++)
	{
		///////////////////////////////////////////////////////////////////////////////
		Target* targets = scene->targets[f];

		for (int i = 0; i < arm.chainCount; i++)
		{
			arm.targets[i] = PointIdentity;
		}
		for (int i = 0; i < armature->objectiveCount; i++)
		{
			// Add target to approrpiate chain
			const int chain = arm.targetChains[i];
			Point* tr = &arm.targets[chain];
			tr->x += targets[i].x;
			tr->y += targets[i].y;
			tr->z += targets[i].z;
		}
		Pose* output = scene->frames[f];
		doSolve_FAST(&arm, output, scene->iterations);
		///////////////////////////////////////////////////////////////////////////////
	}

	// clean up
	free(arm.targetChains);
	free(arm.targets);
	free(arm.transforms);
	free(arm.chains);
	free(arm.joints);
}
