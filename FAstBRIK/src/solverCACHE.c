#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <stdint.h>

#include "include/solverCACHE.h"
#include "include/dataformat.h"
#include "include/algorithm.h"

void checkPoint_CACHE(const Point* p, const char* label, int line)
{
	checkArray((float*)&p->x, 3, label, line);
}

void checkRotation_CACHE(const Rotation* r, const char* label, int line)
{
	checkPoint_CACHE(&r->x, label, line);
	checkPoint_CACHE(&r->y, label, line);
	checkPoint_CACHE(&r->z, label, line);
}

void checkTransform_CACHE(const Transform* t, const char* label, int line)
{
	checkPoint_CACHE(&t->translation, label, line);
	checkRotation_CACHE(&t->rotation, label, line);
}

FLOAT length_squared_CACHE(Point *p1, Point *p2)
{
	FLOAT dx, dy, dz;
	dx = p1->x - p2->x;
	dy = p1->y - p2->y;
	dz = p1->z - p2->z;
	return dx * dx + dy * dy + dz * dz;
}

FLOAT computeError_CACHE(int jointCount, SolverJoint_CACHE*joints)
{
	FLOAT current_error = 0;
	for (int i = 0; i < jointCount; i++)
	{
		// TODO: Can be achieved without using "has objective"
		/*
		if (joints[i].has_objective)
		{
			// TODO: Probably better to just add them up (?)
			const FLOAT ls = length_squared_CACHE(&joints[i].current_target, &joints[i].t.translation);
			current_error = max(ls, current_error);
		}
		*/
	}

	return current_error;
}

Point constrain_CACHE(const Rotation* r, const Point* normal, Point* c)
{
	// Project onto axis (or, transform to local space)
	const FLOAT xDot = normal->x * r->x.x + normal->y * r->x.y + normal->z * r->x.z;
	const FLOAT yDot = normal->x * r->y.x + normal->y * r->y.y + normal->z * r->y.z;
	const FLOAT zDot = normal->x * r->z.x + normal->y * r->z.y + normal->z * r->z.z;

	// Convert to polar coordinates
	const FLOAT angle = ACOS(xDot);
	const FLOAT norm = SQRT(yDot * yDot + zDot * zDot);
	if (norm > EPS_THRES)
	{
		// Find ellipse axis'
		FLOAT a, b;
		if (yDot >= FLT(0.0) && zDot >= FLT(0.0)) { a = c->x; b = c->y; }
		else if (yDot >= FLT(0.0) && zDot < FLT(0.0)) { a = c->x; b = c->w; }
		else if (yDot < FLT(0.0) && zDot >= FLT(0.0)) { a = c->z; b = c->y; }
		else /* (yDot < FLT(0.0) && zDot < FLT(0.0)) */ { a = c->z; b = c->w; }

		// "Project" onto ellipse.
		// This skews the result but works well enough.
		const FLOAT py = yDot / norm * a;
		const FLOAT pz = zDot / norm * b;

		/* Projecting properly will break hinge constraints
		const FLOAT el = atan2(a*zDot, b*yDot);
		const FLOAT py = a * COS(el);
		const FLOAT pz = b * SIN(el);
		*/

		// Compute final angle to check whether we need to constrain at all
		const FLOAT proj = SQRT(py * py + pz * pz);
		if (proj < EPS_THRES)
		{
			// If all constraints are 0 we can just use forward vector
			return r->x;
		}
		else
		{
			if (angle > proj)
			{
				// Convert to euclidean space and normalise
				const FLOAT dot = COS(proj);
				const FLOAT s = SQRT(FLT(1.0) - dot * dot) / proj;
				const FLOAT yd = py * s;
				const FLOAT zd = pz * s;

				// Project back into worldspace
				Point out;
				out.x = dot * r->x.x + yd * r->y.x + zd * r->z.x;
				out.y = dot * r->x.y + yd * r->y.y + zd * r->z.y;
				out.z = dot * r->x.z + yd * r->y.z + zd * r->z.z;

				return out;
			}
		}
	}
	return *normal;
}

Rotation rotateTowards_CACHE(const Rotation* r, const Point* normal)
{
	Rotation out;
	out.x = *normal;

	const FLOAT ydot = r->y.x * normal->x + r->y.y * normal->y + r->y.z * normal->z;
	if (ydot < (FLT(1.0) - EPS_THRES))
	{
		// Project and normalize y axis
		const Point y = { r->y.x - normal->x * ydot, r->y.y - normal->y * ydot, r->y.z - normal->z * ydot };
		const FLOAT invNorm = FLT(1.0) / SQRT(y.x*y.x + y.y*y.y + y.z*y.z);

		out.y.x = y.x * invNorm;
		out.y.y = y.y * invNorm;
		out.y.z = y.z * invNorm;
	}
	else
	{
		// Can just reflect forward axis for the y axis whem orthogonal
		const Point refl = { -r->x.x, -r->x.y, -r->x.z };
		out.y = refl;
	}

	// Cross product for vector base
	out.z.x = out.x.y * out.y.z - out.x.z * out.y.y;
	out.z.y = out.x.z * out.y.x - out.x.x * out.y.z;
	out.z.z = out.x.x * out.y.y - out.x.y * out.y.x;

	return out;
}


void doSolve_CACHE(Armature_CACHE* armature, int *jointOrder, Pose *output, unsigned int iterations)
{
	//FLOAT old_error = FLT_MAX;
	//FLOAT current_error = 0;
	//current_error = computeError_CACHE(armature->jointCount, armature->joints);
	// TODO: Always same amount of iterations for performance testing
	// while (current_error > FABRIK_EPS && fabs(old_error - current_error) > FABRIK_EPS)
	for(unsigned int i = 0; i < iterations; i++)
	{
		// forward
		for (int j = 0; j < armature->jointCount-1; j++)
		{
			const int index = jointOrder[j];
			SolverJoint_CACHE* ae = &armature->joints[index];

			// Calculate proper average
			Point* point = &armature->transforms[index].translation;
			point->x = ae->point_avg.x * ae->weight;
			point->y = ae->point_avg.y * ae->weight;
			point->z = ae->point_avg.z * ae->weight;
			ae->point_avg = ae->current_target;

			// Compute desired direction
			const Point* prev = &armature->transforms[ae->predecessor].translation;
			const Point delta = { point->x - prev->x, point->y - prev->y, point->z - prev->z };
			const FLOAT norm = max(SQRT(delta.x * delta.x + delta.y * delta.y + delta.z * delta.z), FABRIK_EPS_CACHE);
			Point nrm = { delta.x / norm, delta.y / norm, delta.z / norm };

			// Move bone
			Point *prept = &armature->joints[ae->predecessor].point_avg;
			prept->x += point->x - ae->bone_length * nrm.x;
			prept->y += point->y - ae->bone_length * nrm.y;
			prept->z += point->z - ae->bone_length * nrm.z;
		}

		// backward
		// we did not move the root, therefore we can skip it
		for (int j = armature->jointCount - 2; j >= 0; j--)
		{
			const int index = jointOrder[j];
			SolverJoint_CACHE* ae = &armature->joints[index];
			Transform* at = &armature->transforms[index];
			const Transform* pt = &armature->transforms[ae->predecessor];

			// Compute desired direction
			const Point* point = &at->translation;
			const Point* prev = &pt->translation;
			const Point delta = { point->x - prev->x, point->y - prev->y, point->z - prev->z };
			const FLOAT norm = max(SQRT(delta.x*delta.x + delta.y*delta.y + delta.z*delta.z), FABRIK_EPS_CACHE);
			Point nrm = { delta.x / norm, delta.y / norm, delta.z / norm };

			// Constrain to axis and rotate
#ifdef FABRIK_CONSTRAINT
			nrm = constrain_CACHE(&pt->rotation, &nrm, &armature->joints[ae->predecessor].c);
#endif
#ifdef FABRIK_ROTATION
			at->rotation = rotateTowards_CACHE(&pt->rotation, &nrm);
#endif

			// Move bone
			Point* res = &at->translation;
			res->x = prev->x + ae->bone_length * nrm.x;
			res->y = prev->y + ae->bone_length * nrm.y;
			res->z = prev->z + ae->bone_length * nrm.z;
		}
		//old_error = current_error;
		//current_error = computeError_CACHE(armature->jointCount, armature->joints);
	}
	for (int i = 0; i < armature->jointCount; i++)
	{
		output[i] = armature->transforms[i];
		VALIDATE(Transform_CACHE, &output[i]);
	}
}

void solve_CACHE(Scene* scene)
{
	Armature *armature = scene->armature;

	Armature_CACHE arm;
	arm.jointCount = armature->jointCount;

	void* jointsMem = malloc(sizeof(SolverJoint_CACHE) * arm.jointCount + 63);
	arm.joints = (SolverJoint_CACHE*)((((uintptr_t)jointsMem + 63) & ~(uintptr_t)63));

	void* transformsMem = malloc(sizeof(Transform) * arm.jointCount + 63);
	arm.transforms = (Transform*)((((uintptr_t)transformsMem + 63) & ~(uintptr_t)63));

	int** children = (int**)malloc(sizeof(int*) * arm.jointCount);
	int* childCount = (int*)malloc(sizeof(int) * arm.jointCount);

	// compute predecessors and intialize the SolverJoints
	for (int i = 0; i < arm.jointCount; i++)
	{
		childCount[i] = 0;

		arm.joints[i].current_target = PointIdentity;
		arm.joints[i].point_avg = PointIdentity;
		arm.joints[i].c = armature->joints[i].c;
		arm.transforms[i] = scene->initialPose[i];
		if (i != 0)
		{
			arm.joints[i].predecessor = armature->joints[i].parent;
			arm.joints[i].bone_length = SQRT(length_squared_CACHE(&arm.transforms[i].translation, &arm.transforms[arm.joints[i].predecessor].translation));
		}
		else
		{
			arm.joints[i].predecessor = 0;
		}
	}
	for (int i = 0; i < arm.jointCount; i++)
	{
		if (arm.joints[i].predecessor != i)
		{
			childCount[arm.joints[i].predecessor]++;
		}
	}
	for (int i = 0; i < arm.jointCount; i++)
	{
		children[i] = (int*)malloc(sizeof(int) * childCount[i]);
		childCount[i] = 0;
	}
	for (int i = 0; i < arm.jointCount; i++)
	{
		const int pred = arm.joints[i].predecessor;
		if (pred != i)
		{
			children[pred][childCount[pred]] = i;
			childCount[pred]++;
		}
	}

	// Compute topological ordering of joints
	int *jointOrder = (int*)malloc(sizeof(int) * arm.jointCount);
	int jointOrderPos = 0;
	int *toposort = (int*)malloc(sizeof(int) * arm.jointCount);
	for (int i = 0; i < arm.jointCount; i++)
	{
		toposort[i] = childCount[i];
		if (toposort[i] == 0)
		{
			jointOrder[jointOrderPos++] = i;
		}
	}
	for (int i = 0; i < arm.jointCount-1; i++)
	{
		int parentIndex = arm.joints[jointOrder[i]].predecessor;
		toposort[parentIndex]--;
		if (toposort[parentIndex] == 0)
		{
			jointOrder[jointOrderPos++] = parentIndex;
		}
	}
	free(toposort);
	assert(jointOrderPos == arm.jointCount);
	assert(jointOrder[jointOrderPos-1] == 0);

	for (int i = 0; i < arm.jointCount; i++)
	{
		const int newpos_count = childCount[i];
		if (newpos_count > 0)
		{
			arm.joints[i].weight = FLT(1.0) / newpos_count;
		}
		else
		{
			arm.joints[i].weight = FLT(0.0);
		}
	}

	// set objective joints
	for (int i = 0; i < armature->objectiveCount; i++)
	{
		const int index = armature->objectives[i].joint;
		const int newpos_count = childCount[index];
		arm.joints[index].weight = FLT(1.0) / (newpos_count + 1);
	}

	///////////////////////////////////////////////////////////////////////////////

	// run FABRIK for each frame
	for (int f = 0; f < scene->frameCount; f++)
	{
		///////////////////////////////////////////////////////////////////////////////
		Target* targets = scene->targets[f];
		for (int i = 0; i < armature->objectiveCount; i++)
		{
			SolverJoint_CACHE* ae = &arm.joints[armature->objectives[i].joint];
			ae->current_target = targets[i];
			ae->point_avg = ae->current_target;
		}
		Pose* output = scene->frames[f];
		doSolve_CACHE(&arm, jointOrder, output, scene->iterations);
		///////////////////////////////////////////////////////////////////////////////
	}

	// clean up
	free(jointOrder);
	for (int i = 0; i < arm.jointCount; i++)
	{
		free(children[i]);
	}
	free(children);
	free(childCount);

	free(jointsMem);
	free(transformsMem);
}
