#include <stdlib.h>
#include <math.h>

#include "include/armature.h"

const Point Forward = { FLT(1.0), FLT(0.0), FLT(0.0), FLT(0.0) };
const Point Right = { FLT(0.0), FLT(1.0), FLT(0.0), FLT(0.0) };
const Point Up = { FLT(0.0), FLT(0.0), FLT(1.0), FLT(0.0) };

const Point PointIdentity = { FLT(0.0), FLT(0.0), FLT(0.0), FLT(0.0) };
const Rotation RotationIdentity = { FLT(1.0), FLT(0.0), FLT(0.0), FLT(0.0), FLT(0.0), FLT(1.0), FLT(0.0), FLT(0.0), FLT(0.0), FLT(0.0), FLT(1.0), FLT(0.0) };
const Transform TransformIdentity = { FLT(0.0), FLT(0.0), FLT(0.0), FLT(0.0), FLT(1.0), FLT(0.0), FLT(0.0), FLT(0.0), FLT(0.0), FLT(1.0), FLT(0.0), FLT(0.0), FLT(0.0), FLT(0.0), FLT(1.0), FLT(0.0) };

Armature* createArmature(int jointCount, int objectiveCount)
{
	Armature* armature = (Armature*)malloc(sizeof(Armature));
	armature->jointCount = jointCount;
	armature->joints = (Joint*)malloc(sizeof(Joint) * jointCount);
	armature->objectiveCount = objectiveCount;
	armature->objectives = (Objective*)malloc(sizeof(Objective) * objectiveCount);
	return armature;
}

void deleteArmature(Armature* armature)
{
	free(armature->joints);
	free(armature->objectives);
	free(armature);
}

Pose* createPose(const Armature* armature)
{
	const int n = armature->jointCount;
	Pose* pose = (Pose*)malloc(sizeof(Pose) * n);
	return pose;
}

Pose** createPoses(const Armature* armature, int frames)
{
	const int n = armature->jointCount;
	Pose** poses = (Pose**)malloc(sizeof(Pose*) * frames);
	for (int i = 0; i < frames; i++)
	{
		poses[i] = createPose(armature);
	}
	return poses;
}

void deletePoses(Pose** poses, int frames)
{
	for (int i = 0; i < frames; i++)
	{
		deletePose(poses[i]);
	}
	free(poses);
}

void deletePose(Pose* pose)
{
	free(pose);
}

Target** createTargets(const Armature* armature, int frames)
{
	const int n = armature->objectiveCount;
	Target** targets = (Target**)malloc(sizeof(Target*) * frames);
	for (int i = 0; i < frames; i++)
	{
		targets[i] = (Target*)malloc(sizeof(Target) * n);
	}
	return targets;
}

void deleteTargets(Target** targets, int frames)
{
	for (int i = 0; i < frames; i++)
	{
		deleteTarget(targets[i]);
	}
	free(targets);
}

void deleteTarget(Target* target)
{
	free(target);
}

void randomiseArmature(Armature* armature)
{
	const int n = armature->jointCount;
	armature->joints[0].parent = 0;
	for (int i = 1; i < n; i++)
	{
		// Assign random parent from already connected pool
		armature->joints[i].parent = rand() % i;
	}

	const int m = armature->objectiveCount;
	for (int i = 0; i < m; i++)
	{
		// Assign random target joint that isn't root
		armature->objectives[i].joint = 1 + (rand() % (n-1));
	}
}

void randomisePoints(Point* points, int n)
{
	for (int i = 0; i < n; i++)
	{
		Point* point = &points[i];
		point->x = (FLOAT)(rand()) / (FLOAT)(RAND_MAX);
		point->y = (FLOAT)(rand()) / (FLOAT)(RAND_MAX);
		point->z = (FLOAT)(rand()) / (FLOAT)(RAND_MAX);
	}
}

FLOAT ssd(const Point* a, const Point* b)
{
	const Point d = {a->x - b->x, a->y - b->y, a->z - b->z, FLT(0.0)};
	return d.x* d.x + d.y * d.y + d.z * d.z;
}