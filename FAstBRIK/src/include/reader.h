#ifndef READER_H
#define READER_H

#include "armature.h"

#ifndef MIN
#define MIN(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#define FILENAME_BUF_LEN 256

/**
 * Scene reading arguments
 */
typedef struct Args {

	/**
	 * Number of frame subdivisions inserted in between each frame.
	 */
	int frameSubdivisions;

	/**
	 * Number of joint subdivisions inserted in between each joint.
	 */
	int jointSubdivisions;

	/**
	 * The total number of iterations per frame.
	 */
	int maxIterations;

	/**
	 * The binary tree depth for "default" armature
	 */
	int treeDepth;

} Args;

/**
 * A scene, containing all information read from an .arm file
 */
typedef struct Scene {

	/**
	 * Input initial pose.
	 */
	Pose* initialPose;

	/**
	 * Array of targets for each frame
	 */
	Target** targets;

	/**
	 * Output poses for each frame.
	 */
	Pose** frames;

	/**
	 * The total number of frames being animated.
	 */
	int frameCount;

	/**
	 * The total number of iterations per frame.
	 */
	int iterations;

	/**
	 * The armature object being animated
	 */
	Armature* armature;
} Scene;

Scene* readScene(const char* filename, const Args* args); // Read scene from a file
void deleteScene(Scene* scene); // Deallocate whole scene

Scene* makeScene(const Args* args); // Generates a test armature

#endif /* !READER_H */
