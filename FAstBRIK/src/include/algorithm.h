#ifndef _WIN32
// GCC does not support min() and max(), instead it supports fmin() and fmax() according to C99.
// Visual Studio does not support fmin() and fmax() (at least that's what stackoverflow says about versions 2011 and 2012).
// C++ defines min() and max() in <algorithm> but in C there does not seem to be an algorithm.h - hence we use this hack.
#define min(a, b) fmin(a, b)
#define max(a, b) fmax(a, b)
#endif
