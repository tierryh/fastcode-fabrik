#ifndef ARMATURE_H
#define ARMATURE_H

#ifdef WIN32
	#define INLINE inline
	#define MM_FMADD_PS(a, b, c) _mm_fmadd_ps(a, b, c)
	#define MM_FMSUB_PS(a, b, c) _mm_fmsub_ps(a, b, c)
#elif defined __APPLE__
	#define INLINE __attribute__((always_inline))
	#define MM_FMADD_PS(a, b, c) _mm_add_ps(_mm_mul_ps(a, b), c)
	#define MM_FMSUB_PS(a, b, c) _mm_sub_ps(_mm_mul_ps(a, b), c)
#else
	#define INLINE __always_inline
	#define MM_FMADD_PS(a, b, c) _mm_fmadd_ps(a, b, c)
	#define MM_FMSUB_PS(a, b, c) _mm_fmsub_ps(a, b, c)
#endif

#define SINGLE
#ifdef SINGLE

	#define FLOAT float
	#define FLT(N) N##f
	#define COS(N) cosf(N)
	#define ACOS(N) acosf(N)
	#define SIN(N) sinf(N)
	#define ASIN(N) asinf(N)
	#define SQRT(N) sqrtf(N)
	#define LOG(N) logf(N)
	#define FLT_MAX 1e9

#else

	#define FLOAT double
	#define FLT(N) N
	#define COS(N) cos(N)
	#define ACOS(N) acos(N)
	#define SIN(N) sin(N)
	#define ASIN(N) asin(N)
	#define SQRT(N) sqrt(N)
	#define LOG(N) log(N)
	#define FLT_MAX 1e99

#endif // SINGLE

#define EPS_THRES FLT(0.00001)
#define INV_SQRT_TWO FLT(0.70710678119)
#define PI FLT(3.141592653589793)

/**
 * A 3D-point in cartesian coordinates
 */
typedef struct Point {
  FLOAT x, y, z, w;
} Point;
extern const Point PointIdentity;
extern const Point Forward;
extern const Point Right;
extern const Point Up;

typedef Point Target;

/**
 * Represents a joint rotation.
 */
typedef struct Rotation {
	/**
	 * Vector basis
	 */
	Point x, y, z;
} Rotation;
extern const Rotation RotationIdentity;

/**
 * Represents a joint transform.
 */
typedef struct Transform {
	/**
	 * Joint position
	 */
	Point translation;
	/**
	 * Joint rotation
	 */
	Rotation rotation;
} Transform;
extern const Transform TransformIdentity;

typedef Transform Pose;

/**
 * Represents a joint in the armature.
 */
typedef struct Joint {
  /**
   * Parent index
   */
	int parent;
  /**
   * Joint limits, as defined in the paper
   */
  Point c;
} Joint;

/**
 * Represents an objective of a given joint.
 */
typedef struct Objective {
	/**
	 * Joint index
	 */
	int joint;
} Objective;

/**
 * A figure, supporting multiple end effectors, targets and joint limits.
 */
typedef struct Armature {
  /**
   * The total number of objectives in this figure.
   */
  int objectiveCount;
	/**
	 * The total number of joints in this figure.
	 */
	int jointCount;
	/**
	 * The array of objectives
	 */
	Objective* objectives;
  /**
   * The array of joints, root is at 0
   */
	Joint* joints;
} Armature;

/**
 * Helper functions.
 */
Armature* createArmature(int jointCount, int objectiveCount); // Allocate armature
void deleteArmature(Armature* armature); // Deallocate armature

Pose* createPose(const Armature* armature); // Allocate single pose of appropriate size
Pose** createPoses(const Armature* armature, int frames); // Allocate pose of appropriate size
void deletePoses(Pose** poses, int frames); // Deallocate poses
void deletePose(Pose* pose); // Deallocate pose

Target** createTargets(const Armature* armature, int frames); // Allocate objectives of appropriate size
void deleteTargets(Target** points, int frames); // Deallocate targets
void deleteTarget(Target* point); // Deallocate target

void randomiseArmature(Armature* armature); // Create random joint parenting
void randomisePoints(Point* points, int n); // Fill points with random position in [0,1]^3

FLOAT ssd(const Point* a, const Point* b); // Compares two points, returns square difference

#endif /* !ARMATURE_H */
