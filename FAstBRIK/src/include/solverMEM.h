#ifndef SOLVERMEM_H
#define SOLVERMEM_H

#include "reader.h"
#include "ops.h"

#define FABRIK_EPS_MEM FLT(0.001)

typedef struct SolverJoint_MEM {
	int index;
	int successor_count;
	int successor_filled;
	int has_objective;
	FLOAT bone_length_squared;
	FLOAT bone_length;
	FLOAT c[4];
	struct SolverJoint_MEM **successors;
	struct SolverJoint_MEM *predecessor;
	Transform t;
	Target current_target;
	Point point_avg;
} SolverJoint_MEM;

void solve_MEM(Scene* scene); // Solve scene using FABRIK, store result in frames

#endif /* !SOLVERMEM_H */
