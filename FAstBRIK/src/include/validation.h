#ifndef VALIDATION_H
#define VALIDATION_H

#include "armature.h"

#include <immintrin.h>

typedef struct Record {
	int value;
} Record;

#ifdef FABRIK_OP_VALIDATE
#define VALIDATE(T,X) check##T(X, __FILE__, __LINE__)
#else
	#define VALIDATE(T,X)
#endif

#define VALIDATION_EPS FLT(0.001)
#define VALIDATION_BUFFER 500000

Record* history;
int historySize;
int historyPointer;
int isRecording;
int hasError;

void initHistory();
void deleteHistory();
void startRecording();
void startValidation();
void check(FLOAT f, const char* label, int line);
void checkArray(FLOAT* f, int n, const char* label, int line);

#endif
