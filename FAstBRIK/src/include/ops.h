#ifndef OPS_H
#define OPS_H

#include "validation.h"

#define FABRIK_CONSTRAINT
#define FABRIK_ROTATION

#ifdef FABRIK_OP_COUNT
	struct
	{
		unsigned long long int add_op_count;
		unsigned long long int mul_op_count;
		unsigned long long int div_op_count;
		unsigned long long int sqrt_op_count;
		unsigned long long int trig_op_count;
		unsigned long long int max_op_count;
		unsigned long long int tot_op_count;

		unsigned long long int read_op_count;
		unsigned long long int write_op_count;
		unsigned long long int compl_op_count;

	} ops;

	#define OP_ADD(N) ops.add_op_count += N; OP_CST(N)
	#define OP_MUL(N) ops.mul_op_count += N; OP_CST(N)
	#define OP_DIV(N) ops.div_op_count += N; OP_CST(N)
	#define OP_SQR(N) ops.sqrt_op_count += N; OP_CST(N)
	#define OP_TRG(N) ops.trig_op_count += N; OP_CST(N)
	#define OP_MAX(N) ops.max_op_count += N; OP_CST(N)
	#define OP_CST(N) ops.tot_op_count += N

	#define OP_RED(N) ops.read_op_count += N
	#define OP_WRT(N) ops.write_op_count += N
	#define OP_CPL(N) ops.compl_op_count += N
	#define OP_RESET() \
		ops.add_op_count = 0; \
		ops.mul_op_count = 0; \
		ops.div_op_count = 0; \
		ops.sqrt_op_count = 0; \
		ops.trig_op_count = 0; \
		ops.max_op_count = 0; \
		ops.tot_op_count = 0; \
		ops.read_op_count = 0; \
		ops.write_op_count = 0; \
		ops.compl_op_count = 0;
#else
	#define OP_ADD(N)
	#define OP_MUL(N)
	#define OP_DIV(N)
	#define OP_SQR(N)
	#define OP_TRG(N)
	#define OP_MAX(N)
	#define OP_CST(N)

	#define OP_RED(N)
	#define OP_WRT(N)
	#define OP_CPL(N)
	#define OP_RESET()
#endif

#endif /* !OPS_H */
