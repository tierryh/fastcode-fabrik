#ifndef SOLVERCACHE_MEM_CONSTR_H
#define SOLVERCACHE_MEM_CONSTR_H

#include "reader.h"
#include "ops.h"

#define FABRIK_EPS_CACHE_MEM_CONSTR FLT(0.001)

typedef struct SolverJoint_CACHE_MEM_CONSTR {

	Point c;
	FLOAT bone_length;
	int predecessor;
	Target current_target;
	Point point_avg;
	FLOAT weight;

	FLOAT padding;

} SolverJoint_CACHE_MEM_CONSTR;


typedef struct Armature_CACHE_MEM_CONSTR {

	int jointCount;
	Transform* transforms;
	SolverJoint_CACHE_MEM_CONSTR* joints;

} Armature_CACHE_MEM_CONSTR;

void solve_CACHE_MEM_CONSTR(Scene* scene); // Solve scene using FABRIK, store result in frames

#endif /* !SOLVERCACHE_MEM_CONSTR_H */
