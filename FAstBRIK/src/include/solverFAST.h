#ifndef SOLVERFAST_H
#define SOLVERFAST_H

#include "reader.h"
#include "ops.h"

#define FABRIK_EPS_FAST FLT(0.001)


typedef struct SolverJoint_FAST 
{
	Point c; // Constraint values
	FLOAT weight; // Normalisation term (1 / (#targets + #successors))
	FLOAT length; // Desired bone length

} SolverJoint_FAST;

typedef struct SolverChain_FAST
{
	int succ; // First successor chain index (successors are always in series)
	int numb; // Successor count

	int head; // Starting joint index
	int tail; // Ending joint index

} SolverChain_FAST;


typedef struct Armature_FAST {

	int jointCount; // Total number of joints
	SolverJoint_FAST* joints; // Joint settings
	Transform* transforms; // Joint locations

	int chainCount; // Total number of chains
	SolverChain_FAST* chains; // Current chain setup
	Point* targets; // Target locations

	int objectiveCount;
	int* targetChains;
} Armature_FAST;

void solve_FAST(Scene* scene); // Solve scene using FABRIK, store result in frames

#endif /* !SOLVERFAST_H */
