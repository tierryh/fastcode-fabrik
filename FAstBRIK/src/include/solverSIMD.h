#ifndef SOLVERSIMD_H
#define SOLVERSIMD_H

#include "reader.h"
#include "ops.h"

#include <immintrin.h>

#define FABRIK_EPS_SIMD FLT(0.001)

#define FABRIK_CROSS_OPT

#ifndef SINGLE
	#error "solve_SIMD only supports single precision"
#endif // !SINGLE

typedef struct Rotation_SIMD {
	__m128 x, y, z;
} Rotation_SIMD;

typedef struct Transform_SIMD {
	__m128 translation;
	Rotation_SIMD rotation;
} Transform_SIMD;


typedef struct SolverJoint_SIMD {

	int index;
	int successor_count;
	int successor_filled;
	int predecessor_successor_index;
	int has_objective;
	__m128 bone_length_squared;
	__m128 bone_length;
	FLOAT c[4];
	struct SolverJoint_SIMD **successors;
	struct SolverJoint_SIMD *predecessor;
	Transform_SIMD t;
	__m128 current_target;
	__m128 point_avg;

} SolverJoint_SIMD;

void solve_SIMD(Scene* scene); // Solve scene using FABRIK, store result in frames

#endif /* !SOLVERSIMD_H */
