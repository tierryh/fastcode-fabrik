#ifndef SOLVER_OTHER_H
#define SOLVER_OTHER_H

#include "reader.h"
#include "ops.h"

#define FABRIK_EPS_OTHER FLT(0.001)

typedef __m128 Point_Other;
#define PointIdentity_Other _mm_set1_ps(0.0);
typedef Point_Other Target_Other;
typedef struct Rotation_Other {
	Point_Other x, y, z;
} Rotation_Other;
typedef struct Transform_Other {
	Point_Other translation;
	Rotation_Other rotation;
} Transform_Other;

/**
 * Represents a joint in a figure.
 */
typedef struct SolverJoint_Other {
	/**
	 * The transforms of the joint
	 */
	Transform_Other t;
	/**
	 * The current target for the current frame
	 */
	Target_Other current_target;
	/**
	 * Result average
	 */
	Point_Other point_avg;
    /**
	 * The joint limits, as defined in the paper
	 */
	__m128 c;
	/**
	 * The index of the predecessor of this joint
	 */
	int pred_index;
	/**
	 * Whether the joint has an objective
	 */
	FLOAT weight;
	/**
	 * The length of the bone, which ends at this joint.
	 */
	FLOAT bone_length;

	// padding
	int pad;

} SolverJoint_Other;


void solve_Other(Scene* scene); // Solve scene using FABRIK, store result in frames

#endif /* !SOLVER_OTHER_H */