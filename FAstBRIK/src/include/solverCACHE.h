#ifndef SOLVERCACHE_H
#define SOLVERCACHE_H

#include "reader.h"
#include "ops.h"

#define FABRIK_EPS_CACHE FLT(0.001)

typedef struct SolverJoint_CACHE {

	Point c;
	Target current_target;
	Point point_avg;
	FLOAT bone_length;
	FLOAT weight;
	int predecessor;

	FLOAT padding;

} SolverJoint_CACHE;


typedef struct Armature_CACHE {

	int jointCount;
	Transform* transforms;
	SolverJoint_CACHE* joints;

} Armature_CACHE;

void solve_CACHE(Scene* scene); // Solve scene using FABRIK, store result in frames

#endif /* !SOLVERCACHE_H */
