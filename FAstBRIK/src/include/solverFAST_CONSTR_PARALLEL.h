#ifndef SOLVERFAST_CONSTR_PARALLEL_H
#define SOLVERFAST_CONSTR_PARALLEL_H

#include "reader.h"
#include "ops.h"

#define FABRIK_EPS_FAST_CONSTR_PARALLEL FLT(0.001)


typedef struct SolverJoint_FAST_CONSTR_PARALLEL
{
	Point c; // Constraint values
	FLOAT weight; // Normalisation term (1 / (#targets + #successors))
	FLOAT length; // Desired bone length

} SolverJoint_FAST_CONSTR_PARALLEL;

typedef struct SolverChain_FAST_CONSTR_PARALLEL
{
	int succ; // First successor chain index (successors are always in series)
	int numb; // Successor count

	int head; // Starting joint index
	int tail; // Ending joint index

} SolverChain_FAST_CONSTR_PARALLEL;


typedef struct Armature_FAST_CONSTR_PARALLEL {

	int jointCount; // Total number of joints
	SolverJoint_FAST_CONSTR_PARALLEL* joints; // Joint settings
	Transform* transforms; // Joint locations

	int chainCount; // Total number of chains
	SolverChain_FAST_CONSTR_PARALLEL* chains; // Current chain setup
	Point* targets; // Target locations

	int objectiveCount;
	int* targetChains;
} Armature_FAST_CONSTR_PARALLEL;

void solve_FAST_CONSTR_PARALLEL(Scene* scene); // Solve scene using FABRIK, store result in frames

#endif /* !SOLVERFAST_CONSTR_PARALLEL_H */
