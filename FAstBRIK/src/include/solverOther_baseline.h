#ifndef SOLVER_OTHER_BASELINE
#define SOLVER_OTHER_BASELINE

#include "reader.h"
#include "ops.h"

#define FABRIK_EPS FLT(0.001)

/**
 * Represents a joint in a figure.
 */
typedef struct SolverJointOtherBaseline {

	/**
	 * Index within joints array
	 */
	int index;
	/**
	 * Counts the total number of (direct) successors.
	 */
	int successor_count;
	/**
	 * Counts the number of successors already set.
	 */
	int successor_filled;
	/**
	 * Index of this node in the successor array of the predecessor
	 */
	int predecessor_successor_index;
	/**
	 * Whether the joint has an objective
	 */
	int has_objective;
	/**
	 * The squared length of the bone, which ends at this joint.
	 */
	FLOAT bone_length_squared;
	/**
	 * The length of the bone, which ends at this joint.
	 */
	FLOAT bone_length;
	/**
	 * The joint limits, as defined in the paper
	 */
	FLOAT c[4];
	/**
	 * An array of (direct) successors
	 */
	struct SolverJointOtherBaseline **successors;
	/**
	 * A pointer to the predecessor of this joint
	 */
	struct SolverJointOtherBaseline *predecessor;
	/**
	 * The transforms of the joint
	 */
	Transform t;
	/**
	 * The current target for the current frame
	 */
	Target current_target;
	/**
	 * Result average
	 */
	Point point_avg;

} SolverJointOtherBaseline;


void solveOtherBaseline(Scene* scene); // Solve scene using FABRIK, store result in frames

#endif