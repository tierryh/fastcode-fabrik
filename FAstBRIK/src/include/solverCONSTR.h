#ifndef SOLVER_CONSTR_H
#define SOLVER_CONSTR_H

#include "reader.h"
#include "ops.h"

#define FABRIK_EPS_CONSTR FLT(0.001)

/**
 * Represents a joint in a figure.
 */
typedef struct SolverJoint_CONSTR {

	int index;
	int successor_count;
	int successor_filled;
	int predecessor_successor_index;

	int has_objective;
	FLOAT bone_length_squared;
	FLOAT bone_length;
	Point c;
	
	struct SolverJoint_CONSTR** successors;
	struct SolverJoint_CONSTR* predecessor;

	Transform t;
	Target current_target;
	Point point_avg;

} SolverJoint_CONSTR;


void solve_CONSTR(Scene* scene); // Solve scene using FABRIK, store result in frames

#endif /* !SOLVER_CONSTR_H */
