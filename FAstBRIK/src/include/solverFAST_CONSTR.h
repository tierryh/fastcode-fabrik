#ifndef SOLVERFAST_CONSTR_H
#define SOLVERFAST_CONSTR_H

#include "reader.h"
#include "ops.h"

#define FABRIK_EPS_FAST_CONSTR FLT(0.001)


typedef struct SolverJoint_FAST_CONSTR
{
	Point c; // Constraint values
	FLOAT weight; // Normalisation term (1 / (#targets + #successors))
	FLOAT length; // Desired bone length

} SolverJoint_FAST_CONSTR;

typedef struct SolverChain_FAST_CONSTR
{
	int succ; // First successor chain index (successors are always in series)
	int numb; // Successor count

	int head; // Starting joint index
	int tail; // Ending joint index

} SolverChain_FAST_CONSTR;


typedef struct Armature_FAST_CONSTR {

	int jointCount; // Total number of joints
	SolverJoint_FAST_CONSTR* joints; // Joint settings
	Transform* transforms; // Joint locations

	int chainCount; // Total number of chains
	SolverChain_FAST_CONSTR* chains; // Current chain setup
	Point* targets; // Target locations

	int objectiveCount;
	int* targetChains;
} Armature_FAST_CONSTR;

void solve_FAST_CONSTR(Scene* scene); // Solve scene using FABRIK, store result in frames

#endif /* !SOLVERFAST_CONSTR_H */
