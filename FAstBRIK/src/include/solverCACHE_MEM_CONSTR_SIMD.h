#ifndef SOLVERCACHE_MEM_CONSTR_SIMD_H
#define SOLVERCACHE_MEM_CONSTR_SIMD_H

#include "reader.h"
#include "ops.h"

#define FABRIK_EPS_CACHE_MEM_CONSTR_SIMD FLT(0.001)


typedef struct Rotation_CACHE_MEM_CONSTR_SIMD {
	__m128 x, y, z;
} Rotation_CACHE_MEM_CONSTR_SIMD;

typedef struct Transform_CACHE_MEM_CONSTR_SIMD {
	__m128 translation;
	Rotation_CACHE_MEM_CONSTR_SIMD rotation;
} Transform_CACHE_MEM_CONSTR_SIMD;


typedef struct SolverJoint_CACHE_MEM_CONSTR_SIMD {

	__m128 c;
	__m128 bone_length;
	__m128 current_target;
	__m128 point_avg;
	__m128 weight;
	int predecessor;

} SolverJoint_CACHE_MEM_CONSTR_SIMD;


typedef struct Armature_CACHE_MEM_CONSTR_SIMD {

	int jointCount;
	Transform_CACHE_MEM_CONSTR_SIMD* transforms;
	SolverJoint_CACHE_MEM_CONSTR_SIMD* joints;

} Armature_CACHE_MEM_CONSTR_SIMD;

void solve_CACHE_MEM_CONSTR_SIMD(Scene* scene); // Solve scene using FABRIK, store result in frames

#endif /* !SOLVERCACHE_MEM_CONSTR_SIMD_H */
