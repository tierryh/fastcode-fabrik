#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "include/solverOther.h"
#include "include/dataformat.h"
#include "include/algorithm.h"

// an array of joint indices, mapping reordered indices to original indices
int *old_index;

// an array indicating for every joint, whether its constraints are below 90 degrees
char *below90;

void checkPoint_Other(const Point* p, const char* label, int line)
{
	checkArray((float*)&p->x, 3, label, line);
}

void checkRotation_Other(const Rotation* r, const char* label, int line)
{
	checkPoint_Other(&r->x, label, line);
	checkPoint_Other(&r->y, label, line);
	checkPoint_Other(&r->z, label, line);
}

void checkTransform_Other(const Transform* t, const char* label, int line)
{
	checkPoint_Other(&t->translation, label, line);
	checkRotation_Other(&t->rotation, label, line);
}

FLOAT length_squared_Other(Point_Other p1, Point_Other p2)
{
	Point_Other d = _mm_sub_ps(p1, p2);
	return _mm_cvtss_f32(_mm_dp_ps(d, d, 0x77));
}

void constrain_alternative(const Rotation_Other *r1, Point_Other *normal1, __m128 c1, char smaller90) {
	Rotation r;
	_mm_storeu_ps(&r.x.x, r1->x);
	_mm_storeu_ps(&r.y.x, r1->y);
	_mm_storeu_ps(&r.z.x, r1->z);

	Point normal;
	_mm_storeu_ps(&normal.x, *normal1);

	FLOAT c[4];
	_mm_storeu_ps(c, c1);

	// Project onto axis (or transform to local space)
	const FLOAT xDot = normal.x * r.x.x + normal.y * r.x.y + normal.z * r.x.z; OP_ADD(2); OP_MUL(3); OP_RED(6);
	const FLOAT yDot = normal.x * r.y.x + normal.y * r.y.y + normal.z * r.y.z; OP_ADD(2); OP_MUL(3); OP_RED(6);
	const FLOAT zDot = normal.x * r.z.x + normal.y * r.z.y + normal.z * r.z.z; OP_ADD(2); OP_MUL(3); OP_RED(6);
	// determine the ellipse axis of the quadrant
	FLOAT a, b;
	char xsign1, xsign2;
	if (yDot >= FLT(0.0) && zDot >= FLT(0.0)) { a = c[0]; b = c[1]; }
	else if (yDot >= FLT(0.0) && zDot < FLT(0.0)) { a = c[0]; b = c[3]; }
	else if (yDot < FLT(0.0) && zDot >= FLT(0.0)) { a = c[2]; b = c[1]; }
	else /* (yDot < FLT(0.0) && zDot < FLT(0.0)) */ { a = c[2]; b = c[3]; }
	OP_RED(2);

	/*
	The case of angles both below and above 90 degrees is not supported because the paper does not specify properly how this case should be handled.
	Indeed they mention that in this case there should be a parabolic shape used to indicate the allowed two dimensional area, which cannot work when analyzed properly.
	*/

	// check whether the bone is inside or outside the ellipse
	// this is a reformulated check of y^2/a^2 + z^2/b^2 <= 1
	const FLOAT aa = a*a; OP_MUL(1);
	const FLOAT bb = b*b; OP_MUL(1);
	const FLOAT yds = yDot * yDot; OP_MUL(1);
	const FLOAT zds = zDot * zDot; OP_MUL(1);
	int check_in_ellipse = yds*bb + zds*aa <= aa*bb; OP_MUL(3); OP_ADD(2);

	int check = (smaller90 && check_in_ellipse && xDot >= 0) || (!smaller90 && (xDot >= 0 || !check_in_ellipse));
	if (!check) {
		// project the bone
		FLOAT norm = SQRT(yds+zds);
		if(norm > EPS_THRES) {
			FLOAT yp = yDot*a; OP_MUL(1);
			FLOAT zp = zDot*b; OP_MUL(1);
			FLOAT inv_norm = FLT(1.0)/norm; OP_ADD(1); OP_SQR(1); OP_DIV(1);
			yp *= inv_norm; OP_MUL(1);
			zp *= inv_norm; OP_MUL(1);
			FLOAT xp = SQRT(FLT(1.0) - yp*yp - zp*zp); OP_RED(1); OP_SQR(1); OP_ADD(2); OP_MUL(2);
			if(!smaller90) {
				xp = -xp; OP_ADD(1);
			}
			normal.x = xp*r.x.x + yp*r.y.x + zp*r.z.x; OP_MUL(3); OP_ADD(2);
			normal.y = xp*r.x.y + yp*r.y.y + zp*r.z.y; OP_MUL(3); OP_ADD(2);
			normal.z = xp*r.x.z + yp*r.y.z + zp*r.z.z; OP_MUL(3); OP_ADD(2);
			OP_WRT(3);
		} else {
			normal.x = r.x.x;
			normal.y = r.x.y;
			normal.z = r.x.z;
		}
	}
	*normal1 = _mm_loadu_ps(&normal.x);
}

void rotateTowards_Other(const Rotation_Other* r, const Point_Other normal, Rotation_Other *out)
{
	out->x = normal;

	// Project axis
	const __m128 yDot = _mm_dp_ps(normal, r->y, 0b01111111);
	const __m128 yPrj = _mm_mul_ps(normal, yDot);
	const __m128 proj = _mm_sub_ps(r->y, yPrj);

	// normalise
	const __m128 sq = _mm_dp_ps(proj, proj, 0b01111111);
	const __m128 s = _mm_sqrt_ps(sq);
	out->y = _mm_div_ps(proj, s);

	// Cross product for vector base
	const __m128 nrmt = _mm_shuffle_ps(normal, normal, _MM_SHUFFLE(3, 0, 2, 1));
	const __m128 frwt = _mm_shuffle_ps(out->y, out->y, _MM_SHUFFLE(3, 0, 2, 1));
	const __m128 zxy = _mm_sub_ps(_mm_mul_ps(normal, frwt), _mm_mul_ps(nrmt, out->y));
	out->z = _mm_shuffle_ps(zxy, zxy, _MM_SHUFFLE(3, 0, 2, 1));

	// By permuting the cross product we can save a shuffle
	//out.z.z = out.x.x * out.y.y - out.x.y * out.y.x;
	//out.z.x = out.x.y * out.y.z - out.x.z * out.y.y;
	//out.z.y = out.x.z * out.y.x - out.x.x * out.y.z;
}


void doSolve_Other(int jointCount, SolverJoint_Other *joints, Pose *output, unsigned int iterations)
{
	// TODO: Always same amount of iterations for performance testing
	// while (current_error > FABRIK_EPS && fabs(old_error - current_error) > FABRIK_EPS)
	for(unsigned int i = 0; i < iterations; i++)
	{
		// forward
		for (int j = 0; j < jointCount-1; j++)
		{
			const int index = j;// jointOrder[j];

			SolverJoint_Other* ae = &joints[index];
			Transform_Other* at = &ae->t;
			SolverJoint_Other* pe = &joints[ae->pred_index];
			const Transform_Other* pt = &pe->t;

			// Calculate proper average
			at->translation = _mm_mul_ps(ae->point_avg, _mm_set1_ps(ae->weight));
			ae->point_avg = ae->current_target;

			// Compute desired direction
			const __m128 mmDelta = _mm_sub_ps(at->translation, pt->translation);
			const __m128 mmSq = _mm_dp_ps(mmDelta, mmDelta, 0x77);
			const __m128 mmS = _mm_sqrt_ps(mmSq);
			const __m128 mmDiv = _mm_max_ps(mmS, _mm_set1_ps(FABRIK_EPS_OTHER));
			const __m128 mmNrm = _mm_div_ps(mmDelta, mmDiv);

			//const Point delta = { point->x - prev->x, point->y - prev->y, point->z - prev->z };
			//const FLOAT norm = max(SQRT(delta.x * delta.x + delta.y * delta.y + delta.z * delta.z), FABRIK_EPS_CACHE_MEM_CONSTR_SIMD);
			//Point nrm = { delta.x / norm, delta.y / norm, delta.z / norm };

			// Move bone
			const __m128 mmFma = MM_FMSUB_PS(_mm_set1_ps(ae->bone_length), mmNrm, at->translation);
			pe->point_avg = _mm_sub_ps(pe->point_avg, mmFma);

			//prept->x += point->x - ae->bone_length * nrm.x;
			//prept->y += point->y - ae->bone_length * nrm.y;
			//prept->z += point->z - ae->bone_length * nrm.z;
		}

		// backward
		// we did not move the root, therefore we can skip it
		for (int j = jointCount - 2; j >= 0; j--)
		{
			const int index = j;// jointOrder[j];

			SolverJoint_Other* ae = &joints[index];
			Transform_Other* at = &ae->t;
			SolverJoint_Other* pe = &joints[ae->pred_index];
			const Transform_Other* pt = &pe->t;

			// Compute desired direction
			const __m128 mmDelta = _mm_sub_ps(at->translation, pt->translation);
			const __m128 mmSq = _mm_dp_ps(mmDelta, mmDelta, 0x77);
			const __m128 mmS = _mm_sqrt_ps(mmSq);
			const __m128 mmDiv = _mm_max_ps(mmS, _mm_set1_ps(FABRIK_EPS_OTHER));
			__m128 nrm = _mm_div_ps(mmDelta, mmDiv);

			//const Point delta = { point->x - prev->x, point->y - prev->y, point->z - prev->z };
			//const FLOAT norm = max(SQRT(delta.x*delta.x + delta.y*delta.y + delta.z*delta.z), FABRIK_EPS_CACHE_MEM_CONSTR_SIMD);
			//Point nrm = { delta.x / norm, delta.y / norm, delta.z / norm };

			// Constrain to axis and rotate
#ifdef FABRIK_CONSTRAINT
			constrain_alternative(&pt->rotation, &nrm, pe->c, below90[index]);
#endif
#ifdef FABRIK_ROTATION
			rotateTowards_Other(&pt->rotation, nrm, &at->rotation);
#endif

			// Move bone
			at->translation = MM_FMADD_PS(_mm_set1_ps(ae->bone_length), nrm, pt->translation);
		}
	}
	// output
	for (int i = 0; i < jointCount; i++)
	{
		// transform back from SIMD
		Transform_Other *t = &joints[i].t;
		Pose *p = &output[old_index[i]];
		_mm_store_ps(&p->translation.x, t->translation);
		_mm_store_ps(&p->rotation.x.x, t->rotation.x);
		_mm_store_ps(&p->rotation.y.x, t->rotation.y);
		_mm_store_ps(&p->rotation.z.x, t->rotation.z);
		OP_RED(3); OP_WRT(3);
		VALIDATE(Transform_Other, &output[i]);
	}
}

void precompute(SolverJoint_Other *joints, int jointCount) {
	below90 = (char *) malloc(sizeof(char) * jointCount);
	for(int i = 0; i < jointCount; i++) {
		SolverJoint_Other *cur = &joints[i];

		FLOAT buf[4];
		_mm_storeu_ps(buf, cur->c);

		#ifndef NDEBUG
		// ensure constraint property (not both, below and above 90 degrees per joint, see constrain_alternative function)
		int decided = 0;
		int below = 0;
		FLOAT quad = PI / 2;
		for(int j = 0; j < 4; j++) {
			if(!decided) {
				below = buf[j] < quad;
				decided = below ? 1 : (buf[j] == quad ? 0 : 1);
			} else {
				if(below) {
					assert(buf[j] <= quad);
				} else {
					assert(buf[j] >= quad);
				}
			}
		}
		#endif

		// precompute constraints
		for(int j = 0; j < 4; j++) {
			if(buf[j] <= PI / 2) {
				below90[i] = 1;
			} else if(buf[j] > PI / 2) {
				below90[i] = 0;
			}
			buf[j] = SIN(buf[j]); OP_TRG(1); OP_RED(1); OP_WRT(1); OP_MUL(1);
		}
		
		cur->c = _mm_loadu_ps(buf);
		
		// precompute weight
		assert(cur->pad != 0);
		cur->weight = FLT(1.0)/cur->pad;
	}
}

void solve_Other(Scene* scene)
{
	OP_RESET();

	OP_CPL(10); // Scene
	OP_CPL(6); // Armature
	Armature *armature = scene->armature;
	OP_CPL(armature->objectiveCount); // Ojectives
	OP_CPL(armature->jointCount * 5); // Joints
	OP_CPL(armature->jointCount * 12); // Initial
	OP_CPL(armature->jointCount * 12 * scene->frameCount); // Frames

	const int jointCount = armature->jointCount; OP_RED(1);
	SolverJoint_Other *joints = (SolverJoint_Other*)malloc(sizeof(SolverJoint_Other) * jointCount);
	OP_CPL(33 * jointCount); // Working set

	for (int i = 0; i < jointCount; i++) {
		joints[i].pad = 0; // we use the padding field for precalculating the weight
	}

	// compute predecessors and intialize the SolverJoints
	for (int i = 0; i < jointCount; i++)
	{
		joints[i].point_avg = PointIdentity_Other;
		joints[i].current_target = PointIdentity_Other;
		joints[i].c = _mm_loadu_ps(&armature->joints[i].c.x); OP_RED(4); OP_WRT(4);
		// load the transform of the joint
		Transform_Other *t = &joints[i].t;
		Pose *p = &scene->initialPose[i];
		t->translation = _mm_loadu_ps(&p->translation.x);
		t->rotation.x = _mm_loadu_ps(&p->rotation.x.x);
		t->rotation.y = _mm_loadu_ps(&p->rotation.y.x);
		t->rotation.z = _mm_loadu_ps(&p->rotation.z.x);
		if (i != 0)
		{
			int pred_index = armature->joints[i].parent;
			joints[pred_index].pad++;
			joints[i].pred_index = pred_index; OP_WRT(2);
			const FLOAT bone_squared = length_squared_Other(joints[i].t.translation, joints[pred_index].t.translation);
			joints[i].bone_length = SQRT(bone_squared); OP_WRT(1);
		}
		else
		{
			joints[i].pred_index = -1; OP_WRT(2);
		}
	}

	// Reorder joints in a topological order (in the order, the algorithm will iterate over it)
	SolverJoint_Other *joints_ordered = (SolverJoint_Other*)malloc(sizeof(SolverJoint_Other) * jointCount);
	int *new_index = (int *) malloc(sizeof(int) * jointCount);
	old_index = (int *) malloc(sizeof(int) * jointCount);
	int nextIndex = 0;
	int *remainingChildren = (int*)malloc(sizeof(int) * jointCount);
	for (int i = 0; i < jointCount; i++)
	{
		remainingChildren[i] = joints[i].pad; OP_RED(1); OP_WRT(1);
		if (remainingChildren[i] == 0)
		{
			new_index[i] = nextIndex; OP_WRT(1);
			old_index[nextIndex] = i; OP_WRT(1);
			joints_ordered[nextIndex] = joints[i]; OP_RED(1); OP_WRT(1);
			nextIndex++;
		}
	}
	for (int i = 0; i < jointCount-1; i++)
	{
		int parentIndex = joints[old_index[i]].pred_index; OP_RED(2);
		remainingChildren[parentIndex]--; OP_WRT(1);
		if (remainingChildren[parentIndex] == 0)
		{
			new_index[parentIndex] = nextIndex; OP_WRT(1);
			old_index[nextIndex] = parentIndex; OP_WRT(1);
			joints_ordered[nextIndex] = joints[parentIndex]; OP_RED(1); OP_WRT(1);
			nextIndex++;
		}
	}
	// fix the predecessor pointers
	joints_ordered[nextIndex-1].pred_index = -1;
	for (int i = 0; i < jointCount-1; i++) {
		joints_ordered[i].pred_index = new_index[joints[old_index[i]].pred_index]; OP_RED(4); OP_WRT(1);
	}
	free(remainingChildren);
	free(joints);
	assert(nextIndex == jointCount);

	// set objective joints
	for (int i = 0; i < armature->objectiveCount; i++)
	{
		joints_ordered[new_index[armature->objectives[i].joint]].pad++; OP_RED(1); OP_WRT(1);
	}

	// precompute
	precompute(joints_ordered, jointCount);

	///////////////////////////////////////////////////////////////////////////////

	// run FABRIK for each frame
	for (int f = 0; f < scene->frameCount; f++)
	{
		///////////////////////////////////////////////////////////////////////////////
		Target* targets = scene->targets[f];
		for (int i = 0; i < armature->objectiveCount; i++)
		{
			__m128 target = _mm_load_ps(&targets[i].x);
			joints_ordered[new_index[armature->objectives[i].joint]].current_target = target; OP_RED(5); OP_WRT(3);
			joints_ordered[new_index[armature->objectives[i].joint]].point_avg = target; OP_RED(5); OP_WRT(3);
		}
		Pose* output = scene->frames[f]; OP_RED(2);
		doSolve_Other(jointCount, joints_ordered, output, scene->iterations);
		///////////////////////////////////////////////////////////////////////////////
	}

	// clean up
	free(old_index);
	free(new_index);
	free(joints_ordered);
	free(below90);
}
