#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "include/solverMEM.h"
#include "include/dataformat.h"
#include "include/algorithm.h"

void checkPoint_MEM(const Point* p, const char* label, int line)
{
	checkArray((float*)& p->x, 3, label, line);
}

void checkRotation_MEM(const Rotation* r, const char* label, int line)
{
	checkPoint_MEM(&r->x, label, line);
	checkPoint_MEM(&r->y, label, line);
	checkPoint_MEM(&r->z, label, line);
}

void checkTransform_MEM(const Transform* t, const char* label, int line)
{
	checkPoint_MEM(&t->translation, label, line);
	checkRotation_MEM(&t->rotation, label, line);
}

FLOAT length_squared_MEM(Point *p1, Point *p2)
{
	FLOAT dx, dy, dz;
	dx = p1->x - p2->x;
	dy = p1->y - p2->y;
	dz = p1->z - p2->z;
	return dx * dx + dy * dy + dz * dz;
}

FLOAT computeError_MEM(int jointCount, SolverJoint_MEM *joints)
{
	FLOAT current_error = 0;
	for (int i = 0; i < jointCount; i++)
	{
		if (joints[i].has_objective)
		{
			// TODO: Probably better to just add them up (?)
			const FLOAT ls = length_squared_MEM(&joints[i].current_target, &joints[i].t.translation);
			current_error = max(ls, current_error);
		}
	}

	return current_error;
}

Point constrain_MEM(const Rotation* r, const Point* normal, FLOAT c[4])
{
	// Project onto axis (or, transform to local space)
	const FLOAT xDot = normal->x * r->x.x + normal->y * r->x.y + normal->z * r->x.z;
	const FLOAT yDot = normal->x * r->y.x + normal->y * r->y.y + normal->z * r->y.z;
	const FLOAT zDot = normal->x * r->z.x + normal->y * r->z.y + normal->z * r->z.z;

	// Convert to polar coordinates
	const FLOAT angle = ACOS(xDot);
	const FLOAT norm = SQRT(yDot * yDot + zDot * zDot);

	if (norm > EPS_THRES)
	{
		// Find ellipse axis'
		FLOAT a, b;
		if (yDot >= FLT(0.0) && zDot >= FLT(0.0)) { a = c[0]; b = c[1]; }
		else if (yDot >= FLT(0.0) && zDot < FLT(0.0)) { a = c[0]; b = c[3]; }
		else if (yDot < FLT(0.0) && zDot >= FLT(0.0)) { a = c[2]; b = c[1]; }
		else /* (yDot < FLT(0.0) && zDot < FLT(0.0)) */ { a = c[2]; b = c[3]; }

		// "Project" onto ellipse.
		// This skews the result but works well enough.
		const FLOAT py = yDot / norm * a;
		const FLOAT pz = zDot / norm * b;

		/* Projecting properly will break hinge constraints
		const FLOAT el = atan2(a*zDot, b*yDot);
		const FLOAT py = a * COS(el);
		const FLOAT pz = b * SIN(el);
		*/

		// Compute final angle to check whether we need to constrain at all
		const FLOAT proj = SQRT(py * py + pz * pz);
		if (proj < EPS_THRES)
		{
			// If all constraints are 0 we can just use forward vector
			return r->x;
		}
		else
		{
			if (angle > proj)
			{
				// Convert to euclidean space and normalise
				const FLOAT dot = COS(proj);
				const FLOAT s = SQRT(FLT(1.0) - dot * dot) / proj;
				const FLOAT yd = py * s;
				const FLOAT zd = pz * s;

				// Project back into worldspace
				Point out;
				out.x = dot * r->x.x + yd * r->y.x + zd * r->z.x;
				out.y = dot * r->x.y + yd * r->y.y + zd * r->z.y;
				out.z = dot * r->x.z + yd * r->y.z + zd * r->z.z;

				return out;
			}
		}
	}
	return *normal;
}

Rotation rotateTowards_MEM(const Rotation* r, const Point* normal)
{
	Rotation out;
	out.x = *normal;

	const FLOAT ydot = r->y.x * normal->x + r->y.y * normal->y + r->y.z * normal->z;
	if (ydot < (FLT(1.0) - EPS_THRES))
	{
		// Project and normalize y axis
		const Point y = { r->y.x - normal->x * ydot, r->y.y - normal->y * ydot, r->y.z - normal->z * ydot };
		const FLOAT invNorm = FLT(1.0) / SQRT(y.x*y.x + y.y*y.y + y.z*y.z);

		out.y.x = y.x * invNorm;
		out.y.y = y.y * invNorm;
		out.y.z = y.z * invNorm;
	}
	else
	{
		// Can just reflect forward axis for the y axis whem orthogonal
		const Point refl = { -r->x.x, -r->x.y, -r->x.z };
		out.y = refl;
	}

	// Cross product for vector base
	out.z.x = out.x.y * out.y.z - out.x.z * out.y.y;
	out.z.y = out.x.z * out.y.x - out.x.x * out.y.z;
	out.z.z = out.x.x * out.y.y - out.x.y * out.y.x;

	return out;
}


void doSolve_MEM(int jointCount, SolverJoint_MEM *joints, Pose *output, unsigned int iterations)
{
	//FLOAT old_error = FLT_MAX;
	//FLOAT current_error = 0;
	//current_error = computeError_MEM(jointCount, joints);
	// TODO: Always same amount of iterations for performance testing
	// while (current_error > FABRIK_EPS && fabs(old_error - current_error) > FABRIK_EPS)
	for(unsigned int i = 0; i < iterations; i++)
	{
		// forward
		for (int j = 0; j < jointCount-1; j++)
		{
			SolverJoint_MEM *ae = &joints[j];

			// Add target to average count
			int newpos_count = ae->successor_count;
			if (ae->has_objective)
			{
				ae->point_avg.x += ae->current_target.x;
				ae->point_avg.y += ae->current_target.y;
				ae->point_avg.z += ae->current_target.z;
				newpos_count += 1;
			}

			// Calculate proper average
			Point* point = &ae->t.translation;
			if (newpos_count > 0)
			{
				point->x = ae->point_avg.x / newpos_count;
				point->y = ae->point_avg.y / newpos_count;
				point->z = ae->point_avg.z / newpos_count;

				ae->point_avg.x = FLT(0.0);
				ae->point_avg.y = FLT(0.0);
				ae->point_avg.z = FLT(0.0);
			}

			// Compute desired direction
			const Point* prev = &ae->predecessor->t.translation;
			const Point delta = { point->x - prev->x, point->y - prev->y, point->z - prev->z };
			const FLOAT norm = max(SQRT(delta.x * delta.x + delta.y * delta.y + delta.z * delta.z), FABRIK_EPS_MEM);
			Point nrm = { delta.x / norm, delta.y / norm, delta.z / norm };

			// TODO: If we want to rotate on forward pass we need to average rotation. let's just... not do that?
			// No rotations being computed of course also kills constraints since joints down the line need it.
			// Only doing it on backward pass decreases performance but it's probably alright for this project.
			//nrm = constrain(&ae->t.rotation, &nrm, c); // note that *if* we want to do it the constraints need to be flipped here!
			//ae->predecessor->t.rotation = rotateTowards(&ae->t.rotation, &nrm);

			// Move bone
			Point *prept = &ae->predecessor->point_avg;
			prept->x += point->x - ae->bone_length * nrm.x;
			prept->y += point->y - ae->bone_length * nrm.y;
			prept->z += point->z - ae->bone_length * nrm.z;
		}

		// backward
		// we did not move the root, therefore we can skip it
		for (int j = jointCount - 2; j >= 0; j--)
		{
			SolverJoint_MEM *ae = &joints[j];

			// Compute desired direction
			const Point* point = &ae->t.translation;
			const Point* prev = &ae->predecessor->t.translation;
			const Point delta = { point->x - prev->x, point->y - prev->y, point->z - prev->z };
			const FLOAT norm = max(SQRT(delta.x*delta.x + delta.y*delta.y + delta.z*delta.z), FABRIK_EPS_MEM);
			Point nrm = { delta.x / norm, delta.y / norm, delta.z / norm };

			// Constrain to axis and rotate
			nrm = constrain_MEM(&ae->predecessor->t.rotation, &nrm, ae->predecessor->c);
			ae->t.rotation = rotateTowards_MEM(&ae->predecessor->t.rotation, &nrm);

			// Move bone
			Point* res = &ae->t.translation;
			res->x = prev->x + ae->bone_length * nrm.x;
			res->y = prev->y + ae->bone_length * nrm.y;
			res->z = prev->z + ae->bone_length * nrm.z;
		}

		//old_error = current_error;
		//current_error = computeError_MEM(jointCount, joints);
	}
	for (int i = 0; i < jointCount; i++)
	{
		output[joints[i].index] = joints[i].t;
		VALIDATE(Transform_MEM, &output[i]);
	}
}

void solve_MEM(Scene* scene)
{
	OP_RESET();

	Armature *armature = scene->armature;
	const int jointCount = armature->jointCount;
	SolverJoint_MEM *joints = (SolverJoint_MEM*)malloc(sizeof(SolverJoint_MEM) * jointCount);
	SolverJoint_MEM *jointsSorted = (SolverJoint_MEM*)malloc(sizeof(SolverJoint_MEM) * jointCount);

	// compute predecessors and intialize the SolverJoints
	for (int i = 0; i < jointCount; i++)
	{
		joints[i].index = i;
		joints[i].successor_count = 0;
		joints[i].successor_filled = 0;
		joints[i].has_objective = 0;
		joints[i].point_avg = PointIdentity;
		joints[i].c[0] = armature->joints[i].c.x;
		joints[i].c[1] = armature->joints[i].c.y;
		joints[i].c[2] = armature->joints[i].c.z;
		joints[i].c[3] = armature->joints[i].c.w;
		joints[i].t = scene->initialPose[i];
		if (i != 0)
		{
			joints[i].predecessor = &joints[armature->joints[i].parent];
			joints[i].bone_length_squared = length_squared_MEM(&joints[i].t.translation, &joints[i].predecessor->t.translation);
			joints[i].bone_length = SQRT(joints[i].bone_length_squared);
		}
		else
		{
			joints[i].predecessor = NULL;
		}
	}
	for (int i = 0; i < jointCount; i++)
	{
		if (joints[i].predecessor != NULL)
		{
			joints[i].predecessor->successor_count++;
		}
	}
	for (int i = 0; i < jointCount; i++)
	{
		joints[i].successors = (SolverJoint_MEM**)malloc(sizeof(SolverJoint_MEM*) * joints[i].successor_count);
	}
	for (int i = 0; i < jointCount; i++)
	{
		if (joints[i].predecessor != NULL)
		{
			joints[i].predecessor->successors[joints[i].predecessor->successor_filled] = &joints[i];
			joints[i].predecessor->successor_filled++;
		}
	}

	// Compute topological ordering of joints
	int *jointOrder = (int*)malloc(sizeof(int) * jointCount);
	int jointOrderPos = 0;
	int *toposort = (int*)malloc(sizeof(int) * jointCount);
	for (int i = 0; i < jointCount; i++)
	{
		toposort[i] = joints[i].successor_count;
		if (toposort[i] == 0)
		{
			jointOrder[jointOrderPos++] = i;
		}
	}
	for (int i = 0; i < jointCount-1; i++)
	{
		int parentIndex = joints[jointOrder[i]].predecessor->index;
		toposort[parentIndex]--;
		if (toposort[parentIndex] == 0)
		{
			jointOrder[jointOrderPos++] = parentIndex;
		}
	}
	free(toposort);
	assert(jointOrderPos == jointCount);
	assert(jointOrder[jointOrderPos-1] == 0);

	// set objective joints
	for (int i = 0; i < armature->objectiveCount; i++)
	{
		joints[armature->objectives[i].joint].has_objective = 1;
	}

	for (int i = 0; i < jointCount; i++)
	{
		jointsSorted[i] = joints[jointOrder[i]];
		jointsSorted[i].successors = (SolverJoint_MEM**)malloc(sizeof(SolverJoint_MEM*) * joints[i].successor_count);
		if (jointsSorted[i].predecessor != NULL)
		{
			jointsSorted[i].predecessor = &jointsSorted[jointOrder[jointsSorted[i].predecessor->index]];
			for (int j = 0; j < jointsSorted[i].successor_count; j++)
			{
				jointsSorted[i].successors[j] = &jointsSorted[jointOrder[joints[jointOrder[i]].successors[j]->index]];
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////

	// run FABRIK for each frame
	for (int f = 0; f < scene->frameCount; f++)
	{
		///////////////////////////////////////////////////////////////////////////////
		Target* targets = scene->targets[f];
		for (int i = 0; i < armature->objectiveCount; i++)
		{
			for (int j = 0; j < jointCount; j++)
			{
				if (jointOrder[j] == armature->objectives[i].joint)
				{
					jointsSorted[j].current_target = targets[i];
					break;
				}
			}
		}
		Pose* output = scene->frames[f];
		doSolve_MEM(jointCount, jointsSorted, output, scene->iterations);
		///////////////////////////////////////////////////////////////////////////////
	}

	// clean up
	free(jointOrder);
	for (int i = 0; i < jointCount; i++)
	{
		free(joints[i].successors);
		//free(jointsSorted[i].successors); Is this ever allocated? It seems to crash on my machine anyway. ~Felix
	}
	free(joints);
	free(jointsSorted);
}
