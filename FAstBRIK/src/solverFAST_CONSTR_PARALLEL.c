#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "include/solverFAST_CONSTR_PARALLEL.h"
#include "include/dataformat.h"
#include "include/algorithm.h"

void checkPoint_FAST_CONSTR_PARALLEL(const Point* p, const char* label, int line)
{
	checkArray((float*)&p->x, 3, label, line);
}

void checkRotation_FAST_CONSTR_PARALLEL(const Rotation* r, const char* label, int line)
{
	checkPoint_FAST_CONSTR_PARALLEL(&r->x, label, line);
	checkPoint_FAST_CONSTR_PARALLEL(&r->y, label, line);
	checkPoint_FAST_CONSTR_PARALLEL(&r->z, label, line);
}

void checkTransform_FAST_CONSTR_PARALLEL(const Transform* t, const char* label, int line)
{
	checkPoint_FAST_CONSTR_PARALLEL(&t->translation, label, line);
	checkRotation_FAST_CONSTR_PARALLEL(&t->rotation, label, line);
}

INLINE FLOAT length_squared_FAST_CONSTR_PARALLEL(Point *p1, Point *p2)
{
	FLOAT dx, dy, dz;
	dx = p1->x - p2->x;
	dy = p1->y - p2->y;
	dz = p1->z - p2->z;
	return dx * dx + dy * dy + dz * dz;
}

INLINE FLOAT computeError_FAST_CONSTR_PARALLEL(Armature_FAST_CONSTR_PARALLEL* armature)
{
	FLOAT current_error = 0;
	for (int i = 0; i < armature->objectiveCount; i++)
	{
		// Add target to approrpiate chain
		const int chain = armature->targetChains[i];
		Point* tr = &armature->targets[chain];

		const int joint = armature->chains[chain].tail;
		Point* ps = &armature->transforms[joint].translation;

		const FLOAT ls = length_squared_FAST_CONSTR_PARALLEL(tr, ps);
		current_error = max(ls, current_error);
	}
	return current_error;
}

INLINE Point constrain_FAST_CONSTR_PARALLEL(const Rotation* r, const Point* normal, const Point* c)
{
	// Project onto axis (or, transform to local space)
	const FLOAT xDot = normal->x * r->x.x + normal->y * r->x.y + normal->z * r->x.z;
	const FLOAT yDot = normal->x * r->y.x + normal->y * r->y.y + normal->z * r->y.z;
	const FLOAT zDot = normal->x * r->z.x + normal->y * r->z.y + normal->z * r->z.z;

	// Find ellipse axis'
	FLOAT a, b;
	a = (yDot < FLT(0.0)) ? c->z : c->x;
	b = (zDot < FLT(0.0)) ? c->w : c->y;

	// "Project" onto ellipse.
	// This skews the result but works well enough.
	const FLOAT py = yDot * a;
	const FLOAT pz = zDot * b;

	// Compute final angle to check whether we need to constrain at all
	const FLOAT proj = SQRT(py * py + pz * pz);
	if (proj < EPS_THRES)
	{
		// If all constraints are 0 we can just use forward vector
		return r->x;
	}
	else
	{
		const FLOAT dot = FLT(1.0) - proj;
		if (dot > xDot)
		{
			// Convert to euclidean space and normalise
			const FLOAT s = SQRT(FLT(1.0) - dot * dot) / proj;
			const FLOAT yd = py * s;
			const FLOAT zd = pz * s;

			// Project back into worldspace
			Point out;
			out.x = dot * r->x.x + yd * r->y.x + zd * r->z.x;
			out.y = dot * r->x.y + yd * r->y.y + zd * r->z.y;
			out.z = dot * r->x.z + yd * r->y.z + zd * r->z.z;

			return out;
		}
	}
	return *normal;
}

INLINE Rotation rotateTowards_FAST_CONSTR_PARALLEL(const Rotation* r, const Point* normal)
{
	Rotation out;
	out.x = *normal;

	const FLOAT ydot = r->y.x * normal->x + r->y.y * normal->y + r->y.z * normal->z;
	if (ydot < (FLT(1.0) - EPS_THRES))
	{
		// Project and normalize y axis
		const Point y = { r->y.x - normal->x * ydot, r->y.y - normal->y * ydot, r->y.z - normal->z * ydot };
		const FLOAT invNorm = FLT(1.0) / SQRT(y.x*y.x + y.y*y.y + y.z*y.z);

		out.y.x = y.x * invNorm;
		out.y.y = y.y * invNorm;
		out.y.z = y.z * invNorm;
	}
	else
	{
		// Can just reflect forward axis for the y axis whem orthogonal
		const Point refl = { -r->x.x, -r->x.y, -r->x.z };
		out.y = refl;
	}

	// Cross product for vector base
	out.z.x = out.x.y * out.y.z - out.x.z * out.y.y;
	out.z.y = out.x.z * out.y.x - out.x.x * out.y.z;
	out.z.z = out.x.x * out.y.y - out.x.y * out.y.x;

	return out;
}

Point computeNormal_FAST_CONSTR_PARALLEL(const Point* a, const Point* b)
{
	const Point delta = { b->x - a->x, b->y - a->y, b->z - a->z };
	const FLOAT invNorm = FLT(1.0) / max(SQRT(delta.x * delta.x + delta.y * delta.y + delta.z * delta.z), FABRIK_EPS_FAST_CONSTR_PARALLEL);
	const Point out = { delta.x * invNorm, delta.y * invNorm, delta.z * invNorm };
	return out;
}

Point computeBone_FAST_CONSTR_PARALLEL(const Point* point, const Point* nrm, FLOAT length)
{
	Point out;
	out.x = point->x - length * nrm->x;
	out.y = point->y - length * nrm->y;
	out.z = point->z - length * nrm->z;
	return out;
}

Point forward_FAST_CONSTR_PARALLEL(Armature_FAST_CONSTR_PARALLEL* armature, int chain, const SolverJoint_FAST_CONSTR_PARALLEL* joint, const Point* prev)
{
	const SolverChain_FAST_CONSTR_PARALLEL* ch = &armature->chains[chain];
	Point* point = &armature->transforms[ch->tail].translation;
	const SolverJoint_FAST_CONSTR_PARALLEL* pr = &armature->joints[ch->tail];

	// Branch out
	Point avg = armature->targets[chain];
	for (int i = 0; i < ch->numb; i++)
	{
		const Point acc = forward_FAST_CONSTR_PARALLEL(armature, ch->succ + i, pr, point);
		avg.x += acc.x;
		avg.y += acc.y;
		avg.z += acc.z;
	}

	// Calculate proper average
	point->x = avg.x * pr->weight;
	point->y = avg.y * pr->weight;
	point->z = avg.z * pr->weight;

	// Compute all along chain
	for (int k = ch->tail - 1; k >= ch->head; k--)
	{
		const SolverJoint_FAST_CONSTR_PARALLEL* ae = &armature->joints[k];

		// Compute normal from previous bone and move
		Point* pred = &armature->transforms[k].translation;
		const Point nrm = computeNormal_FAST_CONSTR_PARALLEL(pred, point);
		*pred = computeBone_FAST_CONSTR_PARALLEL(point, &nrm, ae->length);
		point = pred;
	}

	// Compute normal from root bone and return predecessor
	const Point nrm = computeNormal_FAST_CONSTR_PARALLEL(prev, point);
	return computeBone_FAST_CONSTR_PARALLEL(point, &nrm, joint->length);
}


void backward_chain_FAST_CONSTR_PARALLEL(Armature_FAST_CONSTR_PARALLEL* armature, int from, int to, const SolverJoint_FAST_CONSTR_PARALLEL** joint, const Transform** transform)
{
	const SolverJoint_FAST_CONSTR_PARALLEL* j = (*joint);
	const Transform* t = (*transform);

	// Grab previous chain
	for (int k = from; k <= to; k++)
	{
		SolverJoint_FAST_CONSTR_PARALLEL* ae = &armature->joints[k];
		Transform* at = &armature->transforms[k];

		// Compute desired direction
		const Point* point = &at->translation;
		const Point* prev = &t->translation;
		Point nrm = computeNormal_FAST_CONSTR_PARALLEL(prev, point);

		// Constrain to axis and rotate
#ifdef FABRIK_CONSTRAINT
		nrm = constrain_FAST_CONSTR_PARALLEL(&t->rotation, &nrm, &j->c);
#endif
#ifdef FABRIK_ROTATION
		at->rotation = rotateTowards_FAST_CONSTR_PARALLEL(&t->rotation, &nrm);
#endif

		// Move bone
		at->translation = computeBone_FAST_CONSTR_PARALLEL(prev, &nrm, -ae->length);

		// Drag joint with
		j = ae; t = at;
	}
	*joint = j;
	*transform = t;
}

void backward_children_FAST_CONSTR_PARALLEL(Armature_FAST_CONSTR_PARALLEL* armature, const SolverChain_FAST_CONSTR_PARALLEL* ch, int offset, const SolverJoint_FAST_CONSTR_PARALLEL* joint, const Transform* transform);

void backward_quad_FAST_CONSTR_PARALLEL(Armature_FAST_CONSTR_PARALLEL* armature, int chain, const SolverJoint_FAST_CONSTR_PARALLEL* const joint, const Transform* const transform)
{
	const SolverChain_FAST_CONSTR_PARALLEL* ch_0 = &armature->chains[chain];
	const SolverChain_FAST_CONSTR_PARALLEL* ch_1 = &armature->chains[chain + 1];
	const SolverChain_FAST_CONSTR_PARALLEL* ch_2 = &armature->chains[chain + 2];
	const SolverChain_FAST_CONSTR_PARALLEL* ch_3 = &armature->chains[chain + 3];

	const int min = MIN(MIN(ch_0->tail - ch_0->head, ch_1->tail - ch_1->head), MIN(ch_2->tail - ch_2->head, ch_3->tail - ch_3->head));

	const SolverJoint_FAST_CONSTR_PARALLEL* joint_0 = joint;
	const SolverJoint_FAST_CONSTR_PARALLEL* joint_1 = joint;
	const SolverJoint_FAST_CONSTR_PARALLEL* joint_2 = joint;
	const SolverJoint_FAST_CONSTR_PARALLEL* joint_3 = joint;
	const Transform* transform_0 = transform;
	const Transform* transform_1 = transform;
	const Transform* transform_2 = transform;
	const Transform* transform_3 = transform;

	// Grab previous chain
	for (int k = 0; k <= min; k++)
	{
		const int k_0 = ch_0->head + k;
		SolverJoint_FAST_CONSTR_PARALLEL* ae_0 = &armature->joints[k_0];
		Transform* at_0 = &armature->transforms[k_0];

		const int k_1 = ch_1->head + k;
		SolverJoint_FAST_CONSTR_PARALLEL* ae_1 = &armature->joints[k_1];
		Transform* at_1 = &armature->transforms[k_1];

		const int k_2 = ch_2->head + k;
		SolverJoint_FAST_CONSTR_PARALLEL* ae_2 = &armature->joints[k_2];
		Transform* at_2 = &armature->transforms[k_2];

		const int k_3 = ch_3->head + k;
		SolverJoint_FAST_CONSTR_PARALLEL* ae_3 = &armature->joints[k_3];
		Transform* at_3 = &armature->transforms[k_3];

		// Compute desired direction
		const Point* point_0 = &at_0->translation;
		const Point* point_1 = &at_1->translation;
		const Point* point_2 = &at_2->translation;
		const Point* point_3 = &at_3->translation;

		const Point* prev_0 = &transform_0->translation;
		const Point* prev_1 = &transform_1->translation;
		const Point* prev_2 = &transform_2->translation;
		const Point* prev_3 = &transform_3->translation;

		Point nrm_0 = computeNormal_FAST_CONSTR_PARALLEL(prev_0, point_0);
		Point nrm_1 = computeNormal_FAST_CONSTR_PARALLEL(prev_1, point_1);
		Point nrm_2 = computeNormal_FAST_CONSTR_PARALLEL(prev_2, point_2);
		Point nrm_3 = computeNormal_FAST_CONSTR_PARALLEL(prev_3, point_3);

		// Constrain to axis and rotate
#ifdef FABRIK_CONSTRAINT
		nrm_0 = constrain_FAST_CONSTR_PARALLEL(&transform_0->rotation, &nrm_0, &joint_0->c);
		nrm_1 = constrain_FAST_CONSTR_PARALLEL(&transform_1->rotation, &nrm_1, &joint_1->c);
		nrm_2 = constrain_FAST_CONSTR_PARALLEL(&transform_2->rotation, &nrm_2, &joint_2->c);
		nrm_3 = constrain_FAST_CONSTR_PARALLEL(&transform_3->rotation, &nrm_3, &joint_3->c);
#endif
#ifdef FABRIK_ROTATION
		at_0->rotation = rotateTowards_FAST_CONSTR_PARALLEL(&transform_0->rotation, &nrm_0);
		at_1->rotation = rotateTowards_FAST_CONSTR_PARALLEL(&transform_1->rotation, &nrm_1);
		at_2->rotation = rotateTowards_FAST_CONSTR_PARALLEL(&transform_2->rotation, &nrm_2);
		at_3->rotation = rotateTowards_FAST_CONSTR_PARALLEL(&transform_3->rotation, &nrm_3);
#endif

		// Move bone
		at_0->translation = computeBone_FAST_CONSTR_PARALLEL(prev_0, &nrm_0, -ae_0->length);
		at_1->translation = computeBone_FAST_CONSTR_PARALLEL(prev_1, &nrm_1, -ae_1->length);
		at_2->translation = computeBone_FAST_CONSTR_PARALLEL(prev_2, &nrm_2, -ae_2->length);
		at_3->translation = computeBone_FAST_CONSTR_PARALLEL(prev_3, &nrm_3, -ae_3->length);

		// Drag joint with
		joint_0 = ae_0;
		transform_0 = at_0;

		joint_1 = ae_1;
		transform_1 = at_1;

		joint_2 = ae_2;
		transform_2 = at_2;

		joint_3 = ae_3;
		transform_3 = at_3;
	}

	// Branch out
	backward_children_FAST_CONSTR_PARALLEL(armature, ch_0, min, joint_0, transform_0);
	backward_children_FAST_CONSTR_PARALLEL(armature, ch_1, min, joint_1, transform_1);
	backward_children_FAST_CONSTR_PARALLEL(armature, ch_2, min, joint_2, transform_2);
	backward_children_FAST_CONSTR_PARALLEL(armature, ch_3, min, joint_3, transform_3);
}

void backward_double_FAST_CONSTR_PARALLEL(Armature_FAST_CONSTR_PARALLEL* armature, int chain, const SolverJoint_FAST_CONSTR_PARALLEL* const joint, const Transform* const transform)
{
	const SolverChain_FAST_CONSTR_PARALLEL* ch_0 = &armature->chains[chain];
	const SolverChain_FAST_CONSTR_PARALLEL* ch_1 = &armature->chains[chain+1];

	const int min = MIN(ch_0->tail - ch_0->head, ch_1->tail - ch_1->head);

	const SolverJoint_FAST_CONSTR_PARALLEL* joint_0 = joint;
	const SolverJoint_FAST_CONSTR_PARALLEL* joint_1 = joint;
	const Transform* transform_0 = transform;
	const Transform* transform_1 = transform;

	// Grab previous chain
	for (int k = 0; k <= min; k++)
	{
		const int k_0 = ch_0->head + k;
		SolverJoint_FAST_CONSTR_PARALLEL* ae_0 = &armature->joints[k_0];
		Transform* at_0 = &armature->transforms[k_0];

		const int k_1 = ch_1->head + k;
		SolverJoint_FAST_CONSTR_PARALLEL* ae_1 = &armature->joints[k_1];
		Transform* at_1 = &armature->transforms[k_1];

		// Compute desired direction
		const Point* point_0 = &at_0->translation;
		const Point* point_1 = &at_1->translation;

		const Point* prev_0 = &transform_0->translation;
		const Point* prev_1 = &transform_1->translation;

		const Point delta_0 = { point_0->x - prev_0->x, point_0->y - prev_0->y, point_0->z - prev_0->z };
		const Point delta_1 = { point_1->x - prev_1->x, point_1->y - prev_1->y, point_1->z - prev_1->z };
		
		Point invNorms;
		invNorms.x = delta_0.x * delta_0.x + delta_0.y * delta_0.y + delta_0.z * delta_0.z;
		invNorms.y = delta_1.x * delta_1.x + delta_1.y * delta_1.y + delta_1.z * delta_1.z;

		// This is slower
		//const __m128 inpt = _mm_load_ps(&invNorms.x);
		//const __m128 maxed = _mm_max_ps(inpt, _mm_set1_ps(FABRIK_EPS_FAST_CONSTR_PARALLEL));
		//const __m128 invnrm = _mm_sqrt_ps(maxed);
		//_mm_store_ps(&invNorms.x, invnrm);

		invNorms.x = FLT(1.0) / SQRT(max(invNorms.x, FABRIK_EPS_FAST_CONSTR_PARALLEL));
		invNorms.y = FLT(1.0) / SQRT(max(invNorms.y, FABRIK_EPS_FAST_CONSTR_PARALLEL));

		Point nrm_0 = { delta_0.x * invNorms.x, delta_0.y * invNorms.x, delta_0.z * invNorms.x };
		Point nrm_1 = { delta_1.x * invNorms.y, delta_1.y * invNorms.y, delta_1.z * invNorms.y };

		//Point nrm_0 = computeNormal_FAST_CONSTR_PARALLEL(prev_0, point_0);
		//Point nrm_1 = computeNormal_FAST_CONSTR_PARALLEL(prev_1, point_1);

		// Constrain to axis and rotate
#ifdef FABRIK_CONSTRAINT
		nrm_0 = constrain_FAST_CONSTR_PARALLEL(&transform_0->rotation, &nrm_0, &joint_0->c);
		nrm_1 = constrain_FAST_CONSTR_PARALLEL(&transform_1->rotation, &nrm_1, &joint_1->c);
#endif
#ifdef FABRIK_ROTATION
		at_0->rotation = rotateTowards_FAST_CONSTR_PARALLEL(&transform_0->rotation, &nrm_0);
		at_1->rotation = rotateTowards_FAST_CONSTR_PARALLEL(&transform_1->rotation, &nrm_1);
#endif

		// Move bone
		at_0->translation = computeBone_FAST_CONSTR_PARALLEL(prev_0, &nrm_0, -ae_0->length);
		at_1->translation = computeBone_FAST_CONSTR_PARALLEL(prev_1, &nrm_1, -ae_1->length);

		// Drag joint with
		joint_0 = ae_0;
		transform_0 = at_0;

		joint_1 = ae_1;
		transform_1 = at_1;
	}
	
	// Branch out
	backward_children_FAST_CONSTR_PARALLEL(armature, ch_0, min, joint_0, transform_0);
	backward_children_FAST_CONSTR_PARALLEL(armature, ch_1, min, joint_1, transform_1);
}

void backward_FAST_CONSTR_PARALLEL(Armature_FAST_CONSTR_PARALLEL* armature, int chain, const SolverJoint_FAST_CONSTR_PARALLEL* joint, const Transform* transform)
{
	const SolverChain_FAST_CONSTR_PARALLEL* ch = &armature->chains[chain];

	// Branch out
	backward_children_FAST_CONSTR_PARALLEL(armature, ch, 0, joint, transform);
}

void backward_children_FAST_CONSTR_PARALLEL(Armature_FAST_CONSTR_PARALLEL* armature, const SolverChain_FAST_CONSTR_PARALLEL* ch, int offset, const SolverJoint_FAST_CONSTR_PARALLEL* joint, const Transform* transform)
{
	backward_chain_FAST_CONSTR_PARALLEL(armature, ch->head + offset, ch->tail, &joint, &transform);

	int index = 0;
	while (index < ch->numb - 3)
	{
		backward_quad_FAST_CONSTR_PARALLEL(armature, ch->succ + index, joint, transform);
		index += 4;
	}

	if (index < ch->numb - 1)
	{
		backward_double_FAST_CONSTR_PARALLEL(armature, ch->succ + index, joint, transform);
		index += 2;
	}

	if (index < ch->numb)
	{
		backward_FAST_CONSTR_PARALLEL(armature, ch->succ + index, joint, transform);
	}
}

void doSolve_FAST_CONSTR_PARALLEL(Armature_FAST_CONSTR_PARALLEL* armature, Pose *output, unsigned int iterations)
{
	//FLOAT old_error = FLT_MAX;
	//FLOAT current_error = 0;
	
	//current_error = computeError_FAST_CONSTR_PARALLEL(armature);
	// TODO: Always same amount of iterations for performance testing
	// while (current_error > FABRIK_EPS && fabs(old_error - current_error) > FABRIK_EPS)
	for(unsigned int i = 0; i < iterations; i++)
	{
		const Transform* root = &armature->transforms[0]; 
		const SolverJoint_FAST_CONSTR_PARALLEL* joint = &armature->joints[0];

		// Do forward and backward pass recursively
		forward_FAST_CONSTR_PARALLEL(armature, 0, joint, &root->translation);
		backward_FAST_CONSTR_PARALLEL(armature, 0, joint, root);

		//old_error = current_error;
		//current_error = computeError_FAST_CONSTR_PARALLEL(armature);
	}

	for (int i = 0; i < armature->jointCount; i++)
	{
		output[i] = armature->transforms[i];
		VALIDATE(Transform_FAST_CONSTR_PARALLEL, &output[i]);
	}
}



void solve_FAST_CONSTR_PARALLEL(Scene* scene)
{
	Armature *armature = scene->armature;
	int jointCount = armature->jointCount;
	int objectiveCount = armature->objectiveCount;

	// Store number of children and targets for each joint
	int* childCount = (int*)malloc(sizeof(int) * jointCount);
	int* targetCount = (int*)malloc(sizeof(int) * jointCount);
	for (int i = 0; i < jointCount; i++)
	{
		childCount[i] = 0;
		targetCount[i] = 0;
	}

	for (int i = 0; i < objectiveCount; i++)
	{
		targetCount[armature->objectives[i].joint]++;
	}

	for (int i = 0; i < jointCount; i++)
	{
		const int parent = armature->joints[i].parent;
		if (parent >= 0)
		{
			childCount[parent]++;
		}
	}

	// Allocate children and reset count to rebuild
	int** children = (int**)malloc(sizeof(int*) * jointCount);
	int** targets = (int**)malloc(sizeof(int*) * jointCount);
	for (int i = 0; i < jointCount; i++)
	{
		children[i] = NULL;
		const int cCount = childCount[i];
		childCount[i] = 0;
		if (cCount > 0)
		{
			children[i] = (int*)malloc(sizeof(int) * cCount);
		}

		targets[i] = NULL;
		const int tCount = targetCount[i];
		targetCount[i] = 0;
		if (tCount > 0)
		{
			targets[i] = (int*)malloc(sizeof(int) * tCount);
		}
	}

	// Assign children
	for (int i = 0; i < jointCount; i++)
	{
		const int parent = armature->joints[i].parent;
		if (parent >= 0)
		{
			const int index = childCount[parent];

			children[parent][index] = i;
			if (parent != i) // ignore root
			{
				childCount[parent]++;
			}
		}
	}

	// Count chains (always count root)
	int chainCount = 1;
	for (int i = 0; i < jointCount; i++)
	{
		const int cCount = childCount[i];
		if (cCount > 1 || targetCount[i] > 0)
		{
			chainCount += cCount;
		}
	}

	// Assign targets
	for (int i = 0; i < objectiveCount; i++)
	{
		const int index = armature->objectives[i].joint;
		targets[index][targetCount[index]] = i;
		targetCount[index]++;
	}

	// Allocate chain armature
	Armature_FAST_CONSTR_PARALLEL arm;
	arm.joints = (SolverJoint_FAST_CONSTR_PARALLEL*)malloc(sizeof(SolverJoint_FAST_CONSTR_PARALLEL) * jointCount);
	arm.transforms = (Transform*)malloc(sizeof(Transform) * jointCount);
	arm.jointCount = jointCount;
	jointCount = 0;

	arm.chains = (SolverChain_FAST_CONSTR_PARALLEL*)malloc(sizeof(SolverChain_FAST_CONSTR_PARALLEL) * chainCount);
	arm.targets = (Point*)malloc(sizeof(Point) * chainCount);
	int* queue = (int*)malloc(sizeof(int) * chainCount);
	arm.chainCount = chainCount;

	// Create target mappings
	arm.targetChains = (int*)malloc(sizeof(int) * objectiveCount);
	arm.objectiveCount = objectiveCount;

	// Create root chain
	arm.chains[0].head = 0;
	arm.targets[0] = PointIdentity;
	chainCount = 1;

	// FIFO
	queue[0] = 0;
	int top = 1, bottom = 0;
	while (bottom < top)
	{
		const int chain = queue[bottom++];
		SolverChain_FAST_CONSTR_PARALLEL* ch = &arm.chains[chain];

		// Assign proper chain-joint index
		int joint = ch->head;
		ch->head = jointCount;

		// Build chain
		int cCount, tCount;
		for(;;)
		{
			cCount = childCount[joint];
			tCount = targetCount[joint];

			// Move along chain
			ch->tail = jointCount++;

			// Get next bone
			Joint* ae = &armature->joints[joint];

			// Set joint settings
			Point* c = &arm.joints[ch->tail].c;
			c->x = FLT(1.0) - COS(ae->c.x);
			c->y = FLT(1.0) - COS(ae->c.y);
			c->z = FLT(1.0) - COS(ae->c.z);
			c->w = FLT(1.0) - COS(ae->c.w);

			arm.transforms[ch->tail] = scene->initialPose[joint];

			const int pred = max(armature->joints[joint].parent, 0); // Parent root bone with itself
			arm.joints[ch->tail].length = SQRT(length_squared_FAST_CONSTR_PARALLEL(&scene->initialPose[joint].translation, &scene->initialPose[pred].translation));
			arm.joints[ch->tail].weight = FLT(1.0) / (cCount + tCount);

			// Go to next joint if chain continues
			if (cCount != 1 || tCount > 0) break;
			joint = children[joint][0];
		}

		// Add children to queue
		ch->numb = cCount;
		ch->succ = chainCount;
		for (int i = 0; i < cCount; i++)
		{
			SolverChain_FAST_CONSTR_PARALLEL* child = &arm.chains[chainCount];
			child->head = children[joint][i];
			queue[top++] = chainCount;
			chainCount++;
		}

		// Assign and initialise targets
		arm.targets[chain] = PointIdentity;
		for (int i = 0; i < tCount; i++)
		{
			arm.targetChains[targets[joint][i]] = chain;
		}
	}
	free(queue);

	// Make sure root isn't touched
	arm.chains[0].head = 1;
	arm.joints[0].weight = FLT(0.0);

	// Initialisation cleanup
	for (int i = 0; i < jointCount; i++)
	{
		if(children[i])	free(children[i]);
		if(targets[i]) free(targets[i]);
	}
	free(children);
	free(targets);
	free(childCount);
	free(targetCount);

	// run FABRIK for each frame
	for (int f = 0; f < scene->frameCount; f++)
	{
		///////////////////////////////////////////////////////////////////////////////
		Target* targets = scene->targets[f];

		for (int i = 0; i < arm.chainCount; i++)
		{
			arm.targets[i] = PointIdentity;
		}
		for (int i = 0; i < armature->objectiveCount; i++)
		{
			// Add target to approrpiate chain
			const int chain = arm.targetChains[i];
			Point* tr = &arm.targets[chain];
			tr->x += targets[i].x;
			tr->y += targets[i].y;
			tr->z += targets[i].z;
		}
		Pose* output = scene->frames[f];
		doSolve_FAST_CONSTR_PARALLEL(&arm, output, scene->iterations);
		///////////////////////////////////////////////////////////////////////////////
	}

	// clean up
	free(arm.targetChains);
	free(arm.targets);
	free(arm.transforms);
	free(arm.chains);
	free(arm.joints);
}
