
import bpy
import math

# Settings:
framestep = 5

# Usage:
# Go into Pose mode and select all bones you want to be end-effectors.
# The upper common parent becomes root and .arm file is generated.

output = ""

# Find involved bones, also compute tail (bones all paths have in common)
selection = []
tail = bpy.context.selected_pose_bones[0].parent_recursive
for bone in bpy.context.selected_pose_bones :
    
    tail = list(set(tail) & set(bone.parent_recursive))
    selection = selection + list(set([bone] + bone.parent_recursive) - set(selection))

# Find true root
root = tail[0]
for bone in tail :
    if root in bone.parent_recursive :
        root = bone;

# Remove tail
selection = list(set(selection) - set(root.parent_recursive))

# recursive function to find bone indices
def labelBone(bone, list):
    
    output = ""
    index = len(list)
    list.append(bone)
    for child in bone.children :
        if child in selection :
            
            output = output + str(index) + " 180 180 180 180 \n"
            output = output + labelBone(child, list)
            
    return output
    
# Label bones
bones = []
structure = "180 180 180 180 \n" + labelBone(root, bones)

# Write file properties
jointcount = len(bones) - 1
framecount = math.floor((bpy.context.scene.frame_end - bpy.context.scene.frame_start) / framestep)
targetcount = len(bpy.context.selected_pose_bones)

output = output + str(jointcount) + " " + str(targetcount) + " " + str(framecount) + "\n"
output = output + structure

# Set target index
for bone in bpy.context.selected_pose_bones :
    output = output + str(bones.index(bone)) + "\n"

# Set initial pose
for bone in bones :
    if not bone is root :
        pos = (bone.head - root.head) / 10
        output = output + ('%.2f %.2f %.2f' % (pos.x, pos.y, pos.z)) + "\n"

# Go through all frames and set target positions
bpy.context.scene.frame_set(bpy.context.scene.frame_start);
while bpy.context.scene.frame_current <= bpy.context.scene.frame_end :
    
    for bone in bpy.context.selected_pose_bones :
        pos = (bone.head - root.head) / 10
        output = output + ('%.2f %.2f %.2f' % (pos.x, pos.y, pos.z)) + "\n"
        
    bpy.context.scene.frame_set(bpy.context.scene.frame_current + framestep);

filename = bpy.path.abspath("//export.arm")
file=open(filename,'w')
file.write(output)
file.close()