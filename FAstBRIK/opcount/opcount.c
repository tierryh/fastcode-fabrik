
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <inttypes.h>

#include "../src/include/reader.h"
#include "../src/include/solver.h"
#include "../src/include/solverSIMD.h"
#include "../src/include/solverMEM.h"
#include "../src/include/solverFAST.h"
#include "../src/include/solverCACHE.h"
#include "../src/include/solverCONSTR.h"
#include "../src/include/solverCACHE_MEM_CONSTR.h"
#include "../src/include/solverCACHE_MEM_CONSTR_SIMD.h"
#include "../src/include/solverFAST_CONSTR.h"
#include "../src/include/solverFAST_CONSTR_PARALLEL.h"
#include "../src/include/solverOther.h"
#include "../src/include/solverOther_baseline.h"

#define VALIDATE_SOLVER(FUNC, NAME) \
if (validate(scene, NAME, &FUNC)) { printf("%s passed! \n", NAME); }

int validate(Scene* scene, const char* name, void (*solver_func)(Scene*))
{
	// Run same scene again to validate results
	if (scene)
	{
		printf("Validating %s... \n", name);
		startValidation();
		solver_func(scene);

		return !hasError;
	}
	return 1;
}


int main(int argc, char *argv[])
{
	char filename[FILENAME_BUF_LEN] = "default";
	if (argc > 1)
	{
		if (strlen(argv[1]) > FILENAME_BUF_LEN - 1) {
			printf("Filename does not fit into (statically-sized) string buffer of size %d - either adapt FILENAME_BUF_LEN or rename the file.\n", FILENAME_BUF_LEN);
			return 1;
		}
		strcpy(filename, argv[1]);
	}

	// Read args
	Args args;
	args.maxIterations = 100;
	if (argc > 2)
	{
		unsigned long long n = strtoumax(argv[2], 0, 10);
		if (n != UINTMAX_MAX)
		{
			args.maxIterations = (unsigned int)n;
		}
	}

	// Read args
	args.jointSubdivisions = 30;
	if (argc > 3)
	{
		unsigned long long n = strtoumax(argv[3], 0, 10);
		if (n != UINTMAX_MAX)
		{
			args.jointSubdivisions = (unsigned int)n;
		}
	}

	// Read args
	args.frameSubdivisions = 3;
	if (argc > 4)
	{
		unsigned long long n = strtoumax(argv[4], 0, 10);
		if (n != UINTMAX_MAX)
		{
			args.frameSubdivisions = (unsigned int)n;
		}
	}

	// Read args
	args.treeDepth = 8;
	if (argc > 5)
	{
		unsigned long long n = strtoumax(argv[5], 0, 25);
		if (n != UINTMAX_MAX)
		{
			args.treeDepth = (unsigned int)n;
		}
	}

	// Read args
	unsigned int hasValidate = 1;
	if (argc > 6)
	{
		hasValidate = (argv[6][0] != '0');
	}

	// Check whether scene could be loaded
	Scene* scene = readScene(filename, &args);
	if (scene)
	{
		printf("Solving %s... \n", filename);

		// Solve
		initHistory();
		if (hasValidate) startRecording();
		solve(scene);

		// Save ops for performance measurment
		FILE *fp;
		fp = fopen("ops.dat", "w");
		fprintf(fp, "%s %d %d %d %d %lld %lld %lld %lld %lld %lld %lld %lld %lld %lld \n",
			filename, args.maxIterations, args.jointSubdivisions, args.frameSubdivisions, args.treeDepth,
			ops.add_op_count, ops.mul_op_count,
			ops.div_op_count, ops.sqrt_op_count,
			ops.trig_op_count, ops.max_op_count,
			ops.tot_op_count,
			ops.read_op_count, ops.write_op_count, ops.compl_op_count);
		fclose(fp);

		// Output results to user
		printf("Total ops: \n");
		printf("adds: %lld, mults: %lld, divs: %lld, sqrts: %lld, trig: %lld, max: %lld, tot: %lld \n",
			ops.add_op_count, ops.mul_op_count,
			ops.div_op_count, ops.sqrt_op_count,
			ops.trig_op_count, ops.max_op_count,
			ops.tot_op_count);

		const int totalIterations = scene->iterations * scene->frameCount;
		printf("Total ops per iteration (total %d): \n", totalIterations);
		printf("adds: %.3f, mults: %.3f, divs: %.3f, sqrts: %.3f, trig: %.3f, max: %.3f, tot: %.3f \n",
			(FLOAT)ops.add_op_count / totalIterations, (FLOAT)ops.mul_op_count / totalIterations,
			(FLOAT)ops.div_op_count / totalIterations, (FLOAT)ops.sqrt_op_count / totalIterations,
			(FLOAT)ops.trig_op_count / totalIterations, (FLOAT)ops.max_op_count / totalIterations,
			(FLOAT)ops.tot_op_count / totalIterations);

		const int totalJoints = totalIterations * scene->armature->jointCount;
		printf("Total ops per joint per iteration (total %d): \n", totalJoints);
		printf("adds: %.3f, mults: %.3f, divs: %.3f, sqrts: %.3f, trig: %.3f, max: %.3f, tot: %.3f \n",
			(FLOAT)ops.add_op_count / totalJoints, (FLOAT)ops.mul_op_count / totalJoints,
			(FLOAT)ops.div_op_count / totalJoints, (FLOAT)ops.sqrt_op_count / totalJoints,
			(FLOAT)ops.trig_op_count / totalJoints, (FLOAT)ops.max_op_count / totalJoints,
			(FLOAT)ops.tot_op_count / totalJoints);

		printf("Total bandwidth (in bytes): \n");
		printf("reads: %lld, writes: %lld, compulsory: %lld \n",
			ops.read_op_count, ops.write_op_count, ops.compl_op_count);
		printf("%.3f < I < %.3f\n", (double)ops.tot_op_count / (double)(ops.read_op_count + ops.write_op_count + ops.compl_op_count), (double)ops.tot_op_count / (double)ops.compl_op_count);

		if (hasValidate)
		{
			// Validate other solvers
			VALIDATE_SOLVER(solve, "naive");
			//VALIDATE_SOLVER(solve_FAST, "FAST");
			//VALIDATE_SOLVER(solve_SIMD, "SIMD");
			//VALIDATE_SOLVER(solve_CACHE, "CACHE");
			//VALIDATE_SOLVER(solve_MEM, "MEM");
			//VALIDATE_SOLVER(solve_CONSTR, "CONSTR");
			//VALIDATE_SOLVER(solve_CACHE_MEM_CONSTR, "CACHE_MEM_CONSTR");
			VALIDATE_SOLVER(solve_CACHE_MEM_CONSTR_SIMD, "CACHE_CONSTR_SIMD");
			//VALIDATE_SOLVER(solve_CACHE_MEM_CONSTR_SIMD, "CACHE_MEM_CONSTR_SIMD");
			//VALIDATE_SOLVER(solve_FAST_CONSTR, "FAST_CONSTR");
			//VALIDATE_SOLVER(solve_FAST_CONSTR_PARALLEL, "FAST_CONSTR_PARALLEL");
			//VALIDATE_SOLVER(solve_Other, "other");
			//VALIDATE_SOLVER(solveOtherBaseline, "other_baseline");
		}

		// Cleanup
		deleteHistory();
		deleteScene(scene);
	}
	else
	{
		printf("Reading error, couldn't solve anything! \n");
		return 1;
	}

	return 0;
}
