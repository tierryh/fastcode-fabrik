
#include <stdlib.h>
#include <stdio.h>

#include <inttypes.h>
#include <string.h>

#include "../src/include/reader.h"
#include "../src/include/solver.h"
#include "../src/include/solverSIMD.h"
#include "../src/include/solverMEM.h"
#include "../src/include/solverFAST.h"
#include "../src/include/solverCACHE.h"
#include "../src/include/solverCONSTR.h"
#include "../src/include/solverCACHE_MEM_CONSTR.h"
#include "../src/include/solverCACHE_CONSTR_SIMD.h"
#include "../src/include/solverCACHE_MEM_CONSTR_SIMD.h"
#include "../src/include/solverFAST_CONSTR.h"
#include "../src/include/solverFAST_CONSTR_PARALLEL.h"
#include "../src/include/solverOther.h"
#include "../src/include/solverOther_baseline.h"

#include "include/tsc_x86.h"
#include "include/profiler.h"

#define CYCLES_REQUIRED 1e7
#define NUM_OPS 1e7
#define REP 5

int main(int argc, char *argv[])
{
	REGISTER(solve, "naive");
	REGISTER(solve_SIMD, "SIMD");
	REGISTER(solve_MEM, "MEM");
	REGISTER(solve_FAST, "FAST");
	REGISTER(solve_CACHE, "CACHE");
	REGISTER(solve_CONSTR, "CONSTR");
	REGISTER(solve_CACHE_MEM_CONSTR, "CACHE_MEM_CONSTR");
	REGISTER(solve_CACHE_CONSTR_SIMD, "CACHE_CONSTR_SIMD");
	REGISTER(solve_CACHE_MEM_CONSTR_SIMD, "CACHE_MEM_CONSTR_SIMD");
	REGISTER(solve_FAST_CONSTR, "FAST_CONSTR");
	REGISTER(solve_FAST_CONSTR_PARALLEL, "FAST_CONSTR_PARALLEL");
	REGISTER(solve_Other, "other");
	REGISTER(solveOtherBaseline, "other_baseline");

	// Read args
	char opsFile[256] = "ops.dat";
	if (argc > 1)
	{
		if (strlen(argv[1]) > FILENAME_BUF_LEN - 1) {
			printf("Filename does not fit into (statically-sized) string buffer of size %d - either adapt FILENAME_BUF_LEN or rename the file.\n", FILENAME_BUF_LEN);
			return 1;
		}
		strcpy(opsFile, argv[1]);
	}
	// 0 = no cachegrind
	// -1 = do not solve (used to subtract scene reading)
	// 1-... = run func
	int cachegrind = 0;
	if (argc > 2)
	{
		unsigned long long n = strtoimax(argv[2], 0, 10);
		if (n != INTMAX_MAX)
		{
			cachegrind = n;
		}
	}

	// Read file from opcount
	FILE *fp;
	fp = fopen(opsFile, "r");
	unsigned long long int add_ops = 1, mul_ops = 1, div_ops = 1, sqrt_ops = 1, trig_ops = 1, max_ops = 1, total = 6;
	unsigned long long int read_ops = 1, write_ops = 1, compl_ops = 2;
	if (fp)
	{
		Args args = {0,0,0,0};
		char filename[256];
		fscanf(fp, "%255s %d %d %d %d %lld %lld %lld %lld %lld %lld %lld %lld %lld %lld \n",
			&filename, &args.maxIterations, &args.jointSubdivisions, &args.frameSubdivisions, &args.treeDepth,
			&add_ops, &mul_ops, &div_ops, &sqrt_ops, &trig_ops, &max_ops, &total, &read_ops, &write_ops, &compl_ops);

		Scene* scene = readScene(filename, &args);

		// Check whether scene could be loaded
		if (scene)
		{
			printf("Solving... \n");
			printf("%d joints in armature.\n", scene->armature->jointCount);

			if (cachegrind == 0)
			{
				for (int i = 0; i < func_count; i++)
				{
					printf("------------------------------------. \n");
					printf("Solving for function %s: \n", funcs[i].name);

					const perf_result result = perf_test(funcs[i].func, scene, default_test_config);

					printf("Best repetition for function %s was with %.3f cycles over %lld ops. \n", funcs[i].name, result.cycles, total);
					printf("Which is %.3f ops/cycle. \n", (double)total / result.cycles);
					printf("Best guess for intensity for function %s was %.3f. \n", funcs[i].name, (double)total / (double)(read_ops + write_ops + compl_ops));
					printf("Compulsory intensity is %.3f ops/byte. \n", (double)total / (double)(compl_ops));
					printf("Bounded intensity is %.3f ops/byte. \n", (double)total / (double)(read_ops + write_ops + compl_ops));
				}
			}
			else if (cachegrind > 0)
			{
				funcs[cachegrind-1].func(scene);
			}

			// Cleanup
			deleteScene(scene);
		}
		else
		{
			printf("Reading error, couldn't solve anything! \n");
			fclose(fp);
			return 1;
		}

		fclose(fp);
	}
	else
	{
		printf("No OpCount file found! Execute OpCount first.");
		return 1;
	}

	return 0;
}
