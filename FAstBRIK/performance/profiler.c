#include "include/profiler.h"
#include "include/tsc_x86.h"

#include <stdio.h>
#include <math.h>

/**
 * The default configuration for performance tests.
 */
perf_test_config default_test_config = {
	.min_cycles = (int)1e7,
	.min_runs = 2,
	.rep = 2,
};

int numfuncs = 0;

perf_result perf_test(perf_func f, Scene* scene, perf_test_config config) {
    int num_runs = config.min_runs;
    double multiplier = 1;
    double cycles;

    // Warm-up phase: we determine a number of executions that allows
    // the code to be executed for at least config.min_cycles cycles.
    // This helps excluding timing overhead when measuring small runtimes.
    do {
        num_runs = (int)(num_runs * multiplier);
		const tsc start = start_tsc();
        for (int i = 0; i < num_runs; i++) {
            f(scene);
        }
		const tsc end = stop_tsc(start);
        cycles = (double)end / num_runs;
        multiplier = MAX(1.0, config.min_cycles / end);
    } while (multiplier > 2);

		num_runs = MAX(num_runs, config.min_runs);
		printf("Measure performance for %d runs: \n", num_runs);

    // Actual performance measurements
    for (int j = 0; j < config.rep; j++) {
		const tsc start = start_tsc();
        for (int i = 0; i < num_runs; ++i) {
            f(scene);
        }
		const tsc end = stop_tsc(start);
        const double c = ((double)end) / num_runs;
		printf("%d-th repetition with %.0f cycles. \n", j, c);
		cycles = MIN(cycles, c);
    }

    const perf_result out = { cycles };
    return out;
}
