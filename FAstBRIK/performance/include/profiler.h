#ifndef PROFILER_H
#define PROFILER_H

#include "../../src/include/reader.h"

// a function pointer typedef for the function passed to performance tests.
typedef void (*perf_func)(Scene*);

// a struct which captures the result of a performance test
typedef struct {
    double cycles;
} perf_result;

// a struct capturing the configuration of a performance test
typedef struct {
    // the minimum amount of cycles that should be executed for a performance test
    // this amount determines the number of iterations the test function will be executed, which will be large for small test functions (to reduce the timing overhead) and small for large test functions.
    int min_cycles;
    // the minimum number of runs a test function should be run
    int min_runs;
    // the number of repetitions of the test
    int rep;
} perf_test_config;

// Default configuration
perf_test_config default_test_config;

// Uglyness because I'm to lazy to implement proper dynamic arrays
int func_count;
struct {
	perf_func func;
	char* name;
} funcs[32];

#define REGISTER(FUNC, NAME) \
funcs[func_count].func = &FUNC; \
funcs[func_count].name = NAME; \
func_count++

/**
 * The performance test.
 * - parameter f: the function which should be tested
 * - parameter arg: the argument which should be passed for the test function
 * - parameter config: the test configuration
 */
perf_result perf_test(perf_func f, Scene* scene, perf_test_config config);

#endif
