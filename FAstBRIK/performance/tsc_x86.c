
#include "include/tsc_x86.h"

tsc start_tsc()
{
	// _mm_lfence();
	tsc t = __rdtsc();
	// _mm_lfence();
	return t;
}

tsc stop_tsc(tsc s)
{
	// _mm_lfence();
	tsc t = __rdtsc();
	// _mm_lfence();
	return t - s;
}
