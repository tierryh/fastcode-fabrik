#!/usr/bin/env python3
from subprocess import run, PIPE
import tempfile
from os import path
import sys
from copy import deepcopy
import argparse
import re

# https://ark.intel.com/content/www/de/de/ark/products/124967/intel-core-i5-8250u-processor-6m-cache-up-to-3-40-ghz.html
# memory bandwidth 34.1 GB/s
# base frequency: 1.60 GHz
# bandwith: 21.31 bytes/cycle
BANDWIDTH = 21.31
# https://software.intel.com/sites/landingpage/IntrinsicsGuide/#text=_mm256_fmadd&expand=2547
PEAKPERFORMANCE = 32
# cat /sys/devices/system/cpu/cpu0/cache/index3/
# size: 6144K
# 8192 sets
# cache line size: 64 bytes
# 12-way set-associative
CACHE_LINE_SIZE = 64

fastbrik_dir = path.dirname(path.abspath(__file__)) + "/../"

# configuration
problem_sizes = [(600, 2**i, 4, j) for i in range(0, 7) for j in range(6, 7)]
# default_armature_template_file = fastbrik_dir + "assets/default.arm"
default_armature_template_file = "default"

# parse arguments
arg_parser = argparse.ArgumentParser(description="Run FAstBRIK benchmarks")
arg_parser.add_argument(
    "--armature",
    "-a",
    metavar = "file",
    default = default_armature_template_file,
    help    = "armature file to use as basis"
)
arg_parser.add_argument(
    "--visualize",
    dest    = "visualize",
    default = False,
    const   = True,
    action  = "store_const",
    help    = "do not perform measurements, but visualize the output"
)
arg_parser.add_argument(
    "--no-build",
    dest    = "remake_dependencies",
    default = True,
    const   = False,
    action  = "store_const",
    help    = "do not rebuild before measurements"
)
arg_parser.add_argument(
    "--output",
    "-o",
    metavar = "file",
    dest    = "output_file",
    default = None,
    help    = "store performance data to file",
)
arg_parser.add_argument(
    "--input",
    "-i",
    metavar="file",
    dest="input_file",
    default=None,
    help="load performance data from file",
)
arg_parser.add_argument(
    "--store",
    "-s",
    metavar = "file",
    dest    = "plot_file",
    default = None,
    help    = "store performance plots to this PNG file, instead of showing them. use {} as placeholder for the plot type (performance vs. runtime)"
)
arg_parser.add_argument(
    "--storex",
    default=640,
    help="Width of exported image",
)
arg_parser.add_argument(
    "--storey",
    default=480,
    help="Width of exported image",
)
arg_parser.add_argument(
    "--no-plot",
    action="store_true",
    help="Do not plot result",
)
arg_parser.add_argument(
    "--plotexclude",
    nargs="*",
    default=[],
    help="exclude solvers"
)
arg_parser.add_argument(
    "--plotinclude",
    nargs="*",
    default=[],
    help="include solvers"
)
# http://valgrind.org/docs/manual/cg-manual.html
# http://wwwcdf.pd.infn.it/valgrind/cg_main.html
# cachegrind automatically sets cache information
arg_parser.add_argument(
    "--cachegrind",
    "-c",
    action="store_true",
    help="Run cachegrind to get a better idea of memory transfers. This is slow!",
)
args = arg_parser.parse_args()

# verify arguments
if args.plot_file is not None and '"' in args.plot_file:
    print("plot_file must not contain quotes")
    exit(1)

# build dependencies
if args.remake_dependencies:
    build_dir = fastbrik_dir + "build/Release/"
    run("make", cwd=build_dir)

if args.visualize:  # run FAstBRIK and visualize output
    visual = run([fastbrik_dir + 'build/Release/FAstBRIK', args.armature])
    if visual.returncode != 0:
        print("Could not visualize, FAstBRIK returned %d" % visual.returncode)
        exit(1)
    exit(0)

def get_cachegrind_misses():
    with open("cachegrind.dat") as f:
        summary = f.readlines()[-1].split()
        return int(summary[6]) + int(summary[9])

if not args.input_file:
    # run benchmarks
    data_points = []
    for maxIterations, jointSubdivisions, frameSubdivisions, treeDepth in problem_sizes:
        print("Get opcount for jointSubdivisions %d and treeDepth %d" % (jointSubdivisions, treeDepth))
        opcount = run([fastbrik_dir + 'build/Release/Opcount', str(args.armature), str(maxIterations), str(jointSubdivisions), str(frameSubdivisions), str(treeDepth), "0"], stdout=PIPE)
        if opcount.returncode != 0:
            print("Could not calculate opcount for jointSubdivisions %d, opcount returned %d, abort" % (jointSubdivisions, opcount.returncode))
            exit(1)

        print("Measure performance for jointSubdivisions %d and treeDepth %d" % (jointSubdivisions, treeDepth))
        perf = run([fastbrik_dir + 'build/Release/Performance', fastbrik_dir + 'ops.dat'], stdout=PIPE)
        if perf.returncode != 0:
            print("Could not measure performance, FAstBRIK returned %d" % perf.returncode)
            exit(1)
        perfstdout = perf.stdout.decode("utf8")

        armaturesizematch = re.search("([0-9]+) joints in armature.", perfstdout)
        if not armaturesizematch:
            print("Could not extract armature size, abort")
            exit(1)
        size = int(armaturesizematch.group(1))
        print("{} joints in armature.".format(size))

        # extract performance data from output
        matches = re.finditer("Best repetition for function ([a-zA-Z0-9_]+) was with ([0-9]+)(\.[0-9]*)? cycles over ([0-9]+) ops.", perfstdout)
        if not matches:
            print("Could not extract performance data, abort")
            exit(1)

        if args.cachegrind:
            cache = run(['valgrind', '--tool=cachegrind', '--cachegrind-out-file=cachegrind.dat', fastbrik_dir + 'build/Release/Performance', fastbrik_dir + 'ops.dat', "-1"], stdout=PIPE, stderr=PIPE)
            cachegrind_base = get_cachegrind_misses()

        names = []
        runtimes = []
        performances = []
        intensities = []

        matchesInt = re.finditer("Best guess for intensity for function ([a-zA-Z0-9_]+) was ([0-9]+\.[0-9]*).", perfstdout)

        for i, match in enumerate(matches):
            name = match.group(1)
            cycles = int(match.group(2))
            operations = int(match.group(4))
            if args.cachegrind:
                cache = run(['valgrind', '--tool=cachegrind', '--cachegrind-out-file=cachegrind.dat', fastbrik_dir + 'build/Release/Performance', fastbrik_dir + 'ops.dat', str(i+1)], stdout=PIPE, stderr=PIPE)
                cachegrind = get_cachegrind_misses() - cachegrind_base
                intensity = operations / (cachegrind * CACHE_LINE_SIZE)
            else:
                matchInt = next(matchesInt)
                assert name == matchInt.group(1)
                intensity = float(matchInt.group(2))
            performance = operations / cycles
            print("Best run of %s in %d cycles, reaching %f flops/cycle, with intensity %f flops/byte." % (name, cycles, performance, intensity))

            names.append(name)
            runtimes.append(cycles)
            performances.append(performance)
            intensities.append(intensity)

        data_points.append({
            'size': size,
            'names': names,
            'runtimes': runtimes,
            'performances': performances,
            'intensities': intensities,
        })
        print()

    # store data and plot
    if len(data_points) == 0:
        print("No data points found")
        exit(1)

    data_points.sort(key=lambda x: x['size'])

    # check that data points are consistent
    for data_point in data_points:
        if data_points[0]['names'] != data_point['names']:
            print("Function names are not stable, abort.")
            exit(1)

    if args.output_file is not None:
        dat_file = open(args.output_file, "w")
        dat_file_name = args.output_file
    else:
        dat_file = tempfile.NamedTemporaryFile(suffix=".dat", mode="w+")
        dat_file_name = dat_file.name

    # write headers
    dat_file.write("# size")
    for name in data_points[0]['names']:
        dat_file.write("    %s runtime" % name)
    for name in data_points[0]['names']:
        dat_file.write("    %s performance" % name)
    for name in data_points[0]['names']:
        dat_file.write("    %s intensity" % name)
    dat_file.write("\n")

    # write data points
    for data_point in data_points:
        dat_file.write("%d" % data_point['size'])
        for runtime in data_point['runtimes']:
            dat_file.write("    %d" % runtime)
        for performance in data_point['performances']:
            dat_file.write("    %f" % performance)
        for intensity in data_point['intensities']:
            dat_file.write("    %f" % intensity)
        dat_file.write("\n")

    dat_file.flush()

    dat_file.close()
else:
    data_points = []
    dat_file_name = args.input_file
    with open(args.input_file, "r") as f:
        lines = f.readlines()
        matches = re.finditer("    ([a-zA-Z0-9_]+) runtime", lines[0])
        functions = [x.group(1) for x in matches]
        for line in lines[1:]:
            spl = line.split()
            assert len(spl) == 1+3*len(functions)
            size = int(spl[0])
            runtimes = list(map(float, spl[1:1+len(functions)]))
            performances = list(map(float, spl[1+len(functions):1+2*len(functions)]))
            intensities = list(map(float, spl[1+2*len(functions):1+3*len(functions)]))
            data_points.append({
                'size': size,
                'names': functions,
                'runtimes': runtimes,
                'performances': performances,
                'intensities': intensities,
            })

if args.no_plot:
    print("Skip plots")
    exit(0)

plotexclude = args.plotexclude[:]
if args.plotinclude:
    for name in data_points[0]['names']:
        if name not in args.plotinclude:
            plotexclude.append(name)

# show the runtime plot
column_offset = 1
columns = []
for name in data_points[0]['names']:
    column_offset += 1
    if name in plotexclude:
        continue
    columns.append('"%s" using 1:%d title "%s" with linespoints' % (dat_file_name, column_offset, name.replace('_', '-')))
plot_command = 'set title "Runtime Plot";set logscale x;set xlabel "Problem size (number of joints)";set ylabel "Runtime [cycles]"; plot ' + ','.join(columns)
if args.plot_file is not None:
    plot_file = args.plot_file
    if '{}' in plot_file:
        plot_file = plot_file.format('runtime')
    plot_command = 'set term pngcairo size {},{}; set output "{}";'.format(args.storex, args.storey, plot_file) + plot_command
    plot = run(["gnuplot", "-e", plot_command])
else:
    plot = run(["gnuplot", "-persist", "-e", plot_command])
if plot.returncode != 0:
    print("Creating runtime plot failed, gnuplot returned %d" % plot.returncode);
    exit(1)

# show the performance plot
columns = []
for name in data_points[0]['names']:
    column_offset += 1
    if name in plotexclude:
        continue
    columns.append('"%s" using 1:%d title "%s" with linespoints' % (dat_file_name, column_offset, name.replace('_', '-')))
plot_command = 'set title "Performance Plot";set logscale x;set xlabel "Problem size (number of joints)";set ylabel "Performance [flops/cycle]"; set yrange [0:1.1]; plot ' + ','.join(columns)
if args.plot_file is not None:
    plot_file = args.plot_file
    if '{}' in plot_file:
        plot_file = plot_file.format('performance')
    plot_command = 'set term pngcairo size {},{}; set output "{}";'.format(args.storex, args.storey, plot_file) + plot_command
    plot = run(["gnuplot", "-e", plot_command])
else:
    plot = run(["gnuplot", "-persist", "-e", plot_command])
if plot.returncode != 0:
    print("Creating performance plot failed, gnuplot returned %d" % plot.returncode);
    exit(1)

# show the roofline plot
roofline_points = []
legend_entries = []
for point in data_points:
    for i, name in enumerate(filter(lambda x: x not in plotexclude, point["names"])):
        roofline_points.append("set label '' at {},{} point pointtype 2 lt {} offset 0.5,0.5".format(point["intensities"][i], point["performances"][i], i+3))

legend_entries.append('{} title "π" with lines'.format(PEAKPERFORMANCE))
legend_entries.append('{}*x title "β" with lines'.format(BANDWIDTH))
for i, name in enumerate(point["names"]):
    if name in plotexclude:
        continue
    legend_entries.append('1/0 title "{}" with points pointtype 2'.format(name.replace('_', '-')))
plot_command = 'set title "Roofline"; set xlabel "I = W/Q"; set ylabel "P = W/T"; set autoscale; set xrange [0.01:5000]; set yrange [0.01:50]; set logscale xy; {}; plot {};'.format(";".join(roofline_points), ", ".join(legend_entries))
if args.plot_file is not None:
    plot_file = args.plot_file
    if '{}' in plot_file:
        plot_file = plot_file.format('roofline')
    plot_command = 'set term pngcairo size {},{}; set output "{}";'.format(args.storex, args.storey, plot_file) + plot_command
    plot = run(["gnuplot", "-e", plot_command])
else:
    plot = run(["gnuplot", "-persist", "-e", plot_command])
if plot.returncode != 0:
    print("Creating runtime plot failed, gnuplot returned %d" % plot.returncode);
    exit(1)
