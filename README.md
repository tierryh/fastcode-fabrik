# FAstBRIK

## Provided Libraries
Project setup (cmake and libraries) are inspired by the ethz course [Shape Modeling and Geometry Processing](https://igl.ethz.ch/teaching/shape-modeling/sm2019/). 
The [libigl](https://github.com/libigl/libigl) repo includes more in-depth instructions and tutorials.

## Project structure
This project is split into 3 application: Opcount, Performance and FastBRIK. This is mainly due to precompiler flags turning certain features on or off
for performance measurements. Additionally, benchmarking is done using a python script with GNUPlot and Valgrind for the roofline model.

### FastBRIK
The visual representation of the armature is done using libIGL. The included GUI allows changing armature, animating it, increasing armature subdivisions, frames etc.
This is the only part of the project implemented in C++. All performance measurements and opcounts are turned off.

### Opcount
The cost measure is evaluated automatically in the naive implementation using macros behind every arithmetic instruction. Validity is measured using a stack based
approach. Since the solution especially for big armatures diverges rather quickly, even correct implementations with changes due to floating point precision will be 
flagged invalid after N steps. It is usually good enough if it holds up a certain amount of iterations and to later inspect the armature in FastBRIK by eye.

### Performance
Running performance measurements requires opcount having run and generated an ops.dat file first. It stores the cost measure and what solver arguments were used.
Performance is measured using the built-in tsc_x86 header and a similar profiler as introduced in the FastCode exercises. All Opcount macros, validations and 
visual representations are turned off.

### Benchmarking
The python script has various arguments as defined in the man page. It has the ability to measure performance for different armature sizes and generates plots.


## Setup
### Cloning the Repository

Recursive:
```
git clone --recursive <url>
```

### Updating

To update libigl:
```
rm -r libigl
git submodule sync
git submodule update --init --recursive
```

### Building project

Create and go into build folder and cmake:
```
cd FAstBRIK
mkdir build
cd build
cmake ..
```
